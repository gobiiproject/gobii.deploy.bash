#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
#Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "Not Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "Not Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set Environment for shell
set -e # Abort script at first error, when a command exits with non-zero status
set -u # Forces exit when a variable is undefined
set -x # xtrace: Set High Verbosity

#-----------------------------------------------------------------------------#

TAG=$(date +"%Y%m%d_%H%M%S");

#-----------------------------------------------------------------------------#
# Create Hotfix download location and download files

docker exec -i -u gadm $DOCKER_WEB_NAME bash -c "
mkdir -pv $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$ORIGINAL_FILE_BACKUP_LOCATION ||true;
cd $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION; 
wget $GOBII_WAR_DOWNLOAD_URL;
";
echo;


#-----------------------------------------------------------------------------#
# backup originial

DOCKER_CMD="cd $GOBII_WAR_LOCATION;
cp -v $CROP1_WAR_NAME $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$ORIGINAL_FILE_BACKUP_LOCATION/$CROP1_WAR_NAME\_$TAG;
cp -v $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$GOBII_WEB_WAR_HOTFIX_FILE_NAME ./$CROP1_WAR_NAME;";
eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#

if [[ -v CROP2_WAR_NAME ]]; then
        DOCKER_CMD="cd $GOBII_WAR_LOCATION;
		cp -v $CROP2_WAR_NAME $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$ORIGINAL_FILE_BACKUP_LOCATION/$CROP2_WAR_NAME\_$TAG;
		cp -v $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$GOBII_WEB_WAR_HOTFIX_FILE_NAME ./$CROP2_WAR_NAME;";
        eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
fi

if [[ -v CROP3_WAR_NAME ]]; then
        DOCKER_CMD="cd $GOBII_WAR_LOCATION;
		cp -v $CROP3_WAR_NAME $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$ORIGINAL_FILE_BACKUP_LOCATION/$CROP3_WAR_NAME\_$TAG;
		cp -v $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$GOBII_WEB_WAR_HOTFIX_FILE_NAME ./$CROP3_WAR_NAME;";
        eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
fi

if [[ -v CROP4_WAR_NAME ]]; then
        DOCKER_CMD="cd $GOBII_WAR_LOCATION;
		cp -v $CROP4_WAR_NAME $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$ORIGINAL_FILE_BACKUP_LOCATION/$CROP4_WAR_NAME\_$TAG;
		cp -v $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$GOBII_WEB_WAR_HOTFIX_FILE_NAME ./$CROP4_WAR_NAME;";
        eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
fi

if [[ -v CROP5_WAR_NAME ]]; then
        DOCKER_CMD="cd $GOBII_WAR_LOCATION;
		cp -v $CROP5_WAR_NAME $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$ORIGINAL_FILE_BACKUP_LOCATION/$CROP5_WAR_NAME\_$TAG;
		cp -v $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$GOBII_WEB_WAR_HOTFIX_FILE_NAME ./$CROP5_WAR_NAME;";
        eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
fi

if [[ -v CROP6_WAR_NAME ]]; then
        DOCKER_CMD="cd $GOBII_WAR_LOCATION;
		cp -v $CROP6_WAR_NAME $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$ORIGINAL_FILE_BACKUP_LOCATION/$CROP6_WAR_NAME\_$TAG;
		cp -v $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$GOBII_WEB_WAR_HOTFIX_FILE_NAME ./$CROP6_WAR_NAME;";
        eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
fi

#-----------------------------------------------------------------------------#

docker exec -i -u gadm gobii-web-node bash -c "
/usr/local/tomcat/bin/shutdown.sh;
sleep 3;
/usr/local/tomcat/bin/startup.sh;
sleep 3
exit;
";

#-----------------------------------------------------------------------------#

