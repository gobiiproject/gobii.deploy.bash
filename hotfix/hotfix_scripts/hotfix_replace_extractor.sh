#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
#Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "Not Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "Not Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set Environment for shell
set -e # Abort script at first error, when a command exits with non-zero status
set -u # Forces exit when a variable is undefined
set -x # xtrace: Set High Verbosity

#-----------------------------------------------------------------------------#

TAG=$(date +"%Y%m%d_%H%M%S");

#-----------------------------------------------------------------------------#
# Create Hotfix download location and download files

docker exec -i -u gadm $DOCKER_WEB_NAME bash -c "
mkdir -pv $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$ORIGINAL_FILE_BACKUP_LOCATION ||true;
cd $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION; 
wget $EXTRACTOR_JAR_DOWNLOAD_URL;
";
echo;

#-----------------------------------------------------------------------------#

DOCKER_CMD="cd $EXTRACTOR_JAR_LOCATION;
cp -v $EXTRACTOR_JAR_NAME $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$ORIGINAL_FILE_BACKUP_LOCATION/$EXTRACTOR_JAR_NAME\_$TAG;
cp -v $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$EXTRACTOR_JAR_HOTFIX_FILE_NAME ./$EXTRACTOR_JAR_NAME;";
eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
echo;

#-----------------------------------------------------------------------------#

