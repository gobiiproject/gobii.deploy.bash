#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
#Author: RLPetrie (rlp243@cornell.edu)

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "Not Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "Not Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set Environment for shell
set -e # Abort script at first error, when a command exits with non-zero status
set -u # Forces exit when a variable is undefined
set -x # xtrace: Set High Verbosity

#-----------------------------------------------------------------------------#
### Get, create & checkout repo

docker exec $DOCKER_WEB_NAME bash -c "rm -rfv /data/workspace/gobii.db/ || true;";
echo;

docker exec $DOCKER_WEB_NAME bash -c "mkdir -p /data/workspace || true;";
echo;

docker exec $DOCKER_WEB_NAME bash -c " cd /data/workspace; git clone --depth 1 https://bitbucket.org/gobiiproject/gobii.db.git -b $GOBII_DB_REPO_BRANCH";
echo;

docker exec $DOCKER_WEB_NAME bash -c "cd /data/workspace/gobii.db/; git status;";
echo;

#-----------------------------------------------------------------------------#
### Run liquibase to update schema of any database changes
#-----------------------------------------------------------------------------#
### UPDATE LIQUIBASE: This needs to be done here because the database docker doesn't have java by default.##
#-----------------------------------------------------------------------------#

# Crop1
docker exec $DOCKER_WEB_NAME bash -c "cd /data/workspace/gobii.db/builder/liquibase;
java -jar bin/liquibase.jar --username=${DB_USERNAME} --password=${DB_PASS} --url=jdbc:postgresql://${DOCKER_DB_HOST}:${DB_PORT}/${DB_NAME_CROP1} --driver=org.postgresql.Driver --classpath=drivers/postgresql-9.4.1209.jar --changeLogFile=changelogs/db.changelog-2.2.3.xml update;";
echo;

if [[ -v DOCKER_CROP2_NAME ]]; then
	docker exec $DOCKER_WEB_NAME bash -c "cd /data/workspace/gobii.db/builder/liquibase;
	java -jar bin/liquibase.jar --username=${DB_USERNAME} --password=${DB_PASS} --url=jdbc:postgresql://${DOCKER_DB_HOST}:${DB_PORT}/${DB_NAME_CROP2} --driver=org.postgresql.Driver --classpath=drivers/postgresql-9.4.1209.jar --changeLogFile=changelogs/db.changelog-2.2.3.xml update;";
fi
echo;

if [[ -v DOCKER_CROP3_NAME ]]; then
	docker exec $DOCKER_WEB_NAME bash -c "cd /data/workspace/gobii.db/builder/liquibase;
	java -jar bin/liquibase.jar --username=${DB_USERNAME} --password=${DB_PASS} --url=jdbc:postgresql://${DOCKER_DB_HOST}:${DB_PORT}/${DB_NAME_CROP3} --driver=org.postgresql.Driver --classpath=drivers/postgresql-9.4.1209.jar --changeLogFile=changelogs/db.changelog-2.2.3.xml update;";
fi
echo;

if [[ -v DOCKER_CROP4_NAME ]]; then
	docker exec $DOCKER_WEB_NAME bash -c "cd /data/workspace/gobii.db/builder/liquibase;
	java -jar bin/liquibase.jar --username=${DB_USERNAME} --password=${DB_PASS} --url=jdbc:postgresql://${DOCKER_DB_HOST}:${DB_PORT}/${DB_NAME_CROP4} --driver=org.postgresql.Driver --classpath=drivers/postgresql-9.4.1209.jar --changeLogFile=changelogs/db.changelog-2.2.3.xml update;";
fi
echo;

if [[ -v DOCKER_CROP5_NAME ]]; then
	docker exec $DOCKER_WEB_NAME bash -c "cd /data/workspace/gobii.db/builder/liquibase;
	java -jar bin/liquibase.jar --username=${DB_USERNAME} --password=${DB_PASS} --url=jdbc:postgresql://${DOCKER_DB_HOST}:${DB_PORT}/${DB_NAME_CROP5} --driver=org.postgresql.Driver --classpath=drivers/postgresql-9.4.1209.jar --changeLogFile=changelogs/db.changelog-2.2.3.xml update;";
fi
echo;

if [[ -v DOCKER_CROP6_NAME ]]; then
	docker exec $DOCKER_WEB_NAME bash -c "cd /data/workspace/gobii.db/builder/liquibase;
	java -jar bin/liquibase.jar --username=${DB_USERNAME} --password=${DB_PASS} --url=jdbc:postgresql://${DOCKER_DB_HOST}:${DB_PORT}/${DB_NAME_CROP6} --driver=org.postgresql.Driver --classpath=drivers/postgresql-9.4.1209.jar --changeLogFile=changelogs/db.changelog-2.2.3.xml update;";
fi
echo;

#-----------------------------------------------------------------------------#
### Cleaning up git repo

docker exec $DOCKER_WEB_NAME bash -c "rm -rfv /data/workspace/gobii.db/ || true;";
echo;