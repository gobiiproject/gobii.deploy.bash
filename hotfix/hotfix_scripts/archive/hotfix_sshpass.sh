#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
#@author: RLPetrie (rlp243@cornell.edu)

#-----------------------------------------------------------------------------# 

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#--------------------------------------#
### Set vars passed

# if [[ -n "${1}" ]]; then CONT_PASS="$1"; else read -sp "Please enter the Container gadm user password: " CONT_PASS; fi
# echo;

# if [[ -n "${2}" ]]; then HOST_PASS="$2"; else read -sp "Please enter the Host gadm user password: " HOST_PASS; fi
# echo;

#--------------------------------------#
### Install sshpass, gen key, pass key

# docker exec -u root gobii-web-node bash -c "
# apt install sshpass -y;
# " || true;
# echo;

# docker exec -u root gobii-db-node bash -c "
# apt install sshpass -y;
# " || true;
# echo;

# docker exec -u root gobii-compute-node bash -c "
# apt install sshpass -y;
# " || true;
# echo;

#--------------------------------------#
### gen key, pass keys

docker exec -u gadm gobii-web-node bash -c "
cd /home/gadm;
ssh-keygen -b 2048 -t rsa -f /home/gadm/.ssh/id_rsa -q -N \"\" || true;
" || true;
echo;

docker exec -u gadm gobii-compute-node bash -c "
cd /home/gadm;
ssh-keygen -b 2048 -t rsa -f /home/gadm/.ssh/id_rsa -q -N \"\"  || true;
" || true;
echo;

docker exec -u gadm gobii-db-node bash -c "
cd /home/gadm;
ssh-keygen -b 2048 -t rsa -f /home/gadm/.ssh/id_rsa -q -N \"\" || true;
" || true;
echo;

#--------------------------------------#
### gen key, pass keys

# docker exec -u gadm gobii-web-node bash -c "
# cd /home/gadm;
# sshpass -p $CONT_PASS ssh-copy-id gobii-db-node || true && sleep 3 && sshpass -p $CONT_PASS ssh-copy-id gobii-db-node || true && sleep 3 && sshpass -p $CONT_PASS ssh-copy-id gobii-db-node || true;
# sshpass -p $CONT_PASS ssh-copy-id gobii-compute-node || true && sleep 3 && sshpass -p $CONT_PASS ssh-copy-id gobii-compute-node || true && sleep 3 && sshpass -p $CONT_PASS ssh-copy-id gobii-compute-node || true;
# sshpass -p $HOST_PASS ssh-copy-id 172.17.0.1 || true && sleep 3 && sshpass -p $HOST_PASS ssh-copy-id 172.17.0.1 || true && sleep 3 && sshpass -p $HOST_PASS ssh-copy-id 172.17.0.1 || true;
# " || true;
# echo;

# docker exec -u gadm gobii-compute-node bash -c "
# cd /home/gadm;
# sshpass -p $CONT_PASS ssh-copy-id gobii-db-node || true && sleep 3;
# sshpass -p $CONT_PASS ssh-copy-id gobii-web-node || true && sleep 3;
# sshpass -p $HOST_PASS ssh-copy-id 172.17.0.1 || true && sleep 3;
# " || true;
# echo;

# docker exec -u gadm gobii-db-node bash -c "
# cd /home/gadm;
# sshpass -p $CONT_PASS ssh-copy-id gobii-web-node || true && sleep 3;
# sshpass -p $CONT_PASS ssh-copy-id gobii-compute-node || true && sleep 3;
# sshpass -p $HOST_PASS ssh-copy-id 172.17.0.1 || true && sleep 3;
# " || true;
# echo;

#--------------------------------------#
### create keypass

KEYPASS=$(docker exec -u gadm gobii-web-node bash -c "cd /home/gadm && cat .ssh/id_rsa.pub")
echo;

#--------------------------------------#
### add keypass to containers

docker exec -u gadm gobii-compute-node bash -c "
cd /home/gadm;
echo $KEYPASS >> .ssh/authorized_keys;
";
echo;

docker exec -u gadm gobii-db-node bash -c "
cd /home/gadm;
echo $KEYPASS >> .ssh/authorized_keys;
";
echo;

