#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
#Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "Not Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "Not Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set Environment for shell
set -e # Abort script at first error, when a command exits with non-zero status
set -u # Forces exit when a variable is undefined
set -x # xtrace: Set High Verbosity

#-----------------------------------------------------------------------------#

TAG=$(date +"%Y%m%d_%H%M%S");

#-----------------------------------------------------------------------------#
# Create Hotfix download location and download files

docker exec -i -u gadm $DOCKER_WEB_NAME bash -c "
mkdir -pv $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$ORIGINAL_FILE_BACKUP_LOCATION ||true;
cd $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION; 
wget $TIMESCOPE_WAR_DOWNLOAD_URL;
";
echo;

#-----------------------------------------------------------------------------#
# backup original

DOCKER_CMD="cd $GOBII_WAR_LOCATION;
cp -v $TIMESCOPE_WAR_NAME $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$ORIGINAL_FILE_BACKUP_LOCATION/$TIMESCOPE_WAR_NAME\_$TAG;
cp -v $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$DATATIMESCOPE_WAR_HOTFIX_FILE_NAME ./$TIMESCOPE_WAR_NAME;";
eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
echo;

#-----------------------------------------------------------------------------#
# restart tomcat

echo "Restarting tomcat..."
echo;

docker exec -i -u gadm $DOCKER_WEB_NAME bash -c "
/usr/local/tomcat/bin/shutdown.sh;
sleep 3;
/usr/local/tomcat/bin/startup.sh;
sleep 3
exit;
";
echo;

#-----------------------------------------------------------------------------#
# create config.properties file and replace existing

set +x # reduce verbosity to hide password
docker exec -i -u gadm $DOCKER_WEB_NAME bash -c "
touch $BUNDLE_PARENT_PATH/config.properties;
echo '# Timescope db credentials' >> $BUNDLE_PARENT_PATH/config.properties;
echo 'db.username=timescoper' >> $BUNDLE_PARENT_PATH/config.properties;
echo 'db.pw=$TIMESCOPER_PASS' >> $BUNDLE_PARENT_PATH/config.properties
echo 'version=$TIMESCOPER_VERSION' >> $BUNDLE_PARENT_PATH/config.properties;
mv -v $BUNDLE_PARENT_PATH/config.properties $TIMESCOPER_CONFIG_LOCATION/config.properties;
";
echo;
set -x

#-----------------------------------------------------------------------------#
# restart tomcat

echo "Restarting tomcat..."
echo;

docker exec -i -u gadm $DOCKER_WEB_NAME bash -c "
/usr/local/tomcat/bin/shutdown.sh;
sleep 3;
/usr/local/tomcat/bin/startup.sh;
sleep 3
exit;
";
echo;

#-----------------------------------------------------------------------------#

