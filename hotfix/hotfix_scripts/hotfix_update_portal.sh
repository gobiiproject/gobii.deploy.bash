#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
#Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "No Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "No Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set Environment for shell
set -e # Abort script at first error, when a command exits with non-zero status
set -u # Forces exit when a variable is undefined
set -x # xtrace: Set High Verbosity

#-----------------------------------------------------------------------------#

TAG=$(date +"%Y%m%d_%H%M%S");

#-----------------------------------------------------------------------------#
# copy files from repo

mkdir -pv $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$ORIGINAL_FILE_BACKUP_LOCATION ||true;
cp -rv hotfix/release222_hotfix/* $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION;

#-----------------------------------------------------------------------------#
# backup old launchers.xml config

docker exec $DOCKER_WEB_NAME bash -c "
cd $PORTAL_CONFIG_LOCATION && cp -rv $PORTAL_CONFIG_LOCATION/$PORTAL_CONFIG_FILE_NAME $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$ORIGINAL_FILE_BACKUP_LOCATION/$PORTAL_CONFIG_FILE_NAME\_$TAG;
";

#-----------------------------------------------------------------------------#
# copy over all new icons for config access

docker exec $DOCKER_WEB_NAME bash -c "
cd $PORTAL_CONFIG_LOCATION && cp -rv $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/img .;
";

#-----------------------------------------------------------------------------#
### marker portal update

cd $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION;

if [[ -v DOCKER_CROP2_NAME ]]; then

        sed -i -e '/<!-- Crop 2 Link and Documentation -->/{r $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/crop_launcher.tmp' -e "d}" $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$PORTAL_CONFIG_FILE_NAME && sed -i 's/\r//' $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$PORTAL_CONFIG_FILE_NAME
fi

if [[ -v DOCKER_CROP3_NAME ]]; then

		sed -i -e '/<!-- Crop 3 Link and Documentation -->/{r $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/crop_launcher.tmp' -e "d}" $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$PORTAL_CONFIG_FILE_NAME && sed -i 's/\r//' $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$PORTAL_CONFIG_FILE_NAME
fi

if [[ -v DOCKER_CROP4_NAME ]]; then

		sed -i -e '/<!-- Crop 4 Link and Documentation -->/{r $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/crop_launcher.tmp' -e "d}" $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$PORTAL_CONFIG_FILE_NAME && sed -i 's/\r//' $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$PORTAL_CONFIG_FILE_NAME
fi

if [[ -v DOCKER_CROP5_NAME ]]; then

		sed -i -e '/<!-- Crop 5 Link and Documentation -->/{r $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/crop_launcher.tmp' -e "d}" $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$PORTAL_CONFIG_FILE_NAME && sed -i 's/\r//' $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$PORTAL_CONFIG_FILE_NAME
fi

if [[ -v DOCKER_CROP6_NAME ]]; then

		sed -i -e '/<!-- Crop 6 Link and Documentation -->/{r $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/crop_launcher.tmp' -e "d}" $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$PORTAL_CONFIG_FILE_NAME && sed -i 's/\r//' $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$PORTAL_CONFIG_FILE_NAME
fi

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cp -rv $BUNDLE_PARENT_PATH/$HOTFIX_FILE_LOCATION/$PORTAL_CONFIG_FILE_NAME $PORTAL_CONFIG_LOCATION/$PORTAL_CONFIG_FILE_NAME
";

#-----------------------------------------------------------------------------#

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_CROP1_URL_XPATH\" \
-r \"$PORTAL_CROP1_URL\" \
-v;
";

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_CROP1_NAME_XPATH\" \
-r \"$PORTAL_CROP1_NAME\" \
-v;
";

# if crop # exisits add and update portal config
if [[ -v DOCKER_CROP2_NAME ]]; then

        docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP2_URL\" \
		-v;
		';

		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP2_NAME\" \
		-v;
		';

fi

#-----------------------------------------------------------------------------#

if [[ -v DOCKER_CROP3_NAME ]]; then

		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP3_URL\" \
		-v;
		';

		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP3_NAME\" \
		-v;
		';

fi

#-----------------------------------------------------------------------------#

if [[ -v DOCKER_CROP4_NAME ]]; then

		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP4_URL\" \
		-v;
		';

		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP4_NAME\" \
		-v;
		';

fi

#-----------------------------------------------------------------------------#

if [[ -v DOCKER_CROP5_NAME ]]; then

		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP5_URL\" \
		-v;
		';

		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP5_NAME\" \
		-v;
		';

fi

#-----------------------------------------------------------------------------#

if [[ -v DOCKER_CROP6_NAME ]]; then

		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP6_URL\" \
		-v;
		';

		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP6_NAME\" \
		-v;
		';

fi

#-----------------------------------------------------------------------------#

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_TIMESCOPE_URL_XPATH\" \
-r \"$PORTAL_TIMESCOPE_URL\" \
-v;
";

if [[ -v PORTAL_OWNCLOUD_URL_XPATH ]]; then
docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_OWNCLOUD_URL_XPATH\" \
-r \"$PORTAL_OWNCLOUD_URL\" \
-v;
";
fi

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_KDC_XPATH\" \
-r \"$PORTAL_KDC_URL\" \
-v;
";

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_LOADER_URL_XPATH\" \
-r \"$PORTAL_LOADER_URL\" \
-v;
";

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_HAPLOTOOL_URL_XPATH\" \
-r \"$PORTAL_HAPLOTOOL_URL\" \
-v;
";

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_PEDVER_URL_XPATH\" \
-r \"$PORTAL_PEDVER_URL\" \
-v;
";

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_FLAPJACK_BYTZ_URL_XPATH\" \
-r \"$PORTAL_FLAPJACK_BYTZ_URL\" \
-v;
";