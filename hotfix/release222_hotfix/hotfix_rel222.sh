#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Set Environment for shell
set -e # Abort script at first error, when a command exits with non-zero status
set -u # Forces exit when a variable is undefined
set -x # xtrace: Set High Verbosity

#-----------------------------------------------------------------------------#
#Author: RLPetrie (rlp243@cornell.edu)

#-----------------------------------------------------------------------------#
### Error on running with sudo

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

#-----------------------------------------------------------------------------#
### verify proper arguments are passed

if [ $# -lt 3 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash <bash_script_name>.sh <path of gobii.parameters> <dockerhubpassw "
    echo "Set dockerhubpassw parameter to 'askme' for the script to prompt for password instead."
    exit 1
fi

#-----------------------------------------------------------------------------#
### load parameters -- gobii.parameters for deployment
#-----------------------------------------------------------------------------#

set +x

source $1

#-----------------------------------------------------------------------------#
### Set release version and Dockerhub password
###>>> if pass set to 'askme' call for user to enter password at cli
#-----------------------------------------------------------------------------#

DOCKER_HUB_PASSWORD=$2

#-----------------------------------------------------------------------------#
### Requesting passwords [Dockerhub]
#-----------------------------------------------------------------------------#
###>>> By default the same password will be set for all administration and 
###>>> gadm users. If a different password is needed, uncomment the 'if' ###>>> statements below to interactively enter the different passwords
#-----------------------------------------------------------------------------#

echo;

if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
fi
echo;

set -x

PWD="`pwd`" && echo "Present Working Directory: $PWD"
echo;

#-----------------------------------------------------------------------------#
### Display hotfix solutions

clear

echo "
This hotfix completes the following:
• Downloads Artifacts from bitbucket downloads: [ https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/ ]
    ↳ gobii-web.war_222hotfix_20200812
	↳ Extractor.jar_222hotfix_20200812
	↳ Digester.jar_222hotfix_20200812
	↳ datatimescope.war_222hotfix_20200812
"


read -p "Please press any key to continue... " -n1 -s
echo;

mkdir -p /data/hotfix/release222_hotfix_files
echo;

cp -rv img/ /data/hotfix/release222_hotfix_files; 
cp -rv launchers.xml /data/hotfix/release222_hotfix_files;
cp -rv crop_launcher.tmp /data/hotfix/release222_hotfix_files;
echo;

cd /data/hotfix/release_222_hotfix_files; wget \
https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/gobii-web.war_222hotfix_20200812 \
https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/Extractor.jar_222hotfix_20200812 \
https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/Digester.jar_222hotfix_20200812 \
https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/datatimescope.war_222hotfix_20200812;
echo;

docker exec -i -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/core/;
mv timescope.war timescope.war_bkup_222hotfix_20200812_1526; 
cp /data/hotfix/rel222_hotfix_files/datatimescope.war_222hotfix_20200812 ./timescope.war; 
mv gobii-obii-dev.war gobii-dev.war_bkup_222hotfix_20200812_1526; 
cp /data/hotfix/rel222_hotfix_files/gobii-web.war_222hotfix_20200812 ./gobii-dev.war;
cd /data/gobii_bundle/core/; mv Digester.jar Digesterbkup_.jar_bkup_222hotfix_20200812_1526; 
cp /data/hotfix/rel222_hotfix_files/Digester.jar_222hotfix_20200812 ./Digester.jar; 
mv Extractor.jar Extractor.jar_bkup_222hotfix_20200812_1526; 
cp /data/hotfix/rel222_hotfix_files/Extractor.jar_222hotfix_20200812 ./Extractor.jar; 
";
echo;

#-----------------------------------------------------------------------------#
### 

#!!! The following process must be run after timescope deploy as user/pass is 
#!!! overwritten after the new war is deployed
#-----------------------------------------------------------------------------#
### Updating Timescoper Configuration
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Updating Timescoper Configuration..."
echo;

### Requesting passwords [Timescoper User]

echo;

if [ $TIMESCOPER_PASS = "askme" ]; then
    read -sp "Please enter the timescoper user password: " TIMESCOPER_PASS
fi
echo;

set -x

touch $BUNDLE_PARENT_PATH/config.properties;
echo "# Timescope db credentials" > $BUNDLE_PARENT_PATH/config.properties;
echo "db.username=timescoper" >> $BUNDLE_PARENT_PATH/config.properties;
echo "db.pw=$TIMESCOPER_PASS" >> $BUNDLE_PARENT_PATH/config.properties
echo "version=$TIMESCOPER_VERSION" >> $BUNDLE_PARENT_PATH/config.properties;

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /usr/local/tomcat/webapps/timescope/WEB-INF/classes;
mv /data/config.properties .
";
echo;


docker exec -i -u gadm $DOCKER_WEB_NAME bash -c "
/usr/local/tomcat/bin/shutdown.sh;
sleep 3;
/usr/local/tomcat/bin/startup.sh;
sleep 3
exit;
";

#-----------------------------------------------------------------------------#
### marker portal update

docker exec $DOCKER_WEB_NAME bash -c "
cd /usr/local/tomcat/webapps/gobii-portal/config/img/ && cp /data/hotfix/release222_hotfix_files/img/* .;
";

cd /data/hotfix/release222_hotfix_files/;

if [[ -v DOCKER_CROP2_NAME ]]; then

        sed -i -e '/<!-- Crop 2 Link and Documentation -->/{r /data/hotfix/release222_hotfix_files/crop_launcher.tmp' -e "d}" /data/hotfix/release222_hotfix_files/launchers.xml && sed -i 's/\r//' /data/hotfix/release222_hotfix_files/launchers.xml
fi

if [[ -v DOCKER_CROP3_NAME ]]; then

		sed -i -e '/<!-- Crop 3 Link and Documentation -->/{r /data/hotfix/release222_hotfix_files/crop_launcher.tmp' -e "d}" /data/hotfix/release222_hotfix_files/launchers.xml && sed -i 's/\r//' /data/hotfix/release222_hotfix_files/launchers.xml
fi

if [[ -v DOCKER_CROP4_NAME ]]; then

		sed -i -e '/<!-- Crop 4 Link and Documentation -->/{r /data/hotfix/release222_hotfix_files/crop_launcher.tmp' -e "d}" /data/hotfix/release222_hotfix_files/launchers.xml && sed -i 's/\r//' /data/hotfix/release222_hotfix_files/launchers.xml
fi

if [[ -v DOCKER_CROP5_NAME ]]; then

		sed -i -e '/<!-- Crop 5 Link and Documentation -->/{r /data/hotfix/release222_hotfix_files/crop_launcher.tmp' -e "d}" /data/hotfix/release222_hotfix_files/launchers.xml && sed -i 's/\r//' /data/hotfix/release222_hotfix_files/launchers.xml
fi

if [[ -v DOCKER_CROP6_NAME ]]; then

		sed -i -e '/<!-- Crop 6 Link and Documentation -->/{r /data/hotfix/release222_hotfix_files/crop_launcher.tmp' -e "d}" /data/hotfix/release222_hotfix_files/launchers.xml && sed -i 's/\r//' /data/hotfix/release222_hotfix_files/launchers.xml
fi

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cp /data/hotfix/release222_hotfix_files/launchers.xml /usr/local/tomcat/webapps/gobii-portal/config/launchers.xml
";

#-----------------------------------------------------------------------------#

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_CROP1_URL_XPATH\" \
-r \"$PORTAL_CROP1_URL\" \
-v;
";

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_CROP1_NAME_XPATH\" \
-r \"$PORTAL_CROP1_NAME\" \
-v;
";

# if crop # exisits add and update portal config
if [[ -v DOCKER_CROP2_NAME ]]; then

        docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP2_URL\" \
		-v;
		';
		
		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP2_NAME\" \
		-v;
		';

fi

#-----------------------------------------------------------------------------#

if [[ -v DOCKER_CROP3_NAME ]]; then

		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP3_URL\" \
		-v;
		';
		
		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP3_NAME\" \
		-v;
		';

fi

#-----------------------------------------------------------------------------#

if [[ -v DOCKER_CROP4_NAME ]]; then

		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP4_URL\" \
		-v;
		';
		
		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP4_NAME\" \
		-v;
		';

fi

#-----------------------------------------------------------------------------#

if [[ -v DOCKER_CROP5_NAME ]]; then

		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP5_URL\" \
		-v;
		';
		
		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP5_NAME\" \
		-v;
		';

fi

#-----------------------------------------------------------------------------#

if [[ -v DOCKER_CROP6_NAME ]]; then

		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP6_URL\" \
		-v;
		';
		
		docker exec -u gadm $DOCKER_WEB_NAME bash -c '
		cd /data/gobii_bundle/config/utils; \
		python3.6 xml_config_parser.py \
		-f $PORTAL_CONFIG_PATH \
		-x \".//launcher[name='goodberry']/name" \
		-r \"$PORTAL_CROP6_NAME\" \
		-v;
		';

fi

#-----------------------------------------------------------------------------#

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_TIMESCOPE_URL_XPATH\" \
-r \"$PORTAL_TIMESCOPE_URL\" \
-v;
";

if [[ -v PORTAL_OWNCLOUD_URL_XPATH ]]; then
docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_OWNCLOUD_URL_XPATH\" \
-r \"$PORTAL_OWNCLOUD_URL\" \
-v;
";  
fi 

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_KDC_XPATH\" \
-r \"$PORTAL_KDC_URL\" \
-v;
";

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_LOADER_URL_XPATH\" \
-r \"$PORTAL_LOADER_URL\" \
-v;
";

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_HAPLOTOOL_URL_XPATH\" \
-r \"$PORTAL_HAPLOTOOL_URL\" \
-v;
";

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_PEDVER_URL_XPATH\" \
-r \"$PORTAL_PEDVER_URL\" \
-v;
";

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /data/gobii_bundle/config/utils; \
python3.6 xml_config_parser.py \
-f $PORTAL_CONFIG_PATH \
-x \"$PORTAL_FLAPJACK_BYTZ_URL_XPATH\" \
-r \"$PORTAL_FLAPJACK_BYTZ_URL\" \
-v;
";

#-----------------------------------------------------------------------------#
### Redeploy Pedver

# get back to gobii.deploy.bash/
cd $PWD; cd ../../;

# pedver
TAG=$(date +"%Y%m%d_%H%M%S"); time bash container_scripts/the_gobii_ship_pedver.sh ../gobii.parameters $DOCKER_HUB_PASSWORD rel2.2.2 2>&1 |& tee -a logs/deployment_logs/pedver_$TAG.log