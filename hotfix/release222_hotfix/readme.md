```bash
   ___  ___  ___ _ _   ___           _           
  / __|/ _ \| _ |_|_) |   \ ___ _ __| |___ _  _  
 | (_ | (_) | _ \ | | | |) / -_) '_ \ / _ \ || | 
  \___|\___/|___/_|_| |___/\___| .__/_\___/\_, | 
  ___          _      _  _     |_|  __ _   |__/  
 | _ ) __ _ __| |_   | || |___| |_ / _(_)_ __    
 | _ \/ _` (_-< ' \  | __ / _ \  _|  _| \ \ /    
 |___/\__,_/__/_||_| |_||_\___/\__|_| |_/_\_\    
                                                 
```



# GOBii Release 2.2.2 Hotfix

This hotfix performs the following:

- Replaces: Digester.jar & Extractor.jar
- Replaces: <crop_name>.war
- Replaces: timescope.war
- Redeploys newest version of PedVer
- Updates Portal: launchers.xml



---



### **Variables**

These are the unique variables for this deployment needed in order to perform the update via script or deployment tools. (Jenkins, Bamboo, etc.)

#### **BRANCH**
```bash
BRANCH="release/2.2.2"
```

#### _Global_
```bash
DOCKER_WEB_NAME="gobii-web-node"
GOBII_WAR_LOCATION="/usr/local/tomcat/webapps/"
HOTFIX_FILE_LOCATION="hotfix/release222_hotfix_files"
ORIGINAL_FILE_BACKUP_LOCATION="hotfix_origbkupFiles/"
TIMESCOPE_WAR_NAME="timescope.war"
TIMESCOPER_CONFIG_LOCATION="/usr/local/tomcat/webapps/timescope/WEB-INF/classes"
PORTAL_CONFIG_LOCATION="/usr/local/tomcat/webapps/gobii-portal/config"
PORTAL_CONFIG_FILE_NAME="launchers.xml"
DIGESTER_JAR_NAME="Digester.jar"
EXTRACTOR_JAR_NAME="Extractor.jar"
PEDVER_DOCKER_NET="pedver_net"
```

#### Digester.jar
```bash
DIGESTER_JAR_DOWNLOAD_URL="https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/Digester.jar_222hotfix_20200812"
DIGESTER_JAR_HOTFIX_FILE_NAME="Digester.jar_222hotfix_20200812"
DIGESTER_JAR_LOCATION="/data/gobii_bundle/core"
```

#### Extractor.jar
```bash
EXTRACTOR_JAR_DOWNLOAD_URL="https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/Extractor.jar_222hotfix_20200812"
EXTRACTOR_JAR_LOCATION="/data/gobii_bundle/core"
EXTRACTOR_JAR_HOTFIX_FILE_NAME="Extractor.jar_222hotfix_20200812"
```

#### gobii-web.war
```bash
GOBII_WAR_DOWNLOAD_URL="https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/gobii-web.war222_b3_hotfix_20200825"
GOBII_WEB_WAR_HOTFIX_FILE_NAME="gobii-web.war222_b3_hotfix_20200825"
CROP1_WAR_NAME="gobii-<name>.war"
#CROP2_WAR_NAME="<crop2_name>.war"
#CROP3_WAR_NAME="<crop3_name>.war"
#CROP4_WAR_NAME="<crop4_name>.war"
#CROP5_WAR_NAME="<crop5_name>.war"
#CROP6_WAR_NAME="<crop6_name>.war"
```

#### datatimescope.war
```bash
TIMESCOPE_WAR_DOWNLOAD_URL="https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/datatimescope.war_222hotfix_20200812"
DATATIMESCOPE_WAR_HOTFIX_FILE_NAME="datatimescope.war_222hotfix_20200812"
TIMESCOPER_VERSION="2.2.2"
TIMESCOPER_PASS="<Enter_Pass_or_Source_gobii.parameters>"
```

#### launchers.xml (GOBii Portal)
```bash
### No Unique non-Global Vars needed
```

#### PedVer Re-Deploy
```bash
DOCKER_HUB_PASSWORD="<Enter_Pass_or_Source_gobii.parameters>"
PEDVER_RELEASE_TAG="rel2.2.2"
PEDVER_WEB_PORT="<7000>"
DEPLOY_BRANCH="release/2.2.2"
```


---



## **Manual Steps**
_This process will describe how to perform the changes via the commandline._

[1] Create hotfix location and download files:
```bash
mkdir -p /data/hotfix/release222_hotfix_files;
cd /data/hotfix/release222_hotfix_files;
wget \
https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/gobii-web.war_222hotfix_20200812 \
https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/Extractor.jar_222hotfix_20200812 \
https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/Digester.jar_222hotfix_20200812 \
https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/datatimescope.war_222hotfix_20200812 \
https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/launchers.xml;
```




[2] Replace jars via gobii-web-node:
```bash
# Access web-node
docker exec -u gadm -ti gobii-web-node bash;

# Create $TAG for backup files
TAG=$(date +"%Y%m%d_%H%M%S");

# Create Backup file location
mkdir -p /data/hotfix/release222_hotfix_files/hotfix_origbkupFiles/

# Navigate to .jar and backup
cd /data/gobii_bundle/core
cp Digester.jar /data/hotfix/release222_hotfix_files/hotfix_origbkupFiles/Digester.jar\_$TAG;
cp Extractor.jar /data/hotfix/release222_hotfix_files/hotfix_origbkupFiles/Extractor.jar\_$TAG;

# Replace .jar with newly downloaded .jar
cp /data/hotfix/release222_hotfix_files/Digester.jar_222hotfix_20200812 /data/gobii_bundle/core/Digester.jar;
cp /data/hotfix/release222_hotfix_files/Extractor.jar_222hotfix_20200812 /data/gobii_bundle/core/Extractor.jar;
```




[3] Replace crop wars via gobii-web-node:
```bash
# Access web-node
docker exec -u gadm -ti gobii-web-node bash;

# Create $TAG for backup files
TAG=$(date +"%Y%m%d_%H%M%S");

# Create Backup file location
mkdir -p /data/hotfix/release222_hotfix_files/hotfix_origbkupFiles/

# Navigate to .war and backup
### ! This step needs to be done for each crop
### ! <crop#_name> must be replaced
cd /usr/local/tomcat/webapps/;
cp /usr/local/tomcat/webapps/<crop1_name>.war /data/hotfix/release222_hotfix_files/hotfix_origbkupFiles/<crop1_name>.war\_$TAG;
#cp /usr/local/tomcat/webapps/<crop2_name>.war /data/hotfix/release222_hotfix_files/hotfix_origbkupFiles/<crop2_name>.war\_$TAG;
#cp /usr/local/tomcat/webapps/<crop3_name>.war /data/hotfix/release222_hotfix_files/hotfix_origbkupFiles/<crop3_name>.war\_$TAG;
#cp /usr/local/tomcat/webapps/<crop4_name>.war /data/hotfix/release222_hotfix_files/hotfix_origbkupFiles/<crop4_name>.war\_$TAG;
#cp /usr/local/tomcat/webapps/<crop5_name>.war /data/hotfix/release222_hotfix_files/hotfix_origbkupFiles/<crop5_name>.war\_$TAG;
#cp /usr/local/tomcat/webapps/<crop6_name>.war /data/hotfix/release222_hotfix_files/hotfix_origbkupFiles/<crop6_name>.war\_$TAG;

# Replace .war with newly downloaded .war
### ! This step needs to be done for each crop
### ! <crop#_name> must be replaced
cp /data/hotfix/release222_hotfix_files/gobii-web.war222_b3_hotfix_20200825 /usr/local/tomcat/webapps/<crop1_name>.war
#cp /data/hotfix/release222_hotfix_files/gobii-web.war222_b3_hotfix_20200825 /usr/local/tomcat/webapps/<crop2_name>.war
#cp /data/hotfix/release222_hotfix_files/gobii-web.war222_b3_hotfix_20200825 /usr/local/tomcat/webapps/<crop3_name>.war
#cp /data/hotfix/release222_hotfix_files/gobii-web.war222_b3_hotfix_20200825 /usr/local/tomcat/webapps/<crop4_name>.war
#cp /data/hotfix/release222_hotfix_files/gobii-web.war222_b3_hotfix_20200825 /usr/local/tomcat/webapps/<crop5_name>.war
#cp /data/hotfix/release222_hotfix_files/gobii-web.war222_b3_hotfix_20200825 /usr/local/tomcat/webapps/<crop6_name>.war

# Restart tomcat service
/usr/local/tomcat/bin/shutdown.sh;
sleep 3;
/usr/local/tomcat/bin/startup.sh;
```




[4] Replace timescope war via gobii-web-node:
```bash
# Access web-node
docker exec -u gadm -ti gobii-web-node bash;

# Create $TAG for backup files
TAG=$(date +"%Y%m%d_%H%M%S");

# Create Backup file location
mkdir -p /data/hotfix/release222_hotfix_files/hotfix_origbkupFiles/

# Navigate to .war and backup
cd /usr/local/tomcat/webapps/;
cp timescope.war /data/hotfix/release222_hotfix_files/hotfix_origbkupFiles/timescope.war\_$TAG;

# Replace .war with newly downloaded .war
cp /data/hotfix/release222_hotfix_files/datatimescope.war_222hotfix_20200812 /usr/local/tomcat/webapps/timescope.war

# Update config.properties
vi /usr/local/tomcat/webapps/timescope/WEB-INF/classes/config.properties;
### Replace each value to match and replacing the appropriate pass
db.username=timescoper
db.pw=<Timescoper_DB_Password>
version=2.2.2

# Restart tomcat service
/usr/local/tomcat/bin/shutdown.sh;
sleep 3;
/usr/local/tomcat/bin/startup.sh;
```




[5] Update backup portal config and copy over files
```bash
# Access web-node
docker exec -u gadm -ti gobii-web-node bash;

# Create $TAG for backup files
TAG=$(date +"%Y%m%d_%H%M%S");

# Create Backup file location
mkdir -p /data/hotfix/release222_hotfix_files/hotfix_origbkupFiles/

# Navigate to config.properties and backup
cp /usr/local/tomcat/webapps/gobii-portal/config/launchers.xml /usr/local/tomcat/webapps/gobii-portal/config/launchers.xml\_$TAG

# Copy all new icons into img/
cp -rv /path/to/gobii.deploy.bash/hotfix/release222_hotfix_files/img/* /usr/local/tomcat/webapps/gobii-portal/config/img/

# update portal config with default config
cat /data/hotfix/release222_hotfix_files/launchers.xml > /usr/local/tomcat/webapps/gobii-portal/config/launchers.xml
```




[6] Update Portal to reflect new config

```bash
# Access web-node
docker exec -u gadm -ti gobii-web-node bash;

# Edit tmp launchers
vi /usr/local/tomcat/webapps/gobii-portal/config/launchers.xml

# Update crop 1 to show as following but replacing values for <name> & <url> to match that of crop 1. Repeat for each crop by pasting into config under crop 1 and replacing values for <name> & <url>.

   <launcher>
		<name>dev</name>
		<url>http://localhost:8081/gobii-dev</url>
		<logo>extract.png</logo>
		<description>GDM Extractor for crop dev</description>
		<color>color-green</color>
		<category>Genotype Management</category>
		<type>Web Apps</type>
		<documentationList>
			<documentation>
				<displayName>GDM Extractor</displayName>
				<url>https://gobiiproject.atlassian.net/l/c/Bz31y18V</url>
			</documentation>
		</documentationList>
	</launcher>

# Update all url instances with the FQDN of the host the container resides on
# Search for localhost and replace by using the '/' command in vi to find each instance
```



[7] Perform PedVer Deployment
```bash
# create pedver docker network
docker network create pedver_net || true;

# Add all existing containers to the pedver network
docker network connect pedver_net gobii-haplo-node || true;
docker network connect pedver_net gobii-portainer-node || true;
docker network connect pedver_net gobii-oc-node || true;
docker network connect pedver_net gobii-kdc-node || true;
docker network connect pedver_net gobii-web-node || true;
docker network connect pedver_net gobii-db-node || true;
docker network connect pedver_net gobii-compute-node || true;
docker network connect pedver_net portainer-sherpa-node || true;

# Remove any pre-existing PedVer containers
docker rm -f gobiimarkerapp-worker gobiimarkerapp redis  || true;

# Delete any exisiting PedVer containers
docker rm -f gobii-pedver-web gobii-pedver-worker gobii-pedver-redis  || true;

# Remove any pre-existing rel2.2.2 PedVer images
docker rmi gadm01/gobii-pedver-web:rel2.2.2 gadm01/gobii-pedver-worker:rel2.2.2 gadm01/gobii-pedver-redis:rel2.2.2 || true;

# Set HOST_DATA_VOLUME var
HOST_DATA_VOLUME=/data/pedver

# Create PedVer config and data location if they don't already exist
mkdir /data/pedver || true;
mkdir /data/pedver/data || true;
mkdir /data/pedver/instance || true;

# Export CONTAINER_DATA_FOLDER & CONTAINER_INSTANCE_FOLDER location
export CONTAINER_DATA_FOLDER=/data/
export CONTAINER_INSTANCE_FOLDER=/gobiimarkerapp/instance/

# Log into Dockerhub with gadmreader
docker login -u <gadmreader> -p <gadmreader password>;

# Pull newest images
docker pull gadm01/gobii_pedver_redis:rel2.2.2
docker pull gadm01/gobii_pedver_web:rel2.2.2
docker pull gadm01/gobii_pedver_worker:rel2.2.2

# Run new image [redis]
docker run -dti \
--name gobii-pedver-redis \
-h pedver-redis \
-p 6379:6379 \
--restart=always \
--network=pedver_net \
gadm01/gobii_pedver_redis:rel2.2.2

# Copy config.ini from deploy repo files to PedVer instance directory
cp <gobii.deploy.bash>/devops_files/pedver/config.ini /data/pedver/instance/

# Set redis name to used by config.ini
REDIS_NAME="gobii-pedver-redis"

# Update config.ini
sed -ie "s~CELERY_BROKER_URL = redis://localhost:6379/0~CELERY_BROKER_URL = redis://$REDIS_NAME:6379/0~g" ${HOST_DATA_VOLUME}/instance/config.ini

sed -ie "s~CELERY_RESULT_BACKEND = redis://localhost:6379/0~CELERY_RESULT_BACKEND = redis://$REDIS_NAME:6379/0~g" ${HOST_DATA_VOLUME}/instance/config.ini

# Run image [web]
docker run -dti \
--name gobii-pedver-web \
-h pedver-web \
-p 7000:80 \
-v /data/pedver/data:/data/ \
-v /data/pedver/instance:/gobiimarkerapp/instance/ \
--restart=always \
--network=pedver_net \
gadm01/gobii_pedver_web:rel2.2.2

# Run image [worker]
docker run -dti \
--name gobii-pedver-worker \
-h gobii-pedver-worker \
-v /data/pedver/data:/data/ \
-v /data/pedver/instance:/gobiimarkerapp/instance/ \
--restart=always \
--network=pedver_net \
gadm01/gobii_pedver_worker:rel2.2.2

# Connect all PedVer Instances to the default bridge docker network
docker network connect bridge gobii-pedver-redis || true;
docker network connect bridge gobii-pedver-web || true;
docker network connect bridge gobii-pedver-worker || true;
```



---



## **Automated Steps [Scripts]**
_This process will describe how to perform the changes via scripts._

**All of the variables used for these scripts will be located at the top of this readme and will be sourced along with all variables already set for the environment in the gobii.parameters file used during the most recent deployment.**

[1] Create a file with all the variables displayed at the top.

- [**Recommendation**]: These variables might be used again but have been set for specific Release 2.2.2 use case. Make sure to use a specific file name to signify these are used only for release 2.2.2 (_Example_: rel222_hotfix.parameters)

```bash
# All Variables that can be copy and pasted into <named_file>

### BRANCH
BRANCH="release/2.2.2"

### Global
DOCKER_WEB_NAME="gobii-web-node"
GOBII_WAR_LOCATION="/usr/local/tomcat/webapps/"
HOTFIX_FILE_LOCATION="hotfix/release222_hotfix_files"
ORIGINAL_FILE_BACKUP_LOCATION="hotfix_origbkupFiles/"
TIMESCOPE_WAR_NAME="timescope.war"
TIMESCOPER_CONFIG_LOCATION="/usr/local/tomcat/webapps/timescope/WEB-INF/classes"
PORTAL_CONFIG_LOCATION="/usr/local/tomcat/webapps/gobii-portal/config"
PORTAL_CONFIG_FILE_NAME="launchers.xml"
DIGESTER_JAR_NAME="Digester.jar"
EXTRACTOR_JAR_NAME="Extractor.jar"
PEDVER_DOCKER_NET="pedver_net"

### Digester.jar
DIGESTER_JAR_DOWNLOAD_URL="https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/Digester.jar_222hotfix_20200812"
DIGESTER_JAR_HOTFIX_FILE_NAME="Digester.jar_222hotfix_20200812"
DIGESTER_JAR_LOCATION="/data/gobii_bundle/core"
### Extractor.jar
EXTRACTOR_JAR_DOWNLOAD_URL="https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/Extractor.jar_222hotfix_20200812"
EXTRACTOR_JAR_LOCATION="/data/gobii_bundle/core"
EXTRACTOR_JAR_HOTFIX_FILE_NAME="Extractor.jar_222hotfix_20200812"

### gobii-web.war
GOBII_WAR_DOWNLOAD_URL="https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/gobii-web.war222_b3_hotfix_20200825"
GOBII_WEB_WAR_HOTFIX_FILE_NAME="gobii-web.war222_b3_hotfix_20200825"
CROP1_WAR_NAME="gobii-<name>.war"
#CROP2_WAR_NAME="<crop2_name>.war"
#CROP3_WAR_NAME="<crop3_name>.war"
#CROP4_WAR_NAME="<crop4_name>.war"
#CROP5_WAR_NAME="<crop5_name>.war"
#CROP6_WAR_NAME="<crop6_name>.war"

### datatimescope.war
TIMESCOPE_WAR_DOWNLOAD_URL="https://bitbucket.org/gobiiproject/gobii.deploy.bash/downloads/datatimescope.war_222hotfix_20200812"
DATATIMESCOPE_WAR_HOTFIX_FILE_NAME="datatimescope.war_222hotfix_20200812"
TIMESCOPER_VERSION="2.2.2"
TIMESCOPER_PASS="<Enter_Pass_or_Source_gobii.parameters>"

### launchers.xml (GOBii Portal)
### ! No Unique non-Global Vars needed

### PedVer Re-Deploy
DOCKER_HUB_PASSWORD="<Enter_Pass_or_Source_gobii.parameters>"
PEDVER_RELEASE_TAG="rel2.2.2"
PEDVER_WEB_PORT="<7000>"
DEPLOY_BRANCH="release/2.2.2"

```

[2] Run the deployment scripts.
```bash
cd /path/to/gobii.deploy.bash/ && \
bash hotfix/hotfix_scripts/hotfix_replace_digester.sh /path/to/rel222_hotfix.parameters /path/to/gobii.parameters

cd /path/to/gobii.deploy.bash/ && \
bash hotfix/hotfix_scripts/hotfix_replace_extractor.sh /path/to/rel222_hotfix.parameters /path/to/gobii.parameters

cd /path/to/gobii.deploy.bash/ && \
bash hotfix/hotfix_scripts/hotfix_replace_crop_war.sh /path/to/rel222_hotfix.parameters /path/to/gobii.parameters

cd /path/to/gobii.deploy.bash/ && \
bash hotfix/hotfix_scripts/hotfix_replace_datatimescope_war.sh /path/to/rel222_hotfix.parameters /path/to/gobii.parameters

cd /path/to/gobii.deploy.bash/ && \
bash hotfix/hotfix_scripts/hotfix_update_portal.sh  /path/to/rel222_hotfix.parameters /path/to/gobii.parameters

cd /path/to/gobii.deploy.bash/ && \
bash container_scripts/jenkins_deploy/pedver_deploy.sh /path/to/rel222_hotfix.parameters /path/to/gobii.parameters
```


---


## **Automated Steps [Deploy Tool]** 


⚠️ **_THIS IS NOT RECOMMENDED UNLESS YOU ARE A DevOps Engineer OR HAVE ASSISTANCE FROM A DevOps Engineer WHOM IS FAMILIAR WITH CI/CD PIPELINE TOOLS!!!_**


_This process will describe how to perform the changes via deployment tools. In this instance it will use Jenkins but the same processes can be used in any deployment tool and reused._

The purpose for using a deployment tool to perform these hot fixes is recommended as any new hotfixes would be using the same set of scripts with new variables.  This way the process never changes only the called file names for download and newest images. 


[1] Create a project/deployment or what name is used to trigger an automated deployment using the deployment tool this is setting up on.

As shown in this image there are 6 different projects that call each of the 6 different scripts for the hotfix.

**Note:** _A singular project/build/deployment can be used to call all the scripts in serial order as long as all the variables are set and passed into the scripts._

![Creating Deployments](readme_img/Jenkins_Projects_Screenshot.jpg)


[2] Set the parameters as listed at the top of the readme to be used on calling the script. If you have a global setting within the tool, the 'Global' vars can be set there as these variables will never change.

![Setting Deployment Parameters](readme_img/Jenkins_Vars_Screenshot.jpg)



[3] Set the deployment to pull/clone the gobii.deploy.bash repo. In this image it uses a variable parameter set within the deployment for:
```bash
BRANCH="release/2.2.2"
```

![Defining Repo](readme_img/Jenkins_repo_Screenshot.jpg)


[4] (_*Optional_ **Yet Recommended** ): Cleaning out the working environment.  This usually helps to make sure the proper git process is used with the correct repo/branch for the deployment.

Set the workspace to be deleted before deployment starts.

![Delete Workspace](readme_img/Jenkins_deleteWorkspace_Screenshot.jpg)


[5] Use a shell or remote call to execute the script with the configured parameters.

- Digester.jar
```bash
bash hotfix/hotfix_scripts/hotfix_replace_digester.sh
```
- Extractor.jar
```bash
bash hotfix/hotfix_scripts/hotfix_replace_extractor.sh
```

- <crop_name.war>
```bash
bash hotfix/hotfix_scripts/hotfix_replace_crop_war.sh
```

- timescope.war
```bash
bash hotfix/hotfix_scripts/hotfix_replace_datatimescope_war.sh
```

- launchers.xml
```bash
bash hotfix/hotfix_scripts/hotfix_update_portal.sh
```

- PedVer ReDeploy
```bash
bash container_scripts/jenkins_deploy/pedver_deploy.sh
```

This is how the build project looks calling the bash execution for the Digester.jar.

![Bash Calling](readme_img/Jenkins_bash_Screenshot.jpg)

#### ⚠️ **Attention: This can be done in many ways. This instance of Jenkins exists within the target host and deploys the containers next to itself.**


---

## [GOBii Jenkins](https://bitbucket.org/gobiiproject/gobii.jenkins/src/b4c3d8960fbc7925e463f8b02d18d1e3ee41ce0e/?at=release%2F2.2.2)

A current Version of the [gobii.jenkins](https://bitbucket.org/gobiiproject/gobii.jenkins/src/b4c3d8960fbc7925e463f8b02d18d1e3ee41ce0e/?at=release%2F2.2.2) repo exists for [release/2.2.2](https://bitbucket.org/gobiiproject/gobii.jenkins/src/b4c3d8960fbc7925e463f8b02d18d1e3ee41ce0e/?at=release%2F2.2.2). 

[1] To run jenkins on an environment clone the repo.
```bash
git clone https://bitbucket.org/gobiiproject/gobii.jenkins.git
```

[2] Navigate to repo directory, checkout out release/2.2.2
```bash
cd /path/to/gobii.jenkins && git checkout release/2.2.2
```

[3] Run Jenkins
```bash
# Replace the <NDD_PATH> as the container needs access to this location
# Replace the <JENKINS_BROWSER_PORT> with the port desired to access Jenkins via web browser

bash jenkins-run.sh <NDD_PATH> <JENKINS_BROWSER_PORT> develop_20200826_build_21
```

