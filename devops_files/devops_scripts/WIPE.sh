#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### GDM KEEP/WIPE
#-----------------------------------------------------------------------------#
# This script is intended to verify all the data is kept or destroyed on a 
# deployment. It is important that a system is not partially wiped by removing 
# files but keeping DB data.
#-----------------------------------------------------------------------------#
### @author: (rpetrie) rlp243@cornell.edu
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
### Environmental Settings
set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
#set -x # xtrace: Similar to #-v, but expands commands [to unset and hide 
# passwords us "set +x"]
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
### Error on running with sudo

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

if [ $# -lt 1 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash <bash_script_name>.sh <path of main.parameters> <path of install.parameters> <dockerhubpassw | \"askme\"> <gobii_release_version>"
    echo "Set dockerhubpassw parameter to 'askme' for the script to prompt for password instead."
    exit 1
fi



#-----------------------------------------------------------------------------#
### load parameters -- main.parameters for deployment
source $1
#-----------------------------------------------------------------------------#


clear

read -p "Would you like to perform a wipe of the GOBii data? [y/n]: " confirm1
echo;

# First Confirmation

if [ $confirm1 == 'y' ]; then
	clear
	echo "	
###############################################################################
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
#!                                                                           !#
#!           #     #    #    ######  #     # ### #     #  #####              !#
#!           #  #  #   # #   #     # ##    #  #  ##    # #     #             !#
#!           #  #  #  #   #  #     # # #   #  #  # #   # #                   !#
#!           #  #  # #     # ######  #  #  #  #  #  #  # #  ####             !#
#!           #  #  # ####### #   #   #   # #  #  #   # # #     #             !#
#!           #  #  # #     # #    #  #    ##  #  #    ## #     #             !#
#!            ## ##  #     # #     # #     # ### #     #  #####              !#
#!                                                                           !#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
###############################################################################
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
#!                                                                           !#
#!       #    ####### ####### ####### #     # ####### ### ####### #     #    !#
#!      # #      #       #    #       ##    #    #     #  #     # ##    #    !#
#!     #   #     #       #    #       # #   #    #     #  #     # # #   #    !#
#!    #     #    #       #    #####   #  #  #    #     #  #     # #  #  #    !#
#!    #######    #       #    #       #   # #    #     #  #     # #   # #    !#
#!    #     #    #       #    #       #    ##    #     #  #     # #    ##    !#
#!    #     #    #       #    ####### #     #    #    ### ####### #     #    !#
#!                                                                           !#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
###############################################################################
"
	echo;

	echo "In performing the following the GOBii Database volumes and all data currently stored within the gobii_bundle will be wiped of data."

	read -p "Please verify you are aware and would like to continue to wipe the GOBii Data! [y/n]: " confirm2
	echo;


	read -p "Please press any key to continue... " -n1 -s
	echo;
	
else
	
	echo "You did not confirm you wanted a wipe of the GOBii Data ..."
	echo;

	echo "Continuing without a data wipe."
	echo;

	read -p "Please press any key to continue... " -n1 -s
	echo;

	exit
fi

# Second Confirmation

if [ $confirm2 == 'y' ]; then
	clear
	echo '
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
###############################################################################
#!                                                                           !#
#!   Failing to read the following message could result in loss of data!!!   !#
#!                                                                           !#
###############################################################################
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
'
	echo;

	echo "Continuing will result in the following directories being deleted:"
	echo "• $NDD_PATH/gobii_bundle/crops/*"
	echo "• $NDD_PATH/gobii_bundle/logs/*"
	echo "• $NDD_PATH/gobii_bundle/config/gobii-web.xml"
	echo;

	echo "Continuing will result in the following docker volumes being deleted:"
	echo "• gobiipostgresetcubuntu"
	echo "• gobiipostgreslibubuntu"
	echo "• gobiipostgreslogubuntu"
	echo;


	read -p "Please confirm that you are aware these files are going to be deleted [y/n]: " confirm3
	echo;

	read -p "Please press any key to continue... " -n1 -s
	echo;

else
	echo "You did not confirm you wanted a wipe of the GOBii Data ..."
	echo;

	echo "Continuing without a data wipe."
	echo;

	read -p "Please press any key to continue... " -n1 -s
	echo;

	exit
fi

if [ $confirm3 == 'y' ]; then
	clear

	echo "You have confirmed that you are aware the following will be destroyed and permanetly removed from this system: "
	echo;
	
	echo "Directories:"
	echo "• $NDD_PATH/gobii_bundle/crops/*"
	echo "• $NDD_PATH/gobii_bundle/logs/*"
	echo "• $NDD_PATH/gobii_bundle/config/gobii-web.xml"
	echo;

	echo "Docker volumes:"
	echo "• gobiipostgresetcubuntu"
	echo "• gobiipostgreslibubuntu"
	echo "• gobiipostgreslogubuntu"
	echo;

	echo "This is the last chance before data will be wiped!"
	echo;

	read -p "Please verify you have backups of all your data outside of GDM and you are willingly wiping the data from GOBii Data Manager [y/n]: " confirm4
	echo;

	if [ $confirm4 == 'y' ]; then

		echo "Continuing with data wipe..."
		echo;

		echo "Should you need to exit the Data Wipe please use Ctrl + C to cancel the script and exit now."
		echo;

		read -p "Please press any key to continue... " -n1 -s
		echo;

		clear
		echo "Performing Data Wipe of:"
		echo "• $NDD_PATH/gobii_bundle/crops/*"
		echo "• $NDD_PATH/gobii_bundle/logs/*"
		echo "• $NDD_PATH/gobii_bundle/config/gobii-web.xml"
		echo;

		rm -rfv $NDD_PATH/gobii_bundle/crops/*
		echo;

		echo "Deleted: $NDD_PATH/gobii_bundle/crops/*"
		echo;

		rm -rfv $NDD_PATH/gobii_bundle/logs/*
		echo;

		echo "Deleted: $NDD_PATH/gobii_bundle/logs/*"
		echo;

		rm -rfv $NDD_PATH/gobii_bundle/config/gobii-web.xml
		echo;

		echo "Deleted: $NDD_PATH/gobii_bundle/config/gobii-web.xml"
		echo;

		echo "Moving on to Docker Volumes..."
		echo;
		
		read -p "Please press any key to continue... " -n1 -s
		echo;

		echo "Performing Data Wipe of:"
		echo "• gobiipostgresetcubuntu"
		echo "• gobiipostgreslibubuntu"
		echo "• gobiipostgreslogubuntu"
		echo;

		echo "Stopping gobii-db-node..."
		docker stop gobii-db-node || true;
		echo;

		echo "Removing gobii-db-node container..."
		docker rm gobii-db-node || true;
		echo;

		echo "Removing gobii-db-node volumes..."
		docker volume rm gobiipostgresetcubuntu || true;
		docker volume rm gobiipostgreslibubuntu || true;
		docker volume rm gobiipostgreslogubuntu || true;
		echo;

		echo "GDM Data Wipe has been completed."
		echo;

		echo "Please make sure to use the ndd_pathing.sh script to rebuild the Non-Destructive deployment symlinks and directories post deployment."
		echo;

		read -p "Please press any key to continue... " -n1 -s
		echo;

	else
		echo "You did not confirm you wanted a wipe of the GOBii Data ..."
		echo;

		echo "Continuing without a data wipe."
		echo;

		read -p "Please press any key to continue... " -n1 -s
		echo;

		exit
	fi

else
	echo "You did not confirm you wanted a wipe of the GOBii Data ..."
	echo;

	echo "Continuing without a data wipe."
	echo;

	read -p "Please press any key to continue... " -n1 -s
	echo;

	exit
fi

echo "Script has completed... Good Bye."
exit