#!/usr/bin/env bash

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
set -x # xtrace: Similar to #-v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

source $1

echo;

if [ $1 = "shutdown" ]; then
    echo "Shutting down tomcat..."
    echo;

    docker exec -u gadm gobii-web-node bash -c "
    /usr/local/tomcat/bin/shutdown.sh;
    ";

fi
echo;

if [ $1 = "restart" ]; then
    echo "Restarting tomcat..."
    echo;

    echo "Shutting down tomcat..."
    echo;

    docker exec -u gadm gobii-web-node bash -c "
    /usr/local/tomcat/bin/shutdown.sh;
    ";
    echo;

    echo "Sleeping..."
    echo;

    sleep 5

    echo "Starting up tomcat..."
    echo;


    docker exec -u gadm gobii-web-node bash -c "
    /usr/local/tomcat/bin/startup.sh;
    ";
    echo;

fi
echo;

if [ $1 = "start" ]; then
    echo "Starting up tomcat..."
    echo;

    docker exec -u gadm gobii-web-node bash -c "
    /usr/local/tomcat/bin/startup.sh;
    ";

fi
echo;