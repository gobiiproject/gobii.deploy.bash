#!/usr/bin/env bash

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
set -x # xtrace: Similar to #-v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#
#@author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

#-----------------------------------------------------------------------------#
### Warn before Cleaning system
#-----------------------------------------------------------------------------#

while true; do
		echo "	
###############################################################################
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
#!                                                                           !#
#!           #     #    #    ######  #     # ### #     #  #####              !#
#!           #  #  #   # #   #     # ##    #  #  ##    # #     #             !#
#!           #  #  #  #   #  #     # # #   #  #  # #   # #                   !#
#!           #  #  # #     # ######  #  #  #  #  #  #  # #  ####             !#
#!           #  #  # ####### #   #   #   # #  #  #   # # #     #             !#
#!           #  #  # #     # #    #  #    ##  #  #    ## #     #             !#
#!            ## ##  #     # #     # #     # ### #     #  #####              !#
#!                                                                           !#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
###############################################################################
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
#!                                                                           !#
#!       #    ####### ####### ####### #     # ####### ### ####### #     #    !#
#!      # #      #       #    #       ##    #    #     #  #     # ##    #    !#
#!     #   #     #       #    #       # #   #    #     #  #     # # #   #    !#
#!    #     #    #       #    #####   #  #  #    #     #  #     # #  #  #    !#
#!    #######    #       #    #       #   # #    #     #  #     # #   # #    !#
#!    #     #    #       #    #       #    ##    #     #  #     # #    ##    !#
#!    #     #    #       #    ####### #     #    #    ### ####### #     #    !#
#!                                                                           !#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
###############################################################################
"
		echo "Do you want to completely clean all docker images, containers, networks and volumes from this system?"
        read -p "Please enter \"EXIT\" or \"WIPE\" to continue: " confirm
        echo;
        case $confirm in
<<<<<<< HEAD
                [WIPE]* ) echo "You have choosen to $confirm the system!"; break;;
                [EXIT]* ) echo "You have choosen to $confirm the script.  EXITING..."; break;;
=======
                [WIPE]* ) echo "You have choosen to $confirm the system!"; echo; break;;
                [EXIT]* ) echo "You have choosen to $confirm the script.  EXITING..."; echo; break;;
>>>>>>> release/2.2
                * ) clear; echo; echo "Answer not \"EXIT\" or \"WIPE\"!"; echo;;
        esac
done

if [[ confirm == 'EXIT' ]]; then exit 1; fi

#-----------------------------------------------------------------------------#
### Perform Docker Service Clean
#-----------------------------------------------------------------------------#

echo "Performing Container Stop...";
echo;

docker stop $(docker ps -qa) || true;
echo;

#-----------------------------------------------------------------------------#

echo "Performing container removal...";
echo;

docker rm $(docker ps -qa) || true;
echo;

#-----------------------------------------------------------------------------#

echo "Performing Image deletion...";
echo;

docker image rm $(docker images) || true;
echo;

#-----------------------------------------------------------------------------#

echo "Performing Volume Deletion...";
echo;

docker volume rm $(docker volume ls) || true;
echo;

#-----------------------------------------------------------------------------#

echo "Performing Network Deletion...";
echo;

docker network rm $(docker network ls) || true;
echo;

#-----------------------------------------------------------------------------#

echo "Performing a prune on the docker system...";
echo;

echo -e "y/n" | docker system prune -a  || true;
echo;

#-----------------------------------------------------------------------------#
### Script Completion...
#-----------------------------------------------------------------------------#

echo "The Docker system has been cleaned and wiped.";
echo "Script Completion. Good Bye.";
echo;