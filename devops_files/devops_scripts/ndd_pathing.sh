#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### ndd_pathing.sh

### This script will manually setup the Non-Destructive deployment.  This 
# removes current data locations under /data from the deployment and then 
# creates symlinks to the persistent data location for crops, logs, gobii_adl 
# and the gobii-web.xml.

### This script also backups up newly deployed gobii-web.xml to $NDD_PATH/
# gobii_bundle/config/archived_xml/gobii-web.xml_$TAG and will request whether 
# to utilize new xml or continue to use old xml.

#-----------------------------------------------------------------------------#
#@author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# setting bash environment

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
set -x # xtrace: Similar to -v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#
### !!! WARNING !!!
#-----------------------------------------------------------------------------#
### Do not run this script as SUDO! There are explicit sudo commands which
# will prompt you for password. But not everything should run as sudo.

### usage: bash <bash_script_name>.sh <path of main.parameters> <path of 
# install.parameters> <dockerhubpassw | "askme"> <gobii_release_version>

#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

#-----------------------------------------------------------------------------#
### load parameters -- main.parameters for deployment
#-----------------------------------------------------------------------------#

set +x

# sourcing *_main.parameters
source $1

#-----------------------------------------------------------------------------#
# Setting Date/time tag variable

TAG=$(date +"%Y-%m-%d_%H-%M-%S")

#-----------------------------------------------------------------------------#

# Set variables for all persistent data locations and symlink (NDD) locations

# Crops
crops_persistent="$NDD_PATH/gobii_bundle/crops"
crops_sym="$BUNDLE_PARENT_PATH/gobii_bundle/crops"

# logs
logs_persistent="$NDD_PATH/gobii_bundle/logs"
logs_sym="$BUNDLE_PARENT_PATH/gobii_bundle/logs"

# gobii-web.xml
gdm_xml_persistent="$NDD_PATH/gobii_bundle/config/gobii-web.xml"
gdm_xml_sym="$BUNDLE_PARENT_PATH/gobii_bundle/config/gobii-web.xml"
gdm_xml_tag=("$gdm_xml_sym"_"$TAG")
gdm_xml_old_tag=("$gdm_xml_persistent"_"$TAG")
xml_backup_path=("$NDD_PATH/gobii_bundle/config/archived_xml")
xml_backup_tag=("$xml_backup_path"_"$TAG")

#-----------------------------------------------------------------------------#
# Create arrays for all persistent and sym locations

persistent_locations=( "$crops_persistent" "$logs_persistent" "$gdm_xml_persistent" )

sym_locations=( "$crops_sym" "$logs_sym" "$gdm_xml_sym" )

#-----------------------------------------------------------------------------#
# clearing
clear

#-----------------------------------------------------------------------------#
# Check file condition for each location before continuing

echo;

for i in "${sym_locations[@]}"; do

	if [ -L ${i} ] ; then
   
   		if [ -e ${i} ] ; then
      		echo "Is a Symlink: $i"
      		echo;

   		else
      		echo "Broken Symlink: $i"
      		echo;
   
	   fi

	elif [ -e ${i} ] ; then
   		echo "Not a symlink: $i"
   		echo;

	else
   		echo "Does not Exist: $i"
   		echo;

	fi

done
echo;


#-----------------------------------------------------------------------------#
# Remove symlocations if a directory and replace with symlinks to persistent 
# data

echo "
Before continuing please verify all data is backed up to a location outside of the GDM directory structure. If you are unsure please use \"Ctrl + C\" to exit the script and verify then rerun this script.
"
echo;

read -p "Please verify you would like to continue [y/n]: " verify1
echo;

#-----------------------------------------------------------------------------#

while [[ "$verify1" == "y" ]]; do


	# Verify crops/ is a directory and not a symlink
	if [ -e ${crops_sym} ] ; then
		if [ -L ${crops_sym} ]; then
			echo "$crops_sym is already a symlink."

		else
			read -p "$crops_sym is a directory. Would you like to remove and replace with the non-destructive deployment symlink to persistent data? [y/n]: " crops_verify
			echo;

			# Verify continue then remove crops/ directory and create symlink
			if [[ "$crops_verify" == "y" ]]; then
			
				# Recursive, force and verbose deletion of crops/
				echo "Removing..."
				echo "rm -rfv $crops_sym || true;"
				echo;

				rm -rfv $crops_sym || true;
				echo;

				# Symbolic, force, no-dereference [treat as a normal file] 
				# creation of crops symlink
				echo "Creating symlink to persistent data..."
				echo "ln -sfn $crops_persistent $crops_sym || true;"
				echo;

				ln -sfn $crops_persistent $crops_sym || true;
				echo;

			else
				echo "Not creating crops symlink."
				echo;

			fi

		fi

	else
		echo "$crops_sym does not exist."
		echo;

	fi

	#-------------------------------------------------------------------------#
	# Verify logs/ is a directory and not a symlink
	if [ -e ${logs_sym} ] ; then
		if [ -L ${logs_sym} ]; then
			echo "$logs_sym is already a symlink."

		else
			read -p "$logs_sym is a directory. Would you like to remove and replace with the non-destructive deployment symlink to persistent data? [y/n]: " logs_verify
			echo;

			# Verify continue then remove logs/ directory and create symlink
			if [[ "$logs_verify" == "y" ]]; then
			
				# Recursive, force and verbose deletion of logs/
				echo "Removing..."
				echo "rm -rfv $logs_sym || true;"
				echo;

				rm -rfv $logs_sym || true;
				echo;

				# Symbolic, force, no-dereference [treat as a normal file] 
				# creation of logs symlink
				echo "Creating symlink to persistent data..."
				echo "ln -sfn $logs_persistent $logs_sym || true;"
				echo;

				ln -sfn $logs_persistent $logs_sym || true;
				echo;

			else
				echo "Not creating logs symlink."
				echo;

			fi

		fi

	else
		echo "$logs_sym does not exist."
		echo;
		
	fi
	
	#-------------------------------------------------------------------------#

	if [ -L ${gdm_xml_sym} ] ; then

		if [ -e ${gdm_xml_sym} ] ; then
			echo "$gdm_xml_sym"
			echo "Is already a symlink."
			echo;

		else		
			echo "$gdm_xml_sym"
			echo "Is a broken symlink. Please verify gobii-web.xml exists in persistent data location."
			echo;
		fi

	elif [ -e ${gdm_xml_sym} ] ; then
		echo "$gdm_xml_sym"
		echo "Exists..."
		echo; 

		echo "Backing up new xml to: $xml_backup_tag"
		echo;

		echo "mkdir -p $xml_backup_path || true;"
		echo "mv $gdm_xml_sym $xml_backup_tag || true;"
		echo;

		mkdir -p $xml_backup_path || true;
		mv $gdm_xml_sym $xml_backup_tag || true;
		echo;

		#---------------------------------------------------------------------## Determine if new xml will be used

		PS3='Replace gobii-web.xml with newly Deployed XML?: '

		#---------------------------------------------------------------------#
		# the below options are set to a numbered menu that sets the $REPLY 
		# variable

		options=(
			"Yes [Backup old gobii-web.xml and replace with new]"
			"No  [Backup new xml and use old gobii-web.xml]"
		)

		#---------------------------------------------------------------------#

		select yn in "${options[@]}"

			do
    			case $yn in
      				"Yes [Backup old gobii-web.xml and replace with new]")
					
					if [[ $REPLY == 1 ]]; then
						echo "Renaming current xml to: $gdm_xml_old_tag"
						echo;

						echo "mv $gdm_xml_persistent $gdm_xml_old_tag || true;"
						echo;

						mv $gdm_xml_persistent $gdm_xml_old_tag || true;

						echo "Coping new xml from: $xml_backup_tag"
						echo "To: $gdm_xml_persistent"
						echo;

						echo "
						cp $xml_backup_tag $gdm_xml_persistent || true;
						"
						echo;

						cp $xml_backup_tag $gdm_xml_persistent || true;
						echo;

						echo "ln -sfn $gdm_xml_persistent $gdm_xml_sym || true;"
						echo;

						# Symbolic, force, no-dereference [treat as a normal 
						# file] creation of logs symlink
						ln -sfn $gdm_xml_persistent $gdm_xml_sym || true;
						echo;

					echo;
					break
					fi
           				;;

        			"No  [Backup new xml and use old gobii-web.xml]")
						if [[ $REPLY == 2 ]]; then
							ln -sfn $gdm_xml_persistent $gdm_xml_sym || true;
								echo;

					break
					fi
            			;;

				*) echo "invalid option $REPLY";;
    			esac
		done

		echo "ndd_pathing.sh script completion!"
		echo;


	else
		echo "$gdm_xml_sym"
		echo "Does not Exist."

	fi

break
done 

exit
