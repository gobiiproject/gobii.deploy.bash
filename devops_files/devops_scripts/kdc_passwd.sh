#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
# Update KDC UI admin password
#-----------------------------------------------------------------------------#

DB_PASSWORD=$1
new_password=$2

#-----------------------------------------------------------------------------#
# Updating password

# kdc variables 
DB_USER=root
DB_NAME=kdcdb
BCRYPT_PROGRAM=/usr/local/tomcat/kdcompute_file_storage/TestOutput_UserDirs/workspace/bcrypt/build/libs/bcrypt.jar

hashed_password=`java -jar "$BCRYPT_PROGRAM" $new_password`

mysql --user="$DB_USER" --password="$DB_PASSWORD" --database="$DB_NAME" --execute="update User set HashedPassword = '$hashed_password' where UserName = 'admin';"

#-----------------------------------------------------------------------------#
# KDC changing default admin password for UI [Manual process]

# log into kdc-node
# run the following command to set default gobii pass as admin password
### Note the -p and the password can not have a space between them when 
# running the mysql command below

#-----------------------------------------------------------------------------#

# manually get hash from : https://www.dailycred.com/article/bcrypt-calculator

#-----------------------------------------------------------------------------#

#mysql -u root -p<Default gobii pass>

#use kdcdb;
#update User set \
#HashedPassword = '<hashed generated password>'\
#where UserName = 'admin';
#exit;

#-----------------------------------------------------------------------------#

# Please be aware this is only placed within the script as a backup to calling 
# the orginal process so as to verify any system administrator has the power 
# to update the kdc UI password.

#-----------------------------------------------------------------------------#
