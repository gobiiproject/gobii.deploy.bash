#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### the_gobii_ship_portainer.sh
### <description goes here>
#-----------------------------------------------------------------------------#
#@author: (palace) kdp44@cornell.edu
###>>> Co-Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
set -x # xtrace: Similar to -v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#
### !!! WARNING !!!
#-----------------------------------------------------------------------------#
### Do not run this script as SUDO! There are explicit sudo commands which
### will prompt you for password. But not everything should run as sudo.

### usage: bash <bash_script_name>.sh <path of main.parameters> <path of 
# install.parameters> <dockerhubpassw | "askme"> <gobii_release_version>

#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

#-----------------------------------------------------------------------------#
### load parameters -- main.parameters for deployment
#-----------------------------------------------------------------------------#

set +x

source $1

#-----------------------------------------------------------------------------#
### load parameters -- install.parameters for configuration
#-----------------------------------------------------------------------------#

source $2

#-----------------------------------------------------------------------------#
### Set release version and Dockerhub password
###>>> if pass set to 'askme' call for user to enter password at cli
#-----------------------------------------------------------------------------#

DOCKER_HUB_PASSWORD=$3

#-----------------------------------------------------------------------------#
### Requesting passwords [Dockerhub]
#-----------------------------------------------------------------------------#
###>>> By default the same password will be set for all administration and 
###>>> gadm users. If a different password is needed, uncomment the 'if' ###>>> statements below to interactively enter the different passwords
#-----------------------------------------------------------------------------#

echo;

if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
fi
echo;

set -x

#-----------------------------------------------------------------------------#
### Starting compute deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the portainer-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove COMPUTE container
#-----------------------------------------------------------------------------#

docker stop $$DOCKER_LDAP_NAME || true && docker rm $DOCKER_LDAP_NAME || true

#-----------------------------------------------------------------------------#
### Pull and start the COMPUTE image
#-----------------------------------------------------------------------------#

docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_LDAP_NAME:$DOCKER_LDAP_VERSION;

#-----------------------------------------------------------------------------#
### Run the lda[-node image
#-----------------------------------------------------------------------------#
#Pulling the docker ldap image


#Deploying ldap Container
docker run -i --detach \
--name $DOCKER_LDAP_NAME \
-h $DOCKER_LDAP_CONTAINER_HOSTNAME \
-v $BUNDLE_PARENT_PATH:/data \
-p $DOCKER_LDAP_PORT:389 \
-p $DOCKER_LDAP_SSL_PORT:636 \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_LDAP_NAME:$DOCKER_LDAP_VERSION;

#Starting ldap Container
docker start $$DOCKER_LDAP_NAME;

sleep 5

#Start LDAP Service
docker exec -i $DOCKER_LDAP_NAME sh -c "service slapd status"
docker exec -i $DOCKER_LDAP_NAME sh -c "service slapd start"

#-----------------------------------------------------------------------------#
### Start the ldap-node image from dockerhub
#-----------------------------------------------------------------------------#

docker start $DOCKER_LDAP_NAME;

#-----------------------------------------------------------------------------#
### ldap-node deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;

echo "ldap-node deployment script completion!"
echo;
