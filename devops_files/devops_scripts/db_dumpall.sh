#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
#Author: RLPetrie (rlp243@cornell.edu)

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "Not Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "Not Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set Environment for shell
set -e # Abort script at first error, when a command exits with non-zero status
set -u # Forces exit when a variable is undefined
set -x # xtrace: Set High Verbosity

#-----------------------------------------------------------------------------#
### Create dated tag variable for backup

TAG=$(date +"%Y%m%d_%H%M%S")

#-----------------------------------------------------------------------------#
### Create backup directory for DB dump

docker exec -i -u gadm $DOCKER_DB_NAME bash -c "
mkdir -pv $BUNDLE_PARENT_PATH/db_backups ||true;
chmod 777 $BUNDLE_PARENT_PATH/db_backups ||true;
";
echo;

docker exec -i -u postgres $DOCKER_DB_NAME bash -c "
cd $BUNDLE_PARENT_PATH/db_backups ||true;
pg_dumpall > db_dumpall_$TAG.sql;
echo;
";
echo;

FILE="$BUNDLE_PARENT_PATH/db_backups/db_dumpall_$TAG.sql"

docker exec -i -u gadm $DOCKER_DB_NAME bash -c "
if [[ -f \"$FILE\" ]]; then
   echo \"DB Dump Location: $BUNDLE_PARENT_PATH/db_backups/db_dumpall_$TAG.sql\"
else
   echo \"FAILURE: Backup does not exist!\"
   exit 1
fi";


