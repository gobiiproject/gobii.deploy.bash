#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
#@author: Joshua Lamos-Sweeney (jdl232@cornell.edu)
###>>> Co-Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Run undefined variable outputs error message, and forces exit

#-----------------------------------------------------------------------------#

if [ -z "$1" ]; then
    echo 'usage moveLaunchersIntoPosition.sh </path/to/launchers.xml>'
    exit
fi

#-----------------------------------------------------------------------------#
# set automation to not prompt

export PROMPT="no"

#-----------------------------------------------------------------------------#
# List elements in sorted order. Technically "<item>" is a grep regex. Not found
# elements print a warning, elements not specified will end up 'on top'

./moveLauncherDown.sh "dev" $1
./moveLauncherDown.sh "Timescope" $1
./moveLauncherDown.sh "File Browser" $1
./moveLauncherDown.sh "KDCompute" $1
./moveLauncherDown.sh "EiB GS pipeline Demo" $1
./moveLauncherDown.sh "GDM Data Loader" $1
./moveLauncherDown.sh "Flapjack" $1
./moveLauncherDown.sh "TASSEL" $1
./moveLauncherDown.sh "GOBii Project" $1
./moveLauncherDown.sh "EiB Platform" $1

#-----------------------------------------------------------------------------#
