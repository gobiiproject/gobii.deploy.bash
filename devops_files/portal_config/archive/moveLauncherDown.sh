#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
#@author: Joshua Lamos-Sweeney (jdl232@cornell.edu)
###>>> Co-Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Run undefined variable outputs error message, and forces exit

#-----------------------------------------------------------------------------#
# If two args are not passed to the script exit

if [ $# -lt 2 ]; then
    echo 'usage moveLauncherDown.sh <name of launch item to move to bottom> </path/to/launchers.xml>'
    exit 1
fi

#-----------------------------------------------------------------------------#
# Set vars

TAG=$(date +"%Y%m%d_%H%M%S")
LAUNCHER_FILE="$2"
LAUNCHER_END_SEGMENT="</launcher>"
LAUNCHER_GROUP_END_SEGMENT="</gobii-launchers>"
LAUNCHER_NAME="$1"
LAUNCHER_POS=`grep -nom1 "$LAUNCHER_NAME" $LAUNCHER_FILE | cut -f1 -d: | cat `

#-----------------------------------------------------------------------------#
# determine if LAUNCHER_POS was and exit if it was not

if [ -z "$LAUNCHER_POS" ]; then
    echo "Launcher Name \"$LAUNCHER_NAME\" was not found. Exiting..."
    echo;
    exit 1
fi

#-----------------------------------------------------------------------------#

#TODO - ASSUMES 'NAME' is the first element of the XML
LAUNCHER_TOP="$(($LAUNCHER_POS-1))"

#echo "Launcher Top = $LAUNCHER_TOP"
LAUNCHER_SIZE=`tail -n +"$LAUNCHER_TOP" $LAUNCHER_FILE | grep -nom1 "$LAUNCHER_END_SEGMENT" | cut -f1 -d: |cat `

#echo "Launcher Size = $LAUNCHER_SIZE"
LAUNCHER_BOTTOM=$(($LAUNCHER_TOP+$LAUNCHER_SIZE))

#echo "Launcher Bottom = $LAUNCHER_BOTTOM"
LAUNCHERS_BOTTOM=`grep -nom1 "$LAUNCHER_GROUP_END_SEGMENT" $LAUNCHER_FILE | cut -f1 -d: | cat`

#echo "Launchers Bottom = $LAUNCHERS_BOTTOM"
BACKUP="launcher_xml.bkup_$TAG"
HEAD="head.tmp_$TAG"
POST="post.tmp_$TAG"
ENTRY="entry.tmp_$TAG"
FOOTER="footer.tmp_$TAG"

#-----------------------------------------------------------------------------#
# create backup of original launcher.xml

cp $LAUNCHER_FILE $BACKUP

#-----------------------------------------------------------------------------#
# prompt for exit if ran manually

if [[ ! -z "$PROMPT" ]]; then
  echo "Prompt for Ctrl+C intervention set to auto..."
  echo;
  sleep 3
else
  read -p "Are you sure? (Ctrl+C now!) " -n1 -s
  echo;
fi

#-----------------------------------------------------------------------------#

head -n $(($LAUNCHER_TOP -1))     $LAUNCHER_FILE > $HEAD
tail -n +$LAUNCHER_TOP     $LAUNCHER_FILE | head -n $LAUNCHER_SIZE > $ENTRY
tail -n +$LAUNCHER_BOTTOM  $LAUNCHER_FILE | head -n $(($LAUNCHERS_BOTTOM-$LAUNCHER_BOTTOM)) > $POST
tail -n +$LAUNCHERS_BOTTOM $LAUNCHER_FILE > $FOOTER

#-----------------------------------------------------------------------------#
# clean up files used

rm $LAUNCHER_FILE
cat $HEAD $POST $ENTRY $FOOTER > $LAUNCHER_FILE
rm $HEAD
rm $POST
rm $ENTRY
rm $FOOTER

#-----------------------------------------------------------------------------#

echo "Script competion for $LAUNCHER_NAME"
echo;