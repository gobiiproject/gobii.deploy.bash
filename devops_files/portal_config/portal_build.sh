
#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "No Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "No Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#

PORTAL_CONFIG_LOCATION="/usr/local/tomcat/webapps/gobii-portal/config"
GOBII_LAUNCHERS_START='<gobii-launchers>'
GOBII_LAUNCHERS_END='</gobii-launchers>'
WEB_APPS_TAB='<!-- Web Apps Tab -->'
DESKTOP_APPS_TAB='<!-- Desktop Apps Tab -->'
LINKS_TAB='<!-- Links Tab -->'
TAG=$(date +"%Y%m%d_%H%M%S")

#-----------------------------------------------------------------------------#
# navigate to /path/to/gobii.deploy.bash and copy launcher_build/ & images

cp -rv devops_files/portal_config/launcher_build/ $BUNDLE_PARENT_PATH/launcher_build_$TAG/
mkdir -p $BUNDLE_PARENT_PATH/launcher_build_$TAG/img
cp -rv hotfix/release223_hotfix/img/. $BUNDLE_PARENT_PATH/launcher_build_$TAG/img/
cd $BUNDLE_PARENT_PATH/launcher_build_$TAG/

#-----------------------------------------------------------------------------#
# create new files for each crop
cp crop_name $DOCKER_CROP1_NAME.xml

# find unique string and replace
sed -i 's~<name>unique_cropName</name>~<name>'$DOCKER_CROP1_NAME'</name>~' $DOCKER_CROP1_NAME.xml
sed -i 's~<url>http://unique_cropURL:8081/gobii-dev</url>~<url>'$PORTAL_CROP1_URL'</url>~' $DOCKER_CROP1_NAME.xml

if [[ -v DOCKER_CROP2_NAME ]]; then
	cp crop_name $DOCKER_CROP2_NAME.xml;
	sed -i 's~<name>unique_cropName</name>~<name>'$DOCKER_CROP2_NAME'</name>~' $DOCKER_CROP2_NAME.xml
	sed -i 's~<url>http://unique_cropURL:8081/gobii-dev</url>~<url>'$PORTAL_CROP2_URL'</url>~' $DOCKER_CROP2_NAME.xml
fi

if [[ -v DOCKER_CROP3_NAME ]]; then
	cp crop_name $DOCKER_CROP3_NAME.xml;
	sed -i 's~<name>unique_cropName</name>~<name>'$DOCKER_CROP3_NAME'</name>~' $DOCKER_CROP3_NAME.xml
	sed -i 's~<url>http://unique_cropURL:8081/gobii-dev</url>~<url>'$PORTAL_CROP3_URL'</url>~' $DOCKER_CROP3_NAME.xml
fi

if [[ -v DOCKER_CROP4_NAME ]]; then
	cp crop_name $DOCKER_CROP4_NAME.xml;
	sed -i 's~<name>unique_cropName</name>~<name>'$DOCKER_CROP4_NAME'</name>~' $DOCKER_CROP4_NAME.xml
	sed -i 's~<url>http://unique_cropURL:8081/gobii-dev</url>~<url>'$PORTAL_CROP4_URL'</url>~' $DOCKER_CROP4_NAME.xml
fi

if [[ -v DOCKER_CROP5_NAME ]]; then
	cp crop_name $DOCKER_CROP5_NAME.xml;
	sed -i 's~<name>unique_cropName</name>~<name>'$DOCKER_CROP5_NAME'</name>~' $DOCKER_CROP5_NAME.xml
	sed -i 's~<url>http://unique_cropURL:8081/gobii-dev</url>~<url>'$PORTAL_CROP5_URL'</url>~' $DOCKER_CROP5_NAME.xml
fi

if [[ -v DOCKER_CROP6_NAME ]]; then
	cp crop_name $DOCKER_CROP6_NAME.xml;
	sed -i 's~<name>unique_cropName</name>~<name>'$DOCKER_CROP6_NAME'</name>~' $DOCKER_CROP6_NAME.xml
	sed -i 's~<url>http://unique_cropURL:8081/gobii-dev</url>~<url>'$PORTAL_CROP6_URL'</url>~' $DOCKER_CROP6_NAME.xml
fi

#-----------------------------------------------------------------------------#
# create xml copies for each with updated urls

cp timescope timescope.xml
sed -i 's~<url>unique_timescopeURL</url>~<url>'$PORTAL_TIMESCOPE_URL'</url>~' timescope.xml

cp filebrowser filebrowser.xml
sed -i 's~<url>unique_fileBrowserURL</url>~<url>'$PORTAL_OWNCLOUD_URL'</url>~' filebrowser.xml

cp kdcompute kdcompute.xml
sed -i 's~<url>unique_kdcURL</url>~<url>'$PORTAL_KDC_URL'</url>~' kdcompute.xml

cp haplo haplo.xml
sed -i 's~<url>unique_haploURL</url>~<url>'$PORTAL_HAPLOTOOL_URL'</url>~' haplo.xml

cp pedver pedver.xml
sed -i 's~<url>unique_pedverURL</url>~<url>'$PORTAL_PEDVER_URL'</url>~' pedver.xml

cp portainer portainer.xml
sed -i 's~<url>unique_portainerURL</url>~<url>'$PORTAL_PORTAINER_URL'</url>~' portainer.xml

#-----------------------------------------------------------------------------#
### Start xml creation

# create temperary launchers.xml file to be manipulated for environment
touch launchers.xml_creation_$TAG;
echo;

# add the start of xml
echo $GOBII_LAUNCHERS_START > launchers.xml_creation_$TAG;
echo -en '\n' >> launchers.xml_creation_$TAG; #new line
echo $WEB_APPS_TAB >> launchers.xml_creation_$TAG; # webapps label
echo -en '\n' >> launchers.xml_creation_$TAG; #new line

cat $DOCKER_CROP1_NAME.xml >> launchers.xml_creation_$TAG;

if [[ -v DOCKER_CROP2_NAME ]]; then
	cat $DOCKER_CROP2_NAME.xml >> launchers.xml_creation_$TAG;
fi

if [[ -v DOCKER_CROP3_NAME ]]; then
	cat $DOCKER_CROP3_NAME.xml >> launchers.xml_creation_$TAG;
fi

if [[ -v DOCKER_CROP4_NAME ]]; then
	cat $DOCKER_CROP4_NAME.xml >> launchers.xml_creation_$TAG;
fi

if [[ -v DOCKER_CROP5_NAME ]]; then
	cat $DOCKER_CROP5_NAME.xml >> launchers.xml_creation_$TAG;
fi

if [[ -v DOCKER_CROP6_NAME ]]; then
	cat $DOCKER_CROP6_NAME.xml >> launchers.xml_creation_$TAG;
fi

cat timescope.xml >> launchers.xml_creation_$TAG;
cat filebrowser.xml >> launchers.xml_creation_$TAG;
cat kdcompute.xml >> launchers.xml_creation_$TAG;
cat eib >> launchers.xml_creation_$TAG;
cat haplo.xml >> launchers.xml_creation_$TAG;
cat pedver.xml >> launchers.xml_creation_$TAG;
# cat flapjack_bytes >> launchers.xml_creation_$TAG;
cat portainer.xml >> launchers.xml_creation_$TAG;

#-----------------------------------------------------------------------------#
# comment for new tab section on portal

echo -en '\n' >> launchers.xml_creation_$TAG; #new line
echo $DESKTOP_APPS_TAB >> launchers.xml_creation_$TAG; # webapps label
echo -en '\n' >> launchers.xml_creation_$TAG; #new line

cat loaderui >> launchers.xml_creation_$TAG;
cat flapjack >> launchers.xml_creation_$TAG;
cat tassel >> launchers.xml_creation_$TAG;
cat dartview >> launchers.xml_creation_$TAG;

#-----------------------------------------------------------------------------#
# comment for new tab section on portal

echo -en '\n' >> launchers.xml_creation_$TAG; #new line
echo $LINKS_TAB >> launchers.xml_creation_$TAG; # webapps label
echo -en '\n' >> launchers.xml_creation_$TAG; #new line

cat gobii_project >> launchers.xml_creation_$TAG;
cat eib_platform >> launchers.xml_creation_$TAG;
cat gdm_helpdesk >> launchers.xml_creation_$TAG;

#-----------------------------------------------------------------------------#
# add the start of xml
echo $GOBII_LAUNCHERS_END >> launchers.xml_creation_$TAG;

#-----------------------------------------------------------------------------#
### Copy from /data inside tomcat

cp launchers.xml_creation_$TAG $BUNDLE_PARENT_PATH

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cp $BUNDLE_PARENT_PATH/launchers.xml_creation_$TAG /usr/local/tomcat/webapps/gobii-portal/config/launchers.xml;
";

# Copy images
docker cp $BUNDLE_PARENT_PATH/launcher_build_$TAG/img/. $DOCKER_WEB_NAME:/usr/local/tomcat/webapps/gobii-portal/config/img
