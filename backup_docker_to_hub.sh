#!/usr/bin/env bash
#This script takes a snapshot (commits) of the docker image passed as a parameter and pushes that commit point to dockerhub in the given repository. The TAG that will be used is the current timestamp of the format YYYY-MM-DD_HH-mm-SS , ex. 2017-05-26_10-49-57.
#The intention of this script is to be used in cronjobs for incremental backups of GOBII instances. 
#Run this as user 'gadm' or your GOBII admin user account, which should be a sudoer. But do not run the script as sudo. It will prompt for password as needed.
if [ $# -lt 2 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash backup_docker_to_hub.sh <name_of_docker> <dockerhub_username> <dockerhubpassw | askme> <dockerhub_repo_name>"
    echo "Set dockerhubpassw parameter to 'askme' for the script to prompt for password instead."
    exit 1
fi

set -u
set -e
set -x

DOCKER_NAME=$1
DOCKER_HUB_USERNAME=$2
DOCKER_HUB_PASSWORD=$3
DOCKER_HUB_REPO_NAME=$4

if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
	echo "Enter your Docker Hub password:"
	read -s -r DOCKER_HUB_PASSWORD
fi

echo "Logging into the dockerhub account..."
docker login -u $DOCKER_HUB_USERNAME -p $DOCKER_HUB_PASSWORD;

TAG=$(date +"%Y-%m-%d_%H-%M-%S")
echo "Saving the docker checkpoint...TAG=" %TAG

docker commit -m "Backup created on $TAG" -a "$DOCKER_HUB_USERNAME" $DOCKER_NAME $DOCKER_HUB_USERNAME/$DOCKER_HUB_REPO_NAME:$TAG;

echo "Pushing changes to the dockerhub repository..."
docker push $DOCKER_HUB_USERNAME/$DOCKER_HUB_REPO_NAME:$TAG;

echo "Done."