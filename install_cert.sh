#!/usr/bin/bash


#usage: bash install_cert.sh </data/mycert.der> <cornel_cert> </usr/local/lib/keytool> </usr/local/jdk/jre/lib/cacerts> <changeit>

#cert file along with path
CERTFILE=$1
#cert added as alias 
CERTALIAS=$2

#Keytool along with path, may be can make use of JAVA_HOME if its set
KEYTOOL=$3
KEYSTORE=$4
KEYSTORE_PWD=$5


if [ "$#" -ne 5 ]
then
	echo "Illegal number of parameters."
	echo "Usage: bash install_cert.sh </data/mycert.der> <cornel_cert> </usr/local/lib/keytool> </usr/local/jdk/jre/lib/cacerts> <changeit>"
	exit 1
fi


if [ -e "$CERTFILE" ]
then
	echo  "Adding '$CERTFILE' to '$KEYSTORE'...."
fi



if $KEYTOOL -list -keystore $KEYSTORE -storepass ${KEYSTORE_PWD} -alias $CERTALIAS >/dev/null
    then
	echo "Key of $CERTALIAS already found, skipping it."
    else
    	$KEYTOOL -import -alias $CERTALIAS -noprompt -keystore $KEYSTORE -storepass ${KEYSTORE_PWD}  -file $CERTFILE
fi
