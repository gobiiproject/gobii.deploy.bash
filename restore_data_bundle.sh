#!/usr/bin/env bash
#This script restores the data files to the gobii_bundle directory from a specified backup path.
#This was written for the purpose of delivering patches and bug fixes without wiping out production data.
#bash ../Repositories/gobiideployment/restore_data_bundle.sh /data/gobii_bundle/ /shared_data/bamboo_gobii_backups/qa_test_incremental/daily
if [ $# -lt 2 ]; then
    echo "Invalid usage."
    echo "Usage: bash restore_data_bundle.sh <path_to_bundle> <backup_path>"
    echo "Description:"
    echo "This script will simply restore data from a previous GOBii bundle backup directory to an existing gobii_bundle."
  	exit 1
fi

if [ ! -d $1 ]; then
    echo "$1 is NOT a directory. Aborting."
    exit 0
fi

if [ ! -d $2 ]; then
    echo "$2 is NOT a directory. Aborting."
    exit 0
fi

#Things to restore:
# 1. crops/**
# 2. logs/**

GOBII_BUNDLE_DIR=$1
BACKUP_DIR=$2

echo "Restoring data from $BACKUP_DIR..."

echo "Moving crops/**..."
rsync -ahIL --progress --update $BACKUP_DIR/crops $GOBII_BUNDLE_DIR

echo "Moving logs/**..."
rsync -ahIL --progress --update $BACKUP_DIR/logs $GOBII_BUNDLE_DIR
