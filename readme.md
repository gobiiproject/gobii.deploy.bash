```bash
   _____  ____  ____  _ _    _____             _                                  _   
  / ____|/ __ \|  _ \(_|_)  |  __ \           | |                                | |  
 | |  __| |  | | |_) |_ _   | |  | | ___ _ __ | | ___  _   _ _ __ ___   ___ _ __ | |_ 
 | | |_ | |  | |  _ <| | |  | |  | |/ _ \ '_ \| |/ _ \| | | | '_ ` _ \ / _ \ '_ \| __|
 | |__| | |__| | |_) | | |  | |__| |  __/ |_) | | (_) | |_| | | | | | |  __/ | | | |_ 
  \_____|\____/|____/|_|_|  |_____/ \___| .__/|_|\___/ \__, |_| |_| |_|\___|_| |_|\__|
  ____            _                     | |             __/ |                         
 |  _ \          | |                    |_|            |___/                          
 | |_) | __ _ ___| |__                                                                
 |  _ < / _` / __| '_ \                                                               
 | |_) | (_| \__ \ | | |                                                              
 |____/ \__,_|___/_| |_|                                                              
```
**Version 2.2**

Author: rpetrie [rlp243@cornell.edu]

---

_This process will present the deployment strageties and direction of what will need to be accomplished in order to deploy GDM via this repositories bash scripts._

---

### Prerequisites & Versions
_This version has been tested with the following versions. It is possible a deployment can be done with different versions than listed below but may be unstable or fail at deployment time._

| System/App   | Version      | 
| ------------ | ------------ |
| **Ubuntu:** [_Host_]  | 16.04.6 LTS  |
| **Ubuntu:** [_Image_]  | 18.04.3 LTS  |
| **Bash** [_Host_]  |  4.3.48(1)-release (x86_64-pc-linux-gnu)  |
| **Bash** [_Image_]  |  4.4.20(1)-release (x86_64-pc-linux-gnu)  |
| **Docker**  |  18.06.1, 19.03.5  |
| **git**  |  2.7.4  |
| **OpenJDK** [_Image_] |  13.0.1  |
| **java** [_Host_] |  1.8.0_191  |

#### Host configuration(s)
The host system(s) will need to have the following configurations:

**\*** Required

**\*** 1.) A **\\data** directory (or symlink) must exist as all gadm processes point to this location.
> **Recommended:** Most sytems use a network file system that is mounted to the host system(s) and all persistent data is targeted to be kept within the mounted file system. Please refer to our [**Non-Destructive Deployment Architecture**](https://gobiiproject.atlassian.net/wiki/spaces/GD/pages/110592003/GOBii+Non-Destructive+Deployment+Architecture) documentation to understand the filesystem configuration. 
>> _The **ndd_pathing.sh** script can be run after deployment to verify symlinks and the file system configuration post deployment._

**\*** 2.) Install Docker on all host systems
> **Future Proofing:** All future versions past release 2.2 are being architected to be system and cloud agnostic using _Docker Swarm_. It is recommended that all systems use docker version 18.09 or higher to be compliant with future releases.

**\*** 3.) Use the following command to create gobii user and group.  These commands will also add the user to the gobii group and set the gobii group as its primary group.
```bash
sudo useradd -m gadm && \
sudo groupadd gobii && \
sudo usermod -aG gobii gadm && \
sudo usermod -g gobii gadm
```
_Don't forget to give gadm sudo and set its password_
> **Replace:** <password\>
```bash
sudo echo "gadm ALL=(ALL:ALL) ALL" >> /etc/sudoers && \
echo -e "<password>\n<password>\n" | passwd gadm
```

4.) Setup workspace in **/home/gadm/workspace**. This is not required but it is recommended to a have a unified working directory in which to clone the deployment repositories.

5.) Create aliases for easier container administration:
```bash
cat gadm_aliases >> ~/gadm/.bashrc
```

---

### Parameters
_The scripts have been built to pull parameters specified at script run time. A default set of parameters can be located within the **params/** directory. These parameters will need to be updated to match the environment GDM is being deployed to._
> **Password Parameters:** The scripts have been built to prompt for password entries at run time as to keep passwords secret if the parameter "**askme**" is used in any of the **\*PASSWORD\*** variables defined by the **gobii.paramters** file.

**Running Deployment:**

Using the following command will kick off the GDM deployment:

```bash
bash <path/to/gobii.deploy.bash>/the_gobii_gobii_ship.sh <path/to/gobii.parameters> <dockerhub user password or "askme"> <container image tag for version>
```
>_Example:_
> ```bash
> bash /data/workspace/gobii.deploy.bash/the_gobii_ship.sh /data/workspace/gobii.deploy.bash/params/gobii.parameters askme release-2.1-1
> ```

Additionally, this can be run from an already pre-existing compute-node or any container on the host system that has java installed. Please refer to the referenced java versions above.

---

## Post Deployment & Testing

In order to determine the deployment was a success please refer to the [BeRT Documentation](https://bitbucket.org/gobiiproject/gobii.bert-scenarios/wiki/Home).