This script is not used and is in place for a place holder until current development of the pedver app has been completed.

#!/usr/bin/env bash

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
set -x # xtrace: Similar to -v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#
### !!! WARNING !!!
#-----------------------------------------------------------------------------#
### Do not run this script as SUDO! There are explicit sudo commands which
### will prompt you for password. But not everything should run as sudo.

### usage: bash <bash_script_name>.sh <path of gobii.parameters> <dockerhubpassw | "askme"> <gobii_release_version>

#-----------------------------------------------------------------------------#
### Requirements: 

###>>> 1. The user that will run this script needs to be a sudoer and under the gobii and docker groups. So preferably the user 'gadm'.

###>>> 2. The working directory needs to be where the gobiiconfig_wrapper.sh 
# is as well, typically <gobii_bundle>/conf/
###--->>> NOTE: The order of execution is important.
###--->>> NOTE: If weird things start happening on your containers, try 
# removing the images as well by running 'docker rmi' on each of the 3 nodes.
#-----------------------------------------------------------------------------#
### If you want a delete-all-images command, run this: 
###>>> [sudo docker stop $(sudo docker ps -qa) || true && sudo docker rm 
# $(sudo docker ps -aq) || true && sudo docker rmi $(sudo docker images -aq) 
# || true]
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
#@author: (palace) kdp44@cornell.edu
###>>> Co-Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#


#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

if [ $# -lt 3 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash <bash_script_name>.sh <path of gobii.parameters> <dockerhubpassw | \"askme\"> <gobii_release_version>"
    echo "Set dockerhubpassw parameter to 'askme' for the script to prompt for password instead."
    exit 1
fi

#-----------------------------------------------------------------------------#
### load parameters -- gobii.parameters for deployment
#-----------------------------------------------------------------------------#

set +x

source $1

#-----------------------------------------------------------------------------#
### Set release version and Dockerhub password
###>>> if pass set to 'askme' call for user to enter password at cli
#-----------------------------------------------------------------------------#

DOCKER_HUB_PASSWORD=$2

#-----------------------------------------------------------------------------#
### GDM Release Version
#-----------------------------------------------------------------------------#

GOBII_RELEASE_VERSION=$3

#-----------------------------------------------------------------------------#
### Requesting passwords [Dockerhub]
#-----------------------------------------------------------------------------#
###>>> By default the same password will be set for all administration and 
###>>> gadm users. If a different password is needed, uncomment the 'if' ###>>> statements below to interactively enter the different passwords
#-----------------------------------------------------------------------------#

echo;

if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
fi
echo;

set -x

#-----------------------------------------------------------------------------#
### Creating /data symlink if not already created
#-----------------------------------------------------------------------------#

#sudo ln -sfn $BUNDLE_PARENT_PATH /data || true

#-----------------------------------------------------------------------------#
### Starting pedver deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the pedver-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove PEDVER container
#-----------------------------------------------------------------------------#

docker stop $DOCKER_PEDVER_NAME || true && docker rm $DOCKER_PEDVER_NAME || true

#-----------------------------------------------------------------------------#
### Pull and start the PEDVER image
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_PEDVER_NAME:$GOBII_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Run the pedver-node image
#-----------------------------------------------------------------------------#

docker run -i --detach \
--name $DOCKER_PEDVER_NAME \
-h $DOCKER_PEDVER_CONTAINER_HOSTNAME \
-v ${NDD_PATH}:${NDD_PATH} \
-v $BUNDLE_PARENT_PATH:/data \
-p $DOCKER_PEDVER_SSH_PORT:22 \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_PEDVER_NAME:$GOBII_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Start the pedver-node image from dockerhub
#-----------------------------------------------------------------------------#

docker start $DOCKER_PEDVER_NAME;

#-----------------------------------------------------------------------------#
### pedver-node deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;

echo "pedver-node deployment script completion!"
echo;
