#!/usr/bin/env bash

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
set -x # xtrace: Similar to #-v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#
### !!! WARNING !!!
#-----------------------------------------------------------------------------#
### Do not run this script as SUDO! There are explicit sudo commands which
### will prompt you for password. But not everything should run as sudo.

### usage: bash <bash_script_name>.sh <path of gobii.parameters> <dockerhubpassw | "askme"> <LDAP_IMAGE_TAG>

#-----------------------------------------------------------------------------#
### Requirements: 

###>>> 1. The user that will run this script needs to be a sudoer and under 
# the gobii and docker groups. So preferably the user 'gadm'.

###>>> 2. The working directory needs to be where the gobiiconfig_wrapper.sh 
# is as well, typically <gobii_bundle>/conf/
###--->>> NOTE: The order of execution is important.
###--->>> NOTE: If weird things start happening on your containers, try 
# removing the images as well by running 'docker rmi' on each of the 3 nodes.
#-----------------------------------------------------------------------------#
### If you want a delete-all-images command, run this: 
###>>> [sudo docker stop $(sudo docker ps -qa) || true && sudo docker rm 
# $(sudo docker ps -aq) || true && sudo docker rmi $(sudo docker images -aq) 
# || true]
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
#@author: (palace) kdp44@cornell.edu
###>>> Co-Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#


#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

if [ $# -lt 3 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash <bash_script_name>.sh <path of gobii.parameters> <dockerhubpassw | \"askme\"> <LDAP_IMAGE_TAG>"
    echo "Set dockerhubpassw parameter to 'askme' for the script to prompt for password instead."
    exit 1
fi

#-----------------------------------------------------------------------------#
### load parameters -- gobii.parameters for deployment
#-----------------------------------------------------------------------------#

set +x

source $1

#-----------------------------------------------------------------------------#
### Set release version and Dockerhub password
###>>> if pass set to 'askme' call for user to enter password at cli
#-----------------------------------------------------------------------------#

DOCKER_HUB_PASSWORD=$2

#-----------------------------------------------------------------------------#
### GDM Release Version
#-----------------------------------------------------------------------------#

LDAP_IMAGE_TAG="askme"

echo;

if [ $LDAP_IMAGE_TAG = "askme" ]; then
    read -p "Please enter the LDAP Container Image Tag: " LDAP_IMAGE_TAG
fi
echo;

#-----------------------------------------------------------------------------#
### Requesting passwords [Dockerhub]
#-----------------------------------------------------------------------------#
###>>> By default the same password will be set for all administration and 
###>>> gadm users. If a different password is needed, uncomment the 'if' ###>>> statements below to interactively enter the different passwords
#-----------------------------------------------------------------------------#

echo;

if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
fi
echo;

set -x

#-----------------------------------------------------------------------------#
### Creating /data symlink if not already created
#-----------------------------------------------------------------------------#

#sudo ln -sfn $BUNDLE_PARENT_PATH /data || true

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Starting ldap-node deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the ldap-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove web containers
#-----------------------------------------------------------------------------#

docker stop $DOCKER_LDAP_NAME || true && docker rm $DOCKER_LDAP_NAME || true

#-----------------------------------------------------------------------------#
### Pull the ldap-node image from dockerhub
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x 

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_LDAP_NAME:$LDAP_IMAGE_TAG;

#-----------------------------------------------------------------------------#
### Run the ldap-node image
#-----------------------------------------------------------------------------#

docker run -dti \
--name $DOCKER_LDAP_NAME \
-h $DOCKER_LDAP_CONTAINER_HOSTNAME \
-v ${NDD_PATH}:${NDD_PATH} \
-v $BUNDLE_PARENT_PATH:/data \
-p $DOCKER_LDAP_PORT:389 \
-p $DOCKER_LDAP_SSL_PORT:636 \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_LDAP_NAME:$LDAP_IMAGE_TAG;

#-----------------------------------------------------------------------------#
### Start the web-node image from dockerhub
#-----------------------------------------------------------------------------#

docker start $DOCKER_LDAP_NAME;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_LDAP_NAME

#-----------------------------------------------------------------------------#
### Container Update & Install vim
#-----------------------------------------------------------------------------#

docker exec $DOCKER_LDAP_NAME bash -c "
apt update;
apt install -y vim;
";

sleep 5


#-----------------------------------------------------------------------------#
### Start LDAP Service
#-----------------------------------------------------------------------------#


docker exec $DOCKER_LDAP_NAME sh -c "service slapd start"
sleep 3
echo;

docker exec $DOCKER_LDAP_NAME sh -c "service slapd status"
sleep 3
echo;



#-----------------------------------------------------------------------------#
### ldap-node deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;

echo "ldap-node deployment script completion!"
echo;
