#!/usr/bin/env bash

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
set -x # xtrace: Similar to -v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#
### !!! WARNING !!!
#-----------------------------------------------------------------------------#
### Do not run this script as SUDO! There are explicit sudo commands which
### will prompt you for password. But not everything should run as sudo.

### usage: bash <bash_script_name>.sh <path of main.parameters> <path of 
# install.parameters> <dockerhubpassw | "askme"> <gobii_release_version>

#-----------------------------------------------------------------------------#
### Requirements: 

###>>> 1. The user that will run this script needs to be a sudoer and under the gobii and docker groups. So preferably the user 'gadm'.

###>>> 2. The working directory needs to be where the gobiiconfig_wrapper.sh 
# is as well, typically <gobii_bundle>/conf/
###--->>> NOTE: The order of execution is important.
###--->>> NOTE: If weird things start happening on your containers, try 
# removing the images as well by running 'docker rmi' on each of the 3 nodes.
#-----------------------------------------------------------------------------#
### If you want a delete-all-images command, run this: 
###>>> [sudo docker stop $(sudo docker ps -qa) || true && sudo docker rm 
# $(sudo docker ps -aq) || true && sudo docker rmi $(sudo docker images -aq) 
# || true]
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
#@author: (palace) kdp44@cornell.edu
###>>> Co-Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#


#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

if [ $# -lt 3 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash <bash_script_name>.sh <path of gobii.parameters> <dockerhubpassw | \"askme\"> <gobii_release_version>"
    echo "Set dockerhubpassw parameter to 'askme' for the script to prompt for password instead."
    exit 1
fi

#-----------------------------------------------------------------------------#
### load parameters -- main.parameters for deployment
#-----------------------------------------------------------------------------#

set +x

source $1

#-----------------------------------------------------------------------------#
### Set release version and Dockerhub password
###>>> if pass set to 'askme' call for user to enter password at cli
#-----------------------------------------------------------------------------#

DOCKER_HUB_PASSWORD=$2

#-----------------------------------------------------------------------------#
### KDCompute Version
#-----------------------------------------------------------------------------#

KDC_RELEASE_VERSION=$3

#-----------------------------------------------------------------------------#
### Requesting passwords [Dockerhub]
#-----------------------------------------------------------------------------#
###>>> By default the same password will be set for all administration and 
###>>> gadm users. If a different password is needed, uncomment the 'if' ###>>> statements below to interactively enter the different passwords
#-----------------------------------------------------------------------------#


echo;

if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
fi
echo;


if [ $KDC_RELEASE_VERSION = "askme" ]; then
    read -p "Please enter KDCompute release version: " KDC_RELEASE_VERSION
fi
echo;

set -x

#-----------------------------------------------------------------------------#
### Creating /data symlink if not already created
#-----------------------------------------------------------------------------#

#sudo ln -sfn $BUNDLE_PARENT_PATH /data || true

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Starting KDCompute deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting KDCompute deployment..."
echo;

set -x

 #-----------------------------------------------------------------------------#
### Stop and remove KDCompute containers
#-----------------------------------------------------------------------------#

docker stop $DOCKER_KDC_NAME || true && docker rm $DOCKER_KDC_NAME || true

#-----------------------------------------------------------------------------#
### Pull the KDC image from dockerhub
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_KDC_NAME:$KDC_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Removing server.admin
#-----------------------------------------------------------------------------#

rm -rf $BUNDLE_PARENT_PATH/server.admin

#-----------------------------------------------------------------------------#
### Run the KDC image
#-----------------------------------------------------------------------------#

#TODO: Parameterize ports 3308 and 2228 below!
docker run -i --detach \
--name $DOCKER_KDC_NAME \
-h $DOCKER_KDC_CONTAINER_HOSTNAME \
-v ${NDD_PATH}:${NDD_PATH} \
-v $BUNDLE_PARENT_PATH:/data \
-v $BUNDLE_PARENT_PATH:/usr/local/tomcat/kdcompute_file_storage/TestOutput_UserDirs \
-p $KDC_PORT:$KDC_PORT_INTERNAL \
-p 3308:3306 \
-p 2228:22 \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_KDC_NAME:$KDC_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Start the KDC image from dockerhub
#-----------------------------------------------------------------------------#

docker start $DOCKER_KDC_NAME;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_KDC_NAME

#-----------------------------------------------------------------------------#
### Update uid & gid of gadm:gobii within the container
### Matching these will be shown as same user:group as recognized by host
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Matching the docker gadm account to that of the host's and changing file ownerships..."
echo;

set -x

DOCKER_CMD="usermod -u $GOBII_UID gadm;";
eval docker exec $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="groupmod -g $GOBII_GID gobii;";
eval docker exec $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -user 1000 -exec chown -h gadm {} \; || :";
eval docker exec $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -group 1001 -exec chgrp -h gobii {} \; || :";
eval docker exec $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### [Deprecated] - KDCompute file storage management
###>>> This has been deprecated but kept for future reference 
#-----------------------------------------------------------------------------#
# Set KDC file storage
# echo "Setting the KDC file storage for GOBII..."
#-----------------------------------------------------------------------------#
### TODO: Sanitize the KDC_FILE_STORAGE_DIR. Prevent the possibility of getting /data deleted.
#Commented out as I haven't tested this yet
#if [[ -v KDC_FILE_STORAGE_DIR ]] && [[ $KDC_FILE_STORAGE_DIR != *"/"* ]] && [[ $KDC_FILE_STORAGE_DIR != *"\\"* ]] && [[ $KDC_FILE_STORAGE_DIR != *"."* ]]; 
#then	
#-----------------------------------------------------------------------------#
# DOCKER_CMD="rm -rf /data/$KDC_FILE_STORAGE_DIR";
# eval docker exec $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";
#else
#	echo "Invalid KDC_FILE_STORAGE_DIR. Make sure it doesn't have any special characters."
#fi

#-----------------------------------------------------------------------------#
### [Deprecated] - KDCompute file storage move
#-----------------------------------------------------------------------------#
# DOCKER_CMD="cd /var; cp -R $KDC_FILE_STORAGE_DIR /data/$KDC_FILE_STORAGE_DIR";
# eval docker exec $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
### [Deprecated] - KDCompute file storage ownership & permissions
#-----------------------------------------------------------------------------#
# DOCKER_CMD="chown -R gadm:gobii /data/$KDC_FILE_STORAGE_DIR";
# eval docker exec $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# [Deprecated] - Start MariaDB on KDC
#-----------------------------------------------------------------------------#
# DOCKER_CMD="service mysql start";
# docker exec $DOCKER_KDC_NAME service mysql start;
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
### Restarting tomcat
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Restarting tomcat as gadm..."
echo;

set -x

#-----------------------------------------------------------------------------#
### tomcat stop
#-----------------------------------------------------------------------------#

DOCKER_CMD="cd /usr/local/tomcat/bin/; sh shutdown.sh;";
eval docker exec -u $DOCKER_SUDOER_USERNAME $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### tomcat start
#-----------------------------------------------------------------------------#

DOCKER_CMD="cd /usr/local/tomcat/bin/; sh startup.sh;";
eval docker exec -u $DOCKER_SUDOER_USERNAME $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";
echo;

#-----------------------------------------------------------------------------#
# Updating KDC password using the kdc_passwd.sh script

# cp ./devops_files/kdc_passwd.sh /data

# read -sp "Please enter the KDC DB root password: " DB_PASSWORD
# echo;


# read -sp "Please enter the new KDC admin password: " new_password
# echo; 

# DOCKER_CMD="bash /data/kdc_passwd.sh $DB_PASSWORD $new_password;";
# eval docker exec $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";
# echo;

#-----------------------------------------------------------------------------#
### KDCompute deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;

echo "KDCompute deployment script completion!"
echo;
