#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Keycloak Deployment
#-----------------------------------------------------------------------------#

# 1.) Deploy Container
docker run -dti \
--name gobii-kc-node \
-h kc-node \
-p 8080:8080 \
-e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
--network=bridge \
jboss/keycloak

sleep 5

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge gobii-kc-node

# 2.) Add/Configure admin user
docker exec -it gobii-kc-node bash -c " 
cd keycloak/bin; \
./add-user-keycloak.sh \
--realm master \
--user admin \
--password admin
"

sleep 5

# 3.) Login with admin user on master realm and change no ssl
docker exec -it gobii-kc-node bash -c "
cd keycloak/bin; \
./kcadm.sh config credentials \
--server http://localhost:8080/auth \
--realm master \
--user admin \
--password admin;
./kcadm.sh update \
realms/master \
-s sslRequired=NONE
"

sleep 5

docker restart gobii-kc-node

