#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "No Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "No Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Starting KDCompute deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting KDCompute deployment..."
echo;

set -x

 #-----------------------------------------------------------------------------#
### Stop and remove KDCompute containers
#-----------------------------------------------------------------------------#

docker rm -f $DOCKER_KDC_NAME || true;

#-----------------------------------------------------------------------------#
### Pull the KDC image from dockerhub
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_KDC_NAME:$KDC_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Removing server.admin
#-----------------------------------------------------------------------------#

rm -rf $BUNDLE_PARENT_PATH/server.admin || true;

#-----------------------------------------------------------------------------#
### Run the KDC image
#-----------------------------------------------------------------------------#

#TODO: Parameterize ports 3308 and 2228 below!
docker run -i --detach \
--name $DOCKER_KDC_NAME \
-h $DOCKER_KDC_CONTAINER_HOSTNAME \
-v ${NDD_PATH}:${NDD_PATH} \
-v $BUNDLE_PARENT_PATH:/data \
-v $BUNDLE_PARENT_PATH:/usr/local/tomcat/kdcompute_file_storage/TestOutput_UserDirs \
-p $KDC_PORT:$KDC_PORT_INTERNAL \
-p 3308:3306 \
-p 2228:22 \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_KDC_NAME:$KDC_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Start the KDC image from dockerhub
#-----------------------------------------------------------------------------#

docker start $DOCKER_KDC_NAME;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_KDC_NAME

#-----------------------------------------------------------------------------#
### Update uid & gid of gadm:gobii within the container
### Matching these will be shown as same user:group as recognized by host
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Matching the docker gadm account to that of the host's and changing file ownerships..."
echo;

set -x

DOCKER_CMD="usermod -u $GOBII_UID gadm;";
eval docker exec $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="groupmod -g $GOBII_GID gobii;";
eval docker exec $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -user 1000 -exec chown -h gadm {} \; || :";
eval docker exec $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -group 1001 -exec chgrp -h gobii {} \; || :";
eval docker exec $DOCKER_KDC_NAME bash -c \"${DOCKER_CMD}\";



set +x
echo;

echo "Restarting tomcat as gadm..."
echo;

set -x

#-----------------------------------------------------------------------------#
### tomcat restart
#-----------------------------------------------------------------------------#

docker exec -i -u gadm $DOCKER_KDC_NAME bash -c "
/usr/local/tomcat/bin/shutdown.sh;
sleep 3;
/usr/local/tomcat/bin/startup.sh;
sleep 3
exit;
";

#-----------------------------------------------------------------------------#
### KDCompute deployment script completion
#-----------------------------------------------------------------------------#
