#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "No Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "No Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Starting Haplo Tool deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the haplo-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove Haplo Container
#-----------------------------------------------------------------------------#

docker rm -f $DOCKER_HAPLO_NAME || true;

#-----------------------------------------------------------------------------#
### Pull and start the COMPUTE image
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_HAPLO_NAME:$DOCKER_HUB_HAPLO_TAG;

#-----------------------------------------------------------------------------#
### Run the haplo-node image
#-----------------------------------------------------------------------------#

docker run -dti \
--name $DOCKER_HAPLO_NAME \
-h $DOCKER_HAPLO_CONTAINER_HOSTNAME \
-v ${NDD_PATH}:${NDD_PATH} \
--entrypoint /bin/bash \
--user shiny \
-v $BUNDLE_PARENT_PATH:/data \
-p $DOCKER_HAPLO_PORT:3838 \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_HAPLO_NAME:$DOCKER_HUB_HAPLO_TAG;

#-----------------------------------------------------------------------------#
### Start the haplo-node image from dockerhub
#-----------------------------------------------------------------------------#

docker start $DOCKER_HAPLO_NAME;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_HAPLO_NAME

#-----------------------------------------------------------------------------#
### Start the shiny service
#-----------------------------------------------------------------------------#

echo "Starting Shiny Service..."
echo;

docker exec -u root $DOCKER_HAPLO_NAME bash -c "
sudo -H -u shiny /usr/bin/shiny-server.sh;
exit;
" || true;
echo;

#-----------------------------------------------------------------------------#
### haplo-node deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;

echo "haplo-node deployment script completion!"
echo;
