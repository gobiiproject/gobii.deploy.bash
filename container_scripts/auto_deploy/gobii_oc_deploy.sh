#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "No Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "No Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Starting ldap-node deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the oc-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove container
#-----------------------------------------------------------------------------#

docker rm -f $DOCKER_OC_NAME || true;

#-----------------------------------------------------------------------------#
### Pull the ldap-node image from dockerhub
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x 

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_OC_NAME:$DOCKER_OC_VERSION;

#-----------------------------------------------------------------------------#
### Run the oc-node image
#-----------------------------------------------------------------------------#

docker run -i --detach --name $DOCKER_OC_NAME \
-h $DOCKER_OC_CONTAINER_HOSTNAME \
-v ${NDD_PATH}:${NDD_PATH} \
-v $BUNDLE_PARENT_PATH:/data \
-p $DOCKER_OC_PORT:8080 \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_OC_NAME:$DOCKER_OC_VERSION;

# Starting oc Container
docker start $DOCKER_OC_NAME;

echo "Sleeping to give system time to start..."

sleep 30

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_OC_NAME

#-----------------------------------------------------------------------------#
### Enabling ownCloud external Storage
#-----------------------------------------------------------------------------#

docker exec $DOCKER_OC_NAME bash -c "
occ app:enable files_external
occ config:app:set core enable_external_storage --value=yes
" || true;

#-----------------------------------------------------------------------------#
### Enable local storage mount
#-----------------------------------------------------------------------------#

docker exec $DOCKER_OC_NAME bash -c "
cd /var/www/owncloud/config;
mv config.php config.php.original;
sed '50 i 'files_external_allow_create_new_local' => 'true',' config.php.original > config.php;
chown www-data:root config.php;
" || true;

#-----------------------------------------------------------------------------#

docker exec $DOCKER_OC_NAME bash -c "
cp mimetypemapping.json /var/www/owncloud/config/
chown www-data:www-data /var/www/owncloud/config/mimetypemapping.json
occ file:scan --all
";

#-----------------------------------------------------------------------------#