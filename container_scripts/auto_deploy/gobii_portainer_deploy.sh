#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "No Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "No Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;


#-----------------------------------------------------------------------------#
### Starting compute deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the portainer-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove COMPUTE container
#-----------------------------------------------------------------------------#

docker stop $DOCKER_PORTAINER_NAME || true && docker rm $DOCKER_PORTAINER_NAME || true

#-----------------------------------------------------------------------------#
### Pull and start the COMPUTE image
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull portainer/portainer;

#-----------------------------------------------------------------------------#
### Run the PORTAINER image
#-----------------------------------------------------------------------------#

if [[ -v DOCKER_PORTAINER_VERSION ]]; then
docker run -i --detach \
--name $DOCKER_PORTAINER_NAME \
-h $DOCKER_PORTAINER_CONTAINER_HOSTNAME \
-p $PORTAINER_PORT:9000 \
--restart=always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v ${NDD_PATH}:${NDD_PATH} \
-v $BUNDLE_PARENT_PATH:/data \
--network=$DOCKER_NETWORK_NAME \
portainer/portainer:$DOCKER_PORTAINER_VERSION;
else
docker run -i --detach \
--name $DOCKER_PORTAINER_NAME \
-h $DOCKER_PORTAINER_CONTAINER_HOSTNAME \
-p $PORTAINER_PORT:9000 \
--restart always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v $BUNDLE_PARENT_PATH:/data \
--network=$DOCKER_NETWORK_NAME \
portainer/portainer;
fi

#-----------------------------------------------------------------------------#
### Start the PORTAINER CONTAINER
#-----------------------------------------------------------------------------#

docker start $DOCKER_PORTAINER_NAME;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_PORTAINER_NAME

#-----------------------------------------------------------------------------#
### portainer-node deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;

echo "portainer-node deployment script completion!"
echo;
