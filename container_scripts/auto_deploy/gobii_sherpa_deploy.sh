#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "No Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "No Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Starting sherpa deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the portainer-sherpa-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove sherpa container
#-----------------------------------------------------------------------------#

docker rm -f $DOCKER_SHERPA_AGENT_NAME || true

#-----------------------------------------------------------------------------#
### Pull and start the sherpa image
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull djenriquez/sherpa:latest;

#-----------------------------------------------------------------------------#
### Run the portainer-sherpa-node image
#-----------------------------------------------------------------------------#

docker run -d \
--name $DOCKER_SHERPA_AGENT_NAME \
-h $DOCKER_SHERPA_CONTAINER_HOSTNAME \
-e CONFIG="[ \
    { \
        \"Path\" : \"/\", \
        \"Access\": \"allow\", \
        \"Addresses\": [$DOCKER_SHERPA_NETWORK_RULES] \
    } \
]" \
-v /var/run/docker.sock:/tmp/docker.sock \
-p 4550:4550 \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
djenriquez/sherpa:latest --allow

#-----------------------------------------------------------------------------#
### Start the portainer-sherpa-node image from dockerhub
#-----------------------------------------------------------------------------#

docker start $DOCKER_SHERPA_AGENT_NAME

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_SHERPA_AGENT_NAME
