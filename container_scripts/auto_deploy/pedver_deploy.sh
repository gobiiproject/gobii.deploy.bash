#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "No Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "No Sourcing \$2"; fi

#-----------------------------------------------------------------------------#

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $PEDVER_DOCKER_NET || true;

docker network connect $PEDVER_DOCKER_NET $DOCKER_HAPLO_NAME || true;
docker network connect $PEDVER_DOCKER_NET $DOCKER_PORTAINER_NAME || true;
docker network connect $PEDVER_DOCKER_NET $DOCKER_OC_NAME || true;
docker network connect $PEDVER_DOCKER_NET $DOCKER_KDC_NAME || true;
docker network connect $PEDVER_DOCKER_NET $DOCKER_WEB_NAME || true;
docker network connect $PEDVER_DOCKER_NET $DOCKER_DB_NAME || true;
docker network connect $PEDVER_DOCKER_NET $DOCKER_COMPUTE_NAME || true;
if [[ -v DOCKER_SHERPA_AGENT_NAME ]]; then
docker network connect $PEDVER_DOCKER_NET $DOCKER_SHERPA_AGENT_NAME || true;
fi

#-----------------------------------------------------------------------------#
### Starting compute deployment
#-----------------------------------------------------------------------------#

echo "Starting the pedver deployment..."
echo;

#-----------------------------------------------------------------------------#
### Stop and remove containers
#-----------------------------------------------------------------------------#

# removing current docker containers
docker rm -f gobiimarkerapp-worker gobiimarkerapp redis || true;
echo;

docker rm -f $DOCKER_PEDVER_WEB_NAME $DOCKER_PEDVER_WORKER_NAME $DOCKER_PEDVER_REDIS_NAME || true;

docker rmi $DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_REDIS:$PEDVER_RELEASE_TAG || true; 
docker rmi $DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_WEB:$PEDVER_RELEASE_TAG || true; 
docker rmi $DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_WORKER:$PEDVER_RELEASE_TAG || true;

#-----------------------------------------------------------------------------#
### Setting location vars
#-----------------------------------------------------------------------------#

# Setting env params
HOST_DATA_VOLUME=$PEDVER_DATA_VOLUME
echo;

# Create directories for pedver data

mkdir ${HOST_DATA_VOLUME} || true;

mkdir ${HOST_DATA_VOLUME}/data || true;

mkdir ${HOST_DATA_VOLUME}/instance || true;

# Export vars
export CONTAINER_DATA_FOLDER=/data/
echo;

export CONTAINER_INSTANCE_FOLDER=/gobiimarkerapp/instance/
echo;

#-----------------------------------------------------------------------------#
# Copy flapjack jar file to instance folder, so it is easy to just replace jars and restart

# containers when there is a flapjack update
cp -rf devops_files/flapjackjars ${HOST_DATA_VOLUME}/instance/ 
echo;

#-----------------------------------------------------------------------------#
### Docker Login

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

#-----------------------------------------------------------------------------#
### Pull images
#-----------------------------------------------------------------------------#

docker pull $DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_REDIS:$PEDVER_RELEASE_TAG;

docker pull $DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_WEB:$PEDVER_RELEASE_TAG;

docker pull $DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_WORKER:$PEDVER_RELEASE_TAG;

#-----------------------------------------------------------------------------#
### Start Container builds & deployments

# Deploy redis
docker run -dti \
--name $DOCKER_PEDVER_REDIS_NAME \
-h $CONTAINER_PEDVER_NAME_REDIS \
-p $PEDVER_REDIS_PORT:6379 \
--restart=always \
--network=$PEDVER_DOCKER_NET \
$DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_REDIS:$PEDVER_RELEASE_TAG
echo;

# Copy config.ini for container access
cp devops_files/pedver/config.ini ${HOST_DATA_VOLUME}/instance/
echo;

# [deprecated] Setting redis IP from docker network
#SET_INSPECT="{{ .NetworkSettings.Networks.$PEDVER_DOCKER_NET.IPAddress }}"
#REDIS_IP=$(docker inspect --format="$SET_INSPECT" $DOCKER_PEDVER_REDIS_NAME)
#echo;

REDIS_NAME="gobii-pedver-redis"

sed -ie "s~CELERY_BROKER_URL = redis://localhost:6379/0~CELERY_BROKER_URL = redis://$REDIS_NAME:6379/0~g" ${HOST_DATA_VOLUME}/instance/config.ini

sed -ie "s~CELERY_RESULT_BACKEND = redis://localhost:6379/0~CELERY_RESULT_BACKEND = redis://$REDIS_NAME:6379/0~g" ${HOST_DATA_VOLUME}/instance/config.ini

# Deploy app node
docker run -dti \
--name $DOCKER_PEDVER_WEB_NAME \
-h $CONTAINER_PEDVER_NAME_WEB \
-p $PEDVER_WEB_PORT:80 \
-v ${HOST_DATA_VOLUME}/data:${CONTAINER_DATA_FOLDER} \
-v ${HOST_DATA_VOLUME}/instance:${CONTAINER_INSTANCE_FOLDER} \
--restart=always \
--network=$PEDVER_DOCKER_NET \
$DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_WEB:$PEDVER_RELEASE_TAG
echo;

# Deploy worker node
docker run -dti  \
--name $DOCKER_PEDVER_WORKER_NAME \
-h $DOCKER_PEDVER_WORKER_NAME \
-v ${HOST_DATA_VOLUME}/data:${CONTAINER_DATA_FOLDER} \
-v ${HOST_DATA_VOLUME}/instance:${CONTAINER_INSTANCE_FOLDER} \
--restart=always \
--network=$PEDVER_DOCKER_NET \
$DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_WORKER:$PEDVER_RELEASE_TAG
echo;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_PEDVER_REDIS_NAME

docker network connect bridge $DOCKER_PEDVER_WEB_NAME

docker network connect bridge $DOCKER_PEDVER_WORKER_NAME

#-----------------------------------------------------------------------------#

set +x 

echo "PedVer Redeploy Completed"
echo;
