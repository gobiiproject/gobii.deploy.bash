#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "No Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "No Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Starting compute deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the compute-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove COMPUTE container
#-----------------------------------------------------------------------------#

docker rm -f $DOCKER_COMPUTE_NAME || true

#-----------------------------------------------------------------------------#
### Pull and start the COMPUTE image
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_COMPUTE_NAME:$GOBII_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Run the compute-node image
#-----------------------------------------------------------------------------#

docker run -i --detach \
--name $DOCKER_COMPUTE_NAME \
-h $DOCKER_COMPUTE_CONTAINER_HOSTNAME \
-v ${NDD_PATH}:${NDD_PATH} \
-v $BUNDLE_PARENT_PATH:/data \
-p $DOCKER_COMPUTE_SSH_PORT:22 \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
--health-cmd="crontab -u gadm -l | grep gobii_bundle || exit 1" \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_COMPUTE_NAME:$GOBII_RELEASE_VERSION;
echo;

#-----------------------------------------------------------------------------#
### Start the compute-node image from dockerhub
#-----------------------------------------------------------------------------#

docker start $DOCKER_COMPUTE_NAME;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_COMPUTE_NAME

#-----------------------------------------------------------------------------#
### Set the proper UID and GID and chown everything from within container
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Matching the docker gadm account to that of the host's and changing file ownerships..."
echo;

set -x

DOCKER_CMD="usermod -u $GOBII_UID gadm;";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="groupmod -g $GOBII_GID gobii;";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -user 1016 -exec chown -h $GOBII_UID {} \; || :";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -group 1016 -exec chgrp -h $GOBII_GID {} \; || :";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Grant permissions and set cronjobs
#-----------------------------------------------------------------------------#

echo "Granting permissions..."
DOCKER_CMD="chmod -Rv +rx /data/$DOCKER_BUNDLE_NAME/loaders/;";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="chmod -Rv +rx /data/$DOCKER_BUNDLE_NAME/extractors/;";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="chmod -Rv g+rwx /data/$DOCKER_BUNDLE_NAME/crops/*/files;";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Configuring cronjobs
#-----------------------------------------------------------------------------#
# Sets the cron jobs for all crops, if there is less than 2 crop, no error will 
# be thrown. This allows the script to be useful for CGs with differing number 
# of crops, without any modifications.
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Setting cron jobs..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Clear the current crontab
#-----------------------------------------------------------------------------#

DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; crontab -r || true;";
eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";

DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; sh addCron.sh /data/$DOCKER_BUNDLE_NAME $DOCKER_CROP1_NAME $DOCKER_CRON_INTERVAL $DOCKER_CRON_FILE_AGE;";
eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";

if [[ -v DOCKER_CROP2_NAME ]]; then
	DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; sh addCron.sh /data/$DOCKER_BUNDLE_NAME $DOCKER_CROP2_NAME $DOCKER_CRON_INTERVAL $DOCKER_CRON_FILE_AGE;";
	eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
fi

if [[ -v DOCKER_CROP3_NAME ]]; then
	DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; sh addCron.sh /data/$DOCKER_BUNDLE_NAME $DOCKER_CROP3_NAME $DOCKER_CRON_INTERVAL $DOCKER_CRON_FILE_AGE;";
	eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
fi

if [[ -v DOCKER_CROP4_NAME ]]; then
	DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; sh addCron.sh /data/$DOCKER_BUNDLE_NAME $DOCKER_CROP4_NAME $DOCKER_CRON_INTERVAL $DOCKER_CRON_FILE_AGE;";
	eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
fi

if [[ -v DOCKER_CROP5_NAME ]]; then
	DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; sh addCron.sh /data/$DOCKER_BUNDLE_NAME $DOCKER_CROP5_NAME $DOCKER_CRON_INTERVAL $DOCKER_CRON_FILE_AGE;";
	eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
fi

if [[ -v DOCKER_CROP6_NAME ]]; then
	DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; sh addCron.sh /data/$DOCKER_BUNDLE_NAME $DOCKER_CROP6_NAME $DOCKER_CRON_INTERVAL $DOCKER_CRON_FILE_AGE;";
	eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
fi

#-----------------------------------------------------------------------------#
### Enable Live Links

DOCKER_CMD="cd /data/gobii_bundle/config && java -jar gobiiconfig.jar -wfqpn gobii-web.xml -ownc -ocERR logs -soR crops -soH $EXTERNAL_HOST_IP_HOSTNAME -soN $DOCKER_LL_PORT -soU $LL_USER1 -soP $LL_USER1_PASS";
eval docker exec -u gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Configure Environment name for emails

if [ -z "$ENV_NAME" ]; then
	echo "Environment Name not set and skipping setting."
	echo;
else
	echo "Environment Name set as: $ENV_NAME"
	echo "Configuring Emails to have shown Environment Name."
	echo;
	JAVA_JAR="java -jar"
	ENV_CONF="java -Dorg.gobii.environment.name=\"$ENV_NAME\" -jar"
	sed -ie "s/$JAVA_JAR/$ENV_CONF/g" /data/gobii_bundle/loaders/cronjob.sh
	sed -ie "s/$JAVA_JAR/$ENV_CONF/g" /data/gobii_bundle/extractors/cronjob.sh
	echo "Configuration change: "
	echo;

	echo "/data/gobii_bundle/loaders/cronjob.sh"
	cat /data/gobii_bundle/loaders/cronjob.sh |grep '\-jar'
	echo;
	
	echo "/data/gobii_bundle/extractors/cronjob.sh"
	cat /data/gobii_bundle/extractors/cronjob.sh |grep '\-jar'
	echo;
fi

#-----------------------------------------------------------------------------#
### compute-node deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;
