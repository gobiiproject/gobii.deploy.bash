#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "No Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "No Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Starting web-node deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the web-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove web containers
#-----------------------------------------------------------------------------#

docker rm -f $DOCKER_WEB_NAME || true

#-----------------------------------------------------------------------------#
### Pull the web-node image from dockerhub
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x 

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_WEB_NAME:$GOBII_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Run the web-node image
#-----------------------------------------------------------------------------#

docker run -i --detach --name $DOCKER_WEB_NAME \
-h $DOCKER_WEB_CONTAINER_HOSTNAME \
-v ${NDD_PATH}:${NDD_PATH} \
-v $BUNDLE_PARENT_PATH:/data \
-p $DOCKER_WEB_PORT:8080 \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
--health-cmd="wget --quiet --spider http://localhost:8080/ || exit 1" \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_WEB_NAME:$GOBII_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Start the web-node image from dockerhub
#-----------------------------------------------------------------------------#

docker start $DOCKER_WEB_NAME;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_WEB_NAME

#-----------------------------------------------------------------------------#
### Copying the params file and gobiiconfig_wrapper.sh
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Copying the param file and the config wrapper first..."
echo;

set -x

docker cp $CONFIGURATOR_PARAM_FILE $DOCKER_WEB_NAME:/data/$DOCKER_BUNDLE_NAME/config/$CONFIGURATOR_PARAM_FILE;
docker cp gobiiconfig_wrapper.sh $DOCKER_WEB_NAME:/data/$DOCKER_BUNDLE_NAME/config/gobiiconfig_wrapper.sh;

#-----------------------------------------------------------------------------#
### Set the proper UID and GID and chown everything from within container
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Matching the docker gadm account to that of the host's and changing file ownerships..."
echo;

set -x

DOCKER_CMD="usermod -u $GOBII_UID gadm;";
eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="groupmod -g $GOBII_GID gobii;";
eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -user 1016 -exec chown -h $GOBII_UID {} \; || :";
eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -group 1016 -exec chgrp -h $GOBII_GID {} \; || :";
eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Update the gobii-web.xml file with install.parameters.
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Updating gobii-web.xml..."
echo;

set -x

# !!! This may need to be rebuilt as there is no longer a params file.
# ! added the '.' to source the vars in the local shell
#DOCKER_CMD="cd $DOCKER_BUNDLE_NAME/config; bash gobiiconfig_wrapper.sh $CONFIGURATOR_PARAM_FILE;";
#DOCKER_CMD="cd $DOCKER_BUNDLE_NAME/config; bash . gobiiconfig_wrapper.sh;";
#eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";

cp $WORKSPACE/gobii.parameters.tmp /data/gobii_bundle/config/gobii.parameters.tmp;

DOCKER_CMD="cd $DOCKER_BUNDLE_NAME/config; bash gobiiconfig_wrapper.sh $CONFIGURATOR_PARAM_FILE;";
eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";

DOCKER_CMD="cd $DOCKER_BUNDLE_NAME/config; rm -f $CONFIGURATOR_PARAM_FILE;";
eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Copy confidentiality.txt to crop and update permissions
#-----------------------------------------------------------------------------#
  
DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/config;
cp -n confidentiality.txt /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP1_NAME/notices || true;
chown $GOBII_UID:$GOBII_GID /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP1_NAME/notices/confidentiality.txt;
chmod 777 /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP1_NAME/notices/confidentiality.txt;";
eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";

# if [[ -v DOCKER_CROP2_NAME ]]; then
#         DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/config;
#         cp -n confidentiality.txt /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP2_NAME/notices || true;
#         chown $GOBII_UID:$GOBII_GID /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP2_NAME/notices/confidentiality.txt;
#         chmod 777 /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP2_NAME/notices/confidentiality.txt;";
#         eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
# fi

# if [[ -v DOCKER_CROP3_NAME ]]; then
#         DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/config;
#         cp -n confidentiality.txt /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP3_NAME/notices || true;
#         chown $GOBII_UID:$GOBII_GID /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP3_NAME/notices/confidentiality.txt;
#         chmod 777 /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP3_NAME/notices/confidentiality.txt;";
#         eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
# fi

# if [[ -v DOCKER_CROP4_NAME ]]; then
#         DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/config;
#         cp -n confidentiality.txt /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP4_NAME/notices || true;
#         chown $GOBII_UID:$GOBII_GID /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP4_NAME/notices/confidentiality.txt;
#         chmod 777 /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP4_NAME/notices/confidentiality.txt;";
#         eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
# fi

# if [[ -v DOCKER_CROP5_NAME ]]; then
#         DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/config;
#         cp -n confidentiality.txt /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP5_NAME/notices || true;
#         chown $GOBII_UID:$GOBII_GID /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP5_NAME/notices/confidentiality.txt;
#         chmod 777 /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP5_NAME/notices/confidentiality.txt;";
#         eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
# fi

# if [[ -v DOCKER_CROP6_NAME ]]; then
#         DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/config;
#         cp -n confidentiality.txt /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP6_NAME/notices || true;
#         chown $GOBII_UID:$GOBII_GID /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP6_NAME/notices/confidentiality.txt;
#         chmod 777 /data/$DOCKER_BUNDLE_NAME/crops/$DOCKER_CROP6_NAME/notices/confidentiality.txt";
#         eval docker exec -u gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
# fi

#-----------------------------------------------------------------------------#
### Run liquibase to update schema of any database changes
#-----------------------------------------------------------------------------#
### UPDATE LIQUIBASE: This needs to be done here because the database docker doesn't have java by default.##
#-----------------------------------------------------------------------------#

# Crop1
DOCKER_CMD="cd /data/liquibase;
java -jar bin/liquibase.jar --username=${DB_USERNAME} --password=${DB_PASS} --url=jdbc:postgresql://${DOCKER_DB_HOST}:${DB_PORT}/${DB_NAME_CROP1} --driver=org.postgresql.Driver --classpath=drivers/postgresql-9.4.1209.jar --changeLogFile=changelogs/db.changelog-master.xml --contexts=${LIQUIBASE_CROP1_CONTEXTS} update;";
eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";

# Crop2 -- no error if crop2 doesn't exist
# if [[ -v DB_NAME_CROP2 ]]; then
# 	DOCKER_CMD="cd /data/liquibase;
# 	java -jar bin/liquibase.jar --username=${DB_USERNAME} --password=${DB_PASS} --url=jdbc:postgresql://${DOCKER_DB_HOST}:${DB_PORT}/${DB_NAME_CROP2} --driver=org.postgresql.Driver --classpath=drivers/postgresql-9.4.1209.jar --changeLogFile=changelogs/db.changelog-master.xml --contexts=${LIQUIBASE_CROP2_CONTEXTS} update;";
# 	eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
# fi

# # Crop3 -- no error if crop3 doesn't exist
# if [[ -v DB_NAME_CROP3 ]]; then
# 	DOCKER_CMD="cd /data/liquibase;
# 	java -jar bin/liquibase.jar --username=${DB_USERNAME} --password=${DB_PASS} --url=jdbc:postgresql://${DOCKER_DB_HOST}:${DB_PORT}/${DB_NAME_CROP3} --driver=org.postgresql.Driver --classpath=drivers/postgresql-9.4.1209.jar --changeLogFile=changelogs/db.changelog-master.xml --contexts=${LIQUIBASE_CROP3_CONTEXTS} update;";
# 	eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
# fi

# # Crop4 -- no error if crop4 doesn't exist
# if [[ -v DB_NAME_CROP4 ]]; then
# 	DOCKER_CMD="cd /data/liquibase;
# 	java -jar bin/liquibase.jar --username=${DB_USERNAME} --password=${DB_PASS} --url=jdbc:postgresql://${DOCKER_DB_HOST}:${DB_PORT}/${DB_NAME_CROP4} --driver=org.postgresql.Driver --classpath=drivers/postgresql-9.4.1209.jar --changeLogFile=changelogs/db.changelog-master.xml --contexts=${LIQUIBASE_CROP4_CONTEXTS} update;";
# 	eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
# fi

# # Crop5 -- no error if crop5 doesn't exist
# if [[ -v DB_NAME_CROP5 ]]; then
# 	DOCKER_CMD="cd /data/liquibase;
# 	java -jar bin/liquibase.jar --username=${DB_USERNAME} --password=${DB_PASS} --url=jdbc:postgresql://${DOCKER_DB_HOST}:${DB_PORT}/${DB_NAME_CROP5} --driver=org.postgresql.Driver --classpath=drivers/postgresql-9.4.1209.jar --changeLogFile=changelogs/db.changelog-master.xml --contexts=${LIQUIBASE_CROP5_CONTEXTS} update;";
# 	eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
# fi

# # Crop6 -- no error if crop6 doesn't exist
# if [[ -v DB_NAME_CROP6 ]]; then
# 	DOCKER_CMD="cd /data/liquibase;
# 	java -jar bin/liquibase.jar --username=${DB_USERNAME} --password=${DB_PASS} --url=jdbc:postgresql://${DOCKER_DB_HOST}:${DB_PORT}/${DB_NAME_CROP6} --driver=org.postgresql.Driver --classpath=drivers/postgresql-9.4.1209.jar --changeLogFile=changelogs/db.changelog-master.xml --contexts=${LIQUIBASE_CROP6_CONTEXTS} update;";
# 	eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";
# fi

#-----------------------------------------------------------------------------#
### Modify the portal configuration
#-----------------------------------------------------------------------------#

# copy icons from repo to /data then to /usr/local/tomcat/webapps/gobii-portal/config/img in web-node

# ! below was commented as this should be updated with newest icons from local repo
# cp -r icons/ /data

# docker exec $DOCKER_WEB_NAME bash -c "
# cd /usr/local/tomcat/webapps/gobii-portal/config/img/ && cp /data/icons/* .;
# ";

# docker exec -u gadm $DOCKER_WEB_NAME bash -c "
# cd /data/gobii_bundle/config/utils; \
# python3.6 xml_config_parser.py \
# -f $PORTAL_CONFIG_PATH \
# -x \"$PORTAL_CROP1_URL_XPATH\" \
# -r \"$PORTAL_CROP1_URL\" \
# -v;
# ";

# docker exec -u gadm $DOCKER_WEB_NAME bash -c "
# cd /data/gobii_bundle/config/utils; \
# python3.6 xml_config_parser.py \
# -f $PORTAL_CONFIG_PATH \
# -x \"$PORTAL_CROP1_NAME_XPATH\" \
# -r \"$PORTAL_CROP1_NAME\" \
# -v;
# ";

# docker exec $DOCKER_WEB_NAME bash -c '
# GDM_LOGO_OLD="<logo>gdm-11-b.png</logo>"
# GDM_LOGO_NEW="<logo>extract.png</logo>"
# sed -i "s~$GDM_LOGO_OLD~$GDM_LOGO_NEW~g" /usr/local/tomcat/webapps/gobii-portal/config/launchers.xml
# ';

# docker exec -u gadm $DOCKER_WEB_NAME bash -c "
# cd /data/gobii_bundle/config/utils; \
# python3.6 xml_config_parser.py \
# -f $PORTAL_CONFIG_PATH \
# -x \"$PORTAL_TIMESCOPE_URL_XPATH\" \
# -r \"$PORTAL_TIMESCOPE_URL\" \
# -v;
# ";

# if [[ -v PORTAL_OWNCLOUD_URL_XPATH ]]; then
# docker exec -u gadm $DOCKER_WEB_NAME bash -c "
# cd /data/gobii_bundle/config/utils; \
# python3.6 xml_config_parser.py \
# -f $PORTAL_CONFIG_PATH \
# -x \"$PORTAL_OWNCLOUD_URL_XPATH\" \
# -r \"$PORTAL_OWNCLOUD_URL\" \
# -v;
# ";  
# fi 

# docker exec -u gadm $DOCKER_WEB_NAME bash -c "
# cd /data/gobii_bundle/config/utils; \
# python3.6 xml_config_parser.py \
# -f $PORTAL_CONFIG_PATH \
# -x \"$PORTAL_KDC_XPATH\" \
# -r \"$PORTAL_KDC_URL\" \
# -v;
# ";

# docker exec -u gadm $DOCKER_WEB_NAME bash -c "
# cd /data/gobii_bundle/config/utils; \
# python3.6 xml_config_parser.py \
# -f $PORTAL_CONFIG_PATH \
# -x \"$PORTAL_LOADER_URL_XPATH\" \
# -r \"$PORTAL_LOADER_URL\" \
# -v;
# ";

# docker exec -u gadm $DOCKER_WEB_NAME bash -c "
# cd /data/gobii_bundle/config/utils; \
# python3.6 xml_config_parser.py \
# -f $PORTAL_CONFIG_PATH \
# -x \"$PORTAL_HAPLOTOOL_URL_XPATH\" \
# -r \"$PORTAL_HAPLOTOOL_URL\" \
# -v;
# ";

# docker exec -u gadm $DOCKER_WEB_NAME bash -c "
# cd /data/gobii_bundle/config/utils; \
# python3.6 xml_config_parser.py \
# -f $PORTAL_CONFIG_PATH \
# -x \"$PORTAL_PEDVER_URL_XPATH\" \
# -r \"$PORTAL_PEDVER_URL\" \
# -v;
# ";

# docker exec -u gadm $DOCKER_WEB_NAME bash -c "
# cd /data/gobii_bundle/config/utils; \
# python3.6 xml_config_parser.py \
# -f $PORTAL_CONFIG_PATH \
# -x \"$PORTAL_FLAPJACK_BYTZ_URL_XPATH\" \
# -r \"$PORTAL_FLAPJACK_BYTZ_URL\" \
# -v;
# ";

# docker exec -u gadm $DOCKER_WEB_NAME bash -c "
# cd /data/gobii_bundle/config/utils; \
# python3.6 xml_config_parser.py \
# -f $PORTAL_CONFIG_PATH \
# -x \"$PORTAL_PORTAINER_URL_XPATH\" \
# -r \"$PORTAL_PORTAINER_URL\" \
# -v;
# ";

# ! Todo; Saved for reference on how to add a block with the python script
# docker exec -u gadm $DOCKER_WEB_NAME bash -c "
# cd /data/gobii_bundle/config/utils; python3.6 xml_config_parser.py -f /usr/local/tomcat/webapps/gobii-portal/config/launchers.xml -a \"<launcher> <name>Flapjack Bytes</name> <url>$PORTAL_FLAPJACK_BYTZ_URL</url> <logo>$PORTAL_FLAPJACK_BYTZ_LOGO</logo> <description>Flapjack Bytes</description> <color>color-green</color> <category>Container Management</category> <type>Web Apps</type> <documentationList> <documentation> <displayName>Flapjack Bytes</displayName> <url>https://gobiiproject.atlassian.net/wiki/spaces/GD/pages/238911510/Flapjack+Bytes</url> </documentation> </documentationList> </launcher>\" -x . ;
# ";

#-----------------------------------------------------------------------------#
### Updating Timescoper Configuration
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Updating Timescoper Configuration..."
echo;

### Requesting passwords [Timescoper User]

echo;

if [ $TIMESCOPER_PASS = "askme" ]; then
    read -sp "Please enter the timescoper user password: " TIMESCOPER_PASS
fi
echo;

set -x

touch $BUNDLE_PARENT_PATH/config.properties;
echo "# Timescope db credentials" > $BUNDLE_PARENT_PATH/config.properties;
echo "db.username=timescoper" >> $BUNDLE_PARENT_PATH/config.properties;
echo "db.pw=$TIMESCOPER_PASS" >> $BUNDLE_PARENT_PATH/config.properties
echo "version=$TIMESCOPER_VERSION" >> $BUNDLE_PARENT_PATH/config.properties;

docker exec -u gadm $DOCKER_WEB_NAME bash -c "
cd /usr/local/tomcat/webapps/timescope/WEB-INF/classes;
mv /data/config.properties .
";
echo;

set -x

#-----------------------------------------------------------------------------#
### Create version file

DOCKER_CMD="touch /data/gobii_bundle/config/gobii.version || true; ";
eval docker exec --user gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";

DOCKER_CMD="echo $GOBII_DEPLOY_VERSION > /data/gobii_bundle/config/gobii.version || true; ";
eval docker exec --user gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";

DOCKER_CMD="chmod 777 /data/gobii_bundle/config/gobii.version || true; ";
eval docker exec --user gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Set fully qualified path for hdf5ExePath parameter in gobii-web.xml
# ! This is set in the gobiiconfig_wrapper.sh
#-----------------------------------------------------------------------------#

# DOCKER_CMD="sed -i 's#<hdf5ExePath>extractors/hdf5/bin</hdf5ExePath>#<hdf5ExePath>/data/gobii_bundle/extractors/hdf5/bin</hdf5ExePath>#' /data/gobii_bundle/config/gobii-web.xml"
# eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Restart tomcat with the proper ownership
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Restarting tomcat under user gadm..."
echo;

set -x

# ! Used more condensed command, below can be removed if it works
# DOCKER_CMD="cd /usr/local/tomcat/bin/; sh shutdown.sh;";
# eval docker exec $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";

# sleep 5

# DOCKER_CMD="cd /usr/local/tomcat/bin/; sh startup.sh;";
# eval docker exec --user gadm $DOCKER_WEB_NAME bash -c \"${DOCKER_CMD}\";

docker exec -i -u gadm gobii-web-node bash -c "
/usr/local/tomcat/bin/shutdown.sh;
sleep 3;
/usr/local/tomcat/bin/startup.sh;
sleep 3
exit;
";

#-----------------------------------------------------------------------------#
### web-node deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;
