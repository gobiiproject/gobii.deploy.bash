#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "No Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "No Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Starting ldap-node deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the ldap-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove web containers
#-----------------------------------------------------------------------------#

docker rm -f $DOCKER_LDAP_NAME || true;

#-----------------------------------------------------------------------------#
### Pull the ldap-node image from dockerhub
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x 

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_LDAP_NAME:$LDAP_IMAGE_TAG;

#-----------------------------------------------------------------------------#
### Run the ldap-node image
#-----------------------------------------------------------------------------#

docker run -dti \
--name $DOCKER_LDAP_NAME \
-h $DOCKER_LDAP_CONTAINER_HOSTNAME \
-v ${NDD_PATH}:${NDD_PATH} \
-v $BUNDLE_PARENT_PATH:/data \
-p $DOCKER_LDAP_PORT:389 \
-p $DOCKER_LDAP_SSL_PORT:636 \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_LDAP_NAME:$LDAP_IMAGE_TAG;

#-----------------------------------------------------------------------------#
### Start the web-node image from dockerhub
#-----------------------------------------------------------------------------#

docker start $DOCKER_LDAP_NAME;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_LDAP_NAME

#-----------------------------------------------------------------------------#
### Container Update & Install vim
#-----------------------------------------------------------------------------#

docker exec $DOCKER_LDAP_NAME bash -c "
apt update;
apt install -y vim;
";

sleep 5


#-----------------------------------------------------------------------------#
### Start LDAP Service
#-----------------------------------------------------------------------------#


docker exec $DOCKER_LDAP_NAME sh -c "service slapd start"
sleep 3
echo;

docker exec $DOCKER_LDAP_NAME sh -c "service slapd status"
sleep 3
echo;



#-----------------------------------------------------------------------------#
### ldap-node deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;

echo "ldap-node deployment script completion!"
echo;
