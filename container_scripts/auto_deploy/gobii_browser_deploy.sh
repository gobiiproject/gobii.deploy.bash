#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "No Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "No Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Starting sherpa deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the gobii-browser-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove sherpa container
#-----------------------------------------------------------------------------#

docker rm -f $DOCKER_BROWSER_NAME || true;

#-----------------------------------------------------------------------------#
### Pull and start the sherpa image
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull filebrowser/filebrowser:latest;

#-----------------------------------------------------------------------------#
### Run the portainer-sherpa-node image
#-----------------------------------------------------------------------------#

docker run -dti \
--name $DOCKER_BROWSER_NAME \
-h $DOCKER_BROWSER_CONTAINER_NAME \
-v ${NDD_PATH}:${NDD_PATH} \
-v $BUNDLE_PARENT_PATH:$BUNDLE_PARENT_PATH \
-v $BROWSER_HOME_MNT:/srv \
-p $BROWSER_PORT:80 \
--network=$DOCKER_NETWORK_NAME \
--restart=always \
filebrowser/filebrowser:latest

#-----------------------------------------------------------------------------#
### Start the portainer-sherpa-node image from dockerhub
#-----------------------------------------------------------------------------#

docker start $DOCKER_BROWSER_NAME

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_BROWSER_NAME
