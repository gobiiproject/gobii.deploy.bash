#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
# Source Parameters

if [[ -e "$1" ]]; then source $1; else echo "No Sourcing \$1"; fi
if [[ -e "$2" ]]; then source $2; else echo "No Sourcing \$2"; fi

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
###############################################################################
### !!! NOTICE: THIS IS DESTRUCTIVE                                         ###
###############################################################################
#-----------------------------------------------------------------------------#
### Stop and remove DB docker container
###>>> DISABLED IN PRODUCTION SYSTEMS - ONLY ENABLE IF DOING A FRESH INSTALL
#-----------------------------------------------------------------------------#
###>>> !!! WARNING: THIS WILL REPLACE YOUR DATABASE DOCKER NODE !!!
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Deploying the DB node..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove DB docker container
#-----------------------------------------------------------------------------#

docker rm -f $DOCKER_DB_NAME || true;

#-----------------------------------------------------------------------------#
### Pull and start the DB docker image 
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_DB_NAME:$GOBII_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Running the db-node 
#-----------------------------------------------------------------------------#

docker run -i --detach --name $DOCKER_DB_NAME \
-h $DOCKER_DB_CONTAINER_HOSTNAME \
-e "gobiiuid=$GOBII_UID" \
-e "gobiigid=$GOBII_GID" \
-e "gobiiuserpassword=${DOCKER_GOBII_ADMIN_PASSWORD}" \
-v ${NDD_PATH}:${NDD_PATH} \
-v ${BUNDLE_PARENT_PATH}:/data \
-v $POSTGRES_ETC:/etc/postgresql \
-v $POSTGRES_LOG:/var/log/postgresql \
-v $POSTGRES_LIB:/var/lib/postgresql \
--restart=always \
-p $DOCKER_DB_PORT:5432 \
--network=$DOCKER_NETWORK_NAME \
--health-cmd="pg_isready -U postgres || exit 1" \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_DB_NAME:$GOBII_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Start db-node
#-----------------------------------------------------------------------------#

docker start $DOCKER_DB_NAME;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_DB_NAME

#-----------------------------------------------------------------------------#
### Install rsync
#-----------------------------------------------------------------------------#

DOCKER_CMD="apt update && apt install rsync -y;";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Set the proper UID and GID and chown everything from within container
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Matching the docker gadm account to that of the host's and changing file ownerships..."
echo;

set -x

DOCKER_CMD="usermod -u $GOBII_UID gadm;";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="groupmod -g $GOBII_GID gobii;";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -user 1016 -exec chown -h $GOBII_UID {} \; || :";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -group 1016 -exec chgrp -h $GOBII_GID {} \; || :";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Clear the target directory of any old gobii_bundle
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Copying the GOBII_BUNDLE to the shared directory/volume..."
echo;

set -x

DOCKER_CMD="rm -rfv /data/$DOCKER_BUNDLE_NAME";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Make sure that the target liquibase directory is clear of old files
#-----------------------------------------------------------------------------#

DOCKER_CMD="rm -rfv /data/liquibase";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Copy the new gobii bundle to the target directory
#-----------------------------------------------------------------------------#

DOCKER_CMD="cd /var; cp -Rv $DOCKER_BUNDLE_NAME /data/$DOCKER_BUNDLE_NAME";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### copy the liquibase directory to the target directory
#-----------------------------------------------------------------------------#

DOCKER_CMD="cd /var; rsync -ahI --delete --progress liquibase/ /data/liquibase || true";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Change the ownership of the folder to the proper gadm account
#-----------------------------------------------------------------------------#

DOCKER_CMD="chown -Rv gadm:gobii /data/$DOCKER_BUNDLE_NAME";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="chown -Rv gadm:gobii /data/liquibase";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Setting gobii group as postgres primary group
#-----------------------------------------------------------------------------#

echo "Setting gobii group as postgres primary group"
echo;

DOCKER_CMD="usermod -aG gobii postgres";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
echo;

DOCKER_CMD="usermod -g gobii postgres";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
echo;

DOCKER_CMD="usermod -aG postgres postgres";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
echo;


#-----------------------------------------------------------------------------#
### db-node deployment script completion
#-----------------------------------------------------------------------------#

echo "Restarting Postgres DB..."
echo;

sleep 3

DOCKER_CMD="service postgresql restart";
eval docker exec -u postgres $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
echo;

sleep 3

#-----------------------------------------------------------------------------#
### db-node deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;
