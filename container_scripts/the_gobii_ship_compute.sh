#!/usr/bin/env bash

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
set -x # xtrace: Similar to -v, but expands commands [to unset and hide docker exec -u gadm $DOCKER_WEB_NAME bash -c '
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#
### !!! WARNING !!!
#-----------------------------------------------------------------------------#
### Do not run this script as SUDO! There are explicit sudo commands which
### will prompt you for password. But not everything should run as sudo.

### usage: bash <bash_script_name>.sh <path of gobii.parameters> <dockerhubpassw | "askme"> <gobii_release_version>

#-----------------------------------------------------------------------------#
### Requirements: 

###>>> 1. The user that will run this script needs to be a sudoer and under the gobii and docker groups. So preferably the user 'gadm'.

###>>> 2. The working directory needs to be where the gobiiconfig_wrapper.sh 
# is as well, typically <gobii_bundle>/conf/
###--->>> NOTE: The order of execution is important.
###--->>> NOTE: If weird things start happening on your containers, try 
# removing the images as well by running 'docker rmi' on each of the 3 nodes.
#-----------------------------------------------------------------------------#
### If you want a delete-all-images command, run this: 
###>>> [sudo docker stop $(sudo docker ps -qa) || true && sudo docker rm 
# $(sudo docker ps -aq) || true && sudo docker rmi $(sudo docker images -aq) 
# || true]
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
#@author: (palace) kdp44@cornell.edu
###>>> Co-Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#


#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

if [ $# -lt 3 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash <bash_script_name>.sh <path of gobii.parameters> <dockerhubpassw | \"askme\"> <gobii_release_version>"
    echo "Set dockerhubpassw parameter to 'askme' for the script to prompt for password instead."
    exit 1
fi

#-----------------------------------------------------------------------------#
### load parameters -- gobii.parameters for deployment
#-----------------------------------------------------------------------------#

set +x

source $1

#-----------------------------------------------------------------------------#
### Set release version and Dockerhub password
###>>> if pass set to 'askme' call for user to enter password at cli
#-----------------------------------------------------------------------------#

DOCKER_HUB_PASSWORD=$2

#-----------------------------------------------------------------------------#
### GDM Release Version
#-----------------------------------------------------------------------------#

GOBII_RELEASE_VERSION=$3

#-----------------------------------------------------------------------------#
### Requesting passwords [Dockerhub]
#-----------------------------------------------------------------------------#
###>>> By default the same password will be set for all administration and 
###>>> gadm users. If a different password is needed, uncomment the 'if' ###>>> statements below to interactively enter the different passwords
#-----------------------------------------------------------------------------#

echo;

if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
fi
echo;

set -x

#-----------------------------------------------------------------------------#
### Creating /data symlink if not already created
#-----------------------------------------------------------------------------#

#sudo ln -sfn $BUNDLE_PARENT_PATH /data || true

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Starting compute deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the compute-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove COMPUTE container
#-----------------------------------------------------------------------------#

docker stop $DOCKER_COMPUTE_NAME || true && docker rm $DOCKER_COMPUTE_NAME || true

#-----------------------------------------------------------------------------#
### Pull and start the COMPUTE image
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_COMPUTE_NAME:$GOBII_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Run the compute-node image
#-----------------------------------------------------------------------------#

docker run -i --detach \
--name $DOCKER_COMPUTE_NAME \
-h $DOCKER_COMPUTE_CONTAINER_HOSTNAME \
-v ${NDD_PATH}:${NDD_PATH} \
-v $BUNDLE_PARENT_PATH:/data \
-p $DOCKER_COMPUTE_SSH_PORT:22 \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
--health-cmd="crontab -u gadm -l | grep gobii_bundle || exit 1" \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_COMPUTE_NAME:$GOBII_RELEASE_VERSION;
echo;

#-----------------------------------------------------------------------------#
### Start the compute-node image from dockerhub
#-----------------------------------------------------------------------------#

docker start $DOCKER_COMPUTE_NAME;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_COMPUTE_NAME

#-----------------------------------------------------------------------------#
### Set the proper UID and GID and chown everything from within container
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Matching the docker gadm account to that of the host's and changing file ownerships..."
echo;

set -x

DOCKER_CMD="usermod -u $GOBII_UID gadm;";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="groupmod -g $GOBII_GID gobii;";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -user 1016 -exec chown -h $GOBII_UID {} \; || :";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -group 1016 -exec chgrp -h $GOBII_GID {} \; || :";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Grant permissions and set cronjobs
#-----------------------------------------------------------------------------#

echo "Granting permissions..."
DOCKER_CMD="chmod -R +rx /data/$DOCKER_BUNDLE_NAME/loaders/;";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="chmod -R +rx /data/$DOCKER_BUNDLE_NAME/extractors/;";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="chmod -R g+rwx /data/$DOCKER_BUNDLE_NAME/crops/*/files;";
eval docker exec $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Configuring cronjobs
#-----------------------------------------------------------------------------#
# Sets the cron jobs for all crops, if there is less than 2 crop, no error will 
# be thrown. This allows the script to be useful for CGs with differing number 
# of crops, without any modifications.
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Setting cron jobs..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Clear the current crontab
#-----------------------------------------------------------------------------#

DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; crontab -r || true;";
eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";

DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; sh addCron.sh /data/$DOCKER_BUNDLE_NAME $DOCKER_CROP1_NAME $DOCKER_CRON_INTERVAL $DOCKER_CRON_FILE_AGE;";
eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";

if [[ -v DOCKER_CROP2_NAME ]]; then
	DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; sh addCron.sh /data/$DOCKER_BUNDLE_NAME $DOCKER_CROP2_NAME $DOCKER_CRON_INTERVAL $DOCKER_CRON_FILE_AGE;";
	eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
fi

if [[ -v DOCKER_CROP3_NAME ]]; then
	DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; sh addCron.sh /data/$DOCKER_BUNDLE_NAME $DOCKER_CROP3_NAME $DOCKER_CRON_INTERVAL $DOCKER_CRON_FILE_AGE;";
	eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
fi

if [[ -v DOCKER_CROP4_NAME ]]; then
	DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; sh addCron.sh /data/$DOCKER_BUNDLE_NAME $DOCKER_CROP4_NAME $DOCKER_CRON_INTERVAL $DOCKER_CRON_FILE_AGE;";
	eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
fi

if [[ -v DOCKER_CROP5_NAME ]]; then
	DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; sh addCron.sh /data/$DOCKER_BUNDLE_NAME $DOCKER_CROP5_NAME $DOCKER_CRON_INTERVAL $DOCKER_CRON_FILE_AGE;";
	eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
fi

if [[ -v DOCKER_CROP6_NAME ]]; then
	DOCKER_CMD="cd /data/$DOCKER_BUNDLE_NAME/loaders/etc; sh addCron.sh /data/$DOCKER_BUNDLE_NAME $DOCKER_CROP6_NAME $DOCKER_CRON_INTERVAL $DOCKER_CRON_FILE_AGE;";
	eval docker exec --user gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";
fi

#-----------------------------------------------------------------------------#
### Enable Live Links

if [ $LL_USER1_PASS = "askme" ]; then
    read -sp "Please enter Password for ownCloud user $LL_USER1: " OC_USER1_PASS
fi
echo;

DOCKER_CMD="cd /data/gobii_bundle/config && java -jar gobiiconfig.jar -wfqpn gobii-web.xml -ownc -ocERR logs -soR crops -soH $EXTERNAL_HOST_IP_HOSTNAME -soN $DOCKER_LL_PORT -soU $LL_USER1 -soP $LL_USER1_PASS";
eval docker exec -u gadm $DOCKER_COMPUTE_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Configure Environment name for emails

if [ -z "$ENV_NAME" ]; then
	echo "Environment Name not set and skipping setting."
	echo;
else
	echo "Environment Name set as: $ENV_NAME"
	echo "Configuring Emails to have shown Environment Name."
	echo;
	JAVA_JAR="java -jar"
	ENV_CONF="java -Dorg.gobii.environment.name=$ENV_NAME -jar"
	sed -ie "s/$JAVA_JAR/$ENV_CONF/g" /data/gobii_bundle/loaders/cronjob.sh
	sed -ie "s/$JAVA_JAR/$ENV_CONF/g" /data/gobii_bundle/extractors/cronjob.sh
	echo "Configuration change: "
	echo;

	echo "/data/gobii_bundle/loaders/cronjob.sh"
	cat /data/gobii_bundle/loaders/cronjob.sh |grep '\-jar'
	echo;
	
	echo "/data/gobii_bundle/extractors/cronjob.sh"
	cat /data/gobii_bundle/extractors/cronjob.sh |grep '\-jar'
	echo;
fi

#-----------------------------------------------------------------------------#
### compute-node deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;

echo "compute-node deployment script completion!"
echo;
