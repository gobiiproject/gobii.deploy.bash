#!/usr/bin/env bash

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces an exit
set -x # xtrace: Similar to -v, but expands commands [to unset and hide passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#
### !!! WARNING !!!
#-----------------------------------------------------------------------------#
### Do not run this script as SUDO! There are explicit sudo commands which
### will prompt you for password. But not everything should run as sudo.

### usage: bash <bash_script_name>.sh <path of main.parameters> <path of install.parameters> <dockerhubpassw | "askme"> <gobii_release_version>

### This a stand-alone equivalent of my THE_GOBII_SHIP Bamboo plan for the 
### ownCloud NODE

#-----------------------------------------------------------------------------#
### Requirements: 

###>>> 1. The user that will run this script needs to be a sudoer and under the gobii and docker groups. So preferably the user 'gadm'.

###>>> 2. The working directory needs to be where the gobiiconfig_wrapper.sh is as well, typically <gobii_bundle>/conf/
###--->>> NOTE: The order of execution is important.
###--->>> NOTE: If weird things start happening on your containers, try removing the images as well by running 'docker rmi' on each of the 3 nodes.
#-----------------------------------------------------------------------------#
### If you want a delete-all-images command, run this: 
###>>> [sudo docker stop $(sudo docker ps -qa) || true && sudo docker rm $(sudo docker ps -aq) || true && sudo docker rmi $(sudo docker images -aq) || true]
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
#@author: (palace) kdp44@cornell.edu
###>>> Co-Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#


#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

if [ $# -lt 3 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash <bash_script_name>.sh <path of gobii.parameters> <dockerhubpassw | \"askme\"> <gobii_release_version>"
    echo "Set dockerhubpassw parameter to 'askme' for the script to prompt for password instead."
    exit 1
fi

#-----------------------------------------------------------------------------#
### load parameters -- main.parameters for deployment
#-----------------------------------------------------------------------------#

set +x

source $1

#-----------------------------------------------------------------------------#
### Set release version and Dockerhub password
###>>> if pass set to 'askme' call for user to enter password at cli
#-----------------------------------------------------------------------------#

DOCKER_HUB_PASSWORD=$2


GOBII_RELEASE_VERSION=$3

#-----------------------------------------------------------------------------#
### Requesting passwords
#-----------------------------------------------------------------------------#
###>>> By default the same password will be set for all administration and 
###>>> gadm users. If a different password is needed, uncomment the 'if' ###>>> statements below to interactively enter the different passwords
#-----------------------------------------------------------------------------#

# TO DO: Update all templates to allow for password requesting
#read -p "Would you like to set the same password for all administration accounts? [y/n]: " administrion_pass_confirm

#if [[ $administrion_pass_confirm == "y" ]]; then

#	echo;
#	read -sp "Please enter default administration password: " administrion_pass
#	DOCKER_HUB_PASSWORD=$administrion_pass
#	DOCKER_GOBII_ADMIN_PASSWORD=$administrion_pass
#	OC_LDAP_CN_PASSWORD=$administrion_pass

#else

if [[ $DOCKER_HUB_PASSWORD = "askme" ]]; then
		read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
fi
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove ownCloud docker containers
#-----------------------------------------------------------------------------#

docker stop $DOCKER_OC_NAME || true 
docker rm $DOCKER_OC_NAME || true

#-----------------------------------------------------------------------------#
### Find and remove current OC images [Not Used on first deploy]
### After first deploy uncomment to remove old images
#-----------------------------------------------------------------------------#

# Will set to continue on fail to find any images to remove
#set +e

#oc_images=(`docker image ls | grep "gadm01/gobii_oc*" | awk '{print $3}'`)

#if [ -z "$oc_images" ]; then echo "no images"; else docker rmi $oc_images; fi

# reset fail fast
#set -e

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Pulling and starting the ownCloud docker image
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_OC_NAME:$DOCKER_OC_VERSION;

#-----------------------------------------------------------------------------#
### Run the oc-node image
#-----------------------------------------------------------------------------#

docker run -i --detach --name $DOCKER_OC_NAME \
-h $DOCKER_OC_CONTAINER_HOSTNAME \
-v ${NDD_PATH}:${NDD_PATH} \
-v $BUNDLE_PARENT_PATH:/data \
-p $DOCKER_OC_PORT:8080 \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_OC_NAME:$DOCKER_OC_VERSION;

# Starting oc Container
docker start $DOCKER_OC_NAME;

echo "Sleeping to give system time to start..."

sleep 30

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_OC_NAME

#-----------------------------------------------------------------------------#
### Configuring ownCloud LDAP
#-----------------------------------------------------------------------------#
### Due to the way bash handles variables from source these commands need to 
### be dumped into a script then ran separately.
### Since passwords are involved, the script will prompt for the password.
#-----------------------------------------------------------------------------#

set +x
echo;

if [[ $OC_LDAP_CN_PASSWORD = "askme" ]]; then
	echo "
	#-----------------------------------------------------------------------------#
### This user is used to the Authenication against AD or LDAP to pull users 
### into ownCloud. [By default in OpenLDAP Administrator user is cn=admin]
#-----------------------------------------------------------------------------#
	"
    echo;

    read -sp "Please enter Authentication user password: " OC_LDAP_CN_PASSWORD
fi
echo;


#-----------------------------------------------------------------------------#
### Adding LDAP Configuration to script
#-----------------------------------------------------------------------------#

docker exec $DOCKER_OC_NAME bash -c "
occ app:enable user_ldap
occ ldap:delete-config s01
occ ldap:create-empty-config
occ ldap:set-config s01 ldapHost "$OC_LDAP_SERVER"
occ ldap:set-config s01 ldapPort "$OC_LDAP_PORT"
occ ldap:set-config s01 ldapAgentName "$OC_LDAP_UID","$OC_LDAP_OU","$OC_LDAP_DN"
occ ldap:set-config s01 ldapAgentPassword "$OC_LDAP_CN_PASSWORD"
occ ldap:set-config s01 ldapBase "$OC_LDAP_DN"
occ ldap:set-config s01 ldapBaseGroups "$OC_LDAP_DN"
occ ldap:set-config s01 ldapBaseUsers "$OC_LDAP_DN"
occ ldap:set-config s01 ldapConfigurationActive "1"
occ ldap:set-config s01 ldapEmailAttribute "mail"
occ ldap:set-config s01 ldapExpertUUIDGroupAttr "uid"
occ ldap:set-config s01 ldapExpertUUIDUserAttr "uid"
occ ldap:set-config s01 ldapExpertUsernameAttr "uid"
occ ldap:set-config s01 ldapGroupDisplayName "cn"
occ ldap:set-config s01 ldapUserDisplayName "cn"
occ ldap:set-config s01 ldapUserFilterObjectclass "inetOrgPerson"
occ ldap:set-config s01 ldapLoginFilter "$OC_LDAP_LOGIN_FILTER"
occ ldap:set-config s01 ldapUserFilter "$OC_LDAP_USER_FILTER"
occ ldap:show-config
" || true

set -x

#-----------------------------------------------------------------------------#
### Testing ownCloud LDAP Configuration
#-----------------------------------------------------------------------------#

docker exec $DOCKER_OC_NAME bash -c "
occ ldap:test-config s01
" || true

#-----------------------------------------------------------------------------#
### Syncing LDAP ownCloud User List
#-----------------------------------------------------------------------------#

docker exec gobii-oc-node bash -c '
occ user:sync -vvv -n "OCA\User_LDAP\User_Proxy"
' || true

#-----------------------------------------------------------------------------#
### echoing LDAP ownCloud User List
#-----------------------------------------------------------------------------#

docker exec $DOCKER_OC_NAME bash -c "
occ user:list
" || true

#-----------------------------------------------------------------------------#
### Reports local and LDAP users
#-----------------------------------------------------------------------------#

docker exec $DOCKER_OC_NAME bash -c "
occ user:report
" || true

#-----------------------------------------------------------------------------#
### Syncing (Non-Disabled) User List from LDAP
#-----------------------------------------------------------------------------#

docker exec $DOCKER_OC_NAME bash -c "
occ user:sync "OCA\User_LDAP\User_Proxy" -m disable -r
" || true

#-----------------------------------------------------------------------------#
### Reports all users local and LDAP after syncing users from LDAP
#-----------------------------------------------------------------------------#

docker exec $DOCKER_OC_NAME bash -c "
occ user:report
" || true

#-----------------------------------------------------------------------------#
### Showing LDAP ownCloud User List
#-----------------------------------------------------------------------------#

docker exec $DOCKER_OC_NAME bash -c "
occ user:list
" || true

#-----------------------------------------------------------------------------#
### Enabling ownCloud external Storage
#-----------------------------------------------------------------------------#

docker exec $DOCKER_OC_NAME bash -c "
occ app:enable files_external
occ config:app:set core enable_external_storage --value=yes
" || true

#-----------------------------------------------------------------------------#
### Enable local storage mount
#-----------------------------------------------------------------------------#

docker exec $DOCKER_OC_NAME bash -c "
cd /var/www/owncloud/config;
mv config.php config.php.original;
sed '50 i 'files_external_allow_create_new_local' => 'true',' config.php.original > config.php;
chown www-data:root config.php;
" || true;

#-----------------------------------------------------------------------------#
### ownCloud updating mimetypemapping.json for all editable files
#-----------------------------------------------------------------------------#
# "log": ["text/plain"],
# "marker": ["text/plain"],
# "file": ["text/plain"],
# "genotype": ["text/plain"],
# "dataset_marker": ["text/plain"],
# "dataset_dnarun": ["text/plain"],
# "dnarun": ["text/plain"],
# "dnasample": ["text/plain"],
# "germplasm": ["text/plain"],
# "marker": ["text/plain"],
# "marker_linkage_group": ["text/plain"],
# "linkage_group": ["text/plain"]
#-----------------------------------------------------------------------------#

docker exec gobii-oc-node bash -c "
cp mimetypemapping.json /var/www/owncloud/config/
chown www-data:www-data /var/www/owncloud/config/mimetypemapping.json
occ file:scan --all
";

#-----------------------------------------------------------------------------#
### ownCloud deployment script process completion
#-----------------------------------------------------------------------------#

echo;
echo "
#-----------------------------------------------------------------------------#
### ownCloud Deployment Completed
#-----------------------------------------------------------------------------#
"
echo;

