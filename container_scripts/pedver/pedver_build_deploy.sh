#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Set Environment for shell

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
set -x # xtrace: Similar to -v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

#-----------------------------------------------------------------------------#
### Proper args passed

if [ $# -lt 1 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash pedver_build_deploy.sh <Docker_Image_Tag> <gobiimarkerapp_branch>"
    exit 1
fi

#-----------------------------------------------------------------------------#
### Set Passed variables

# Build tag
BUILD_TAG=$1

# gobiimarkerapp repo branch
BRANCH=$2

#-----------------------------------------------------------------------------#
### Deploy pedver

# Moving to gobii.markerapp directory
cd ~/workspace/gobii.markerapp
echo;

# Pulling new branch changes
git pull
echo;

# Display current branch
git checkout $BRANCH 

# Pulling new branch changes
git pull
echo;

read -p "Please press any key to continue... " -n1 -s
echo;

# removing current docker containers
docker rm -f gobiimarkerapp-worker gobiimarkerapp redis || true;
echo;

# Deleting images
docker image rm $(docker images) || true;
echo;

# Setting env params
HOST_DATA_VOLUME=/data/pedver/
echo;

# Create directories for pedver data

mkdir -p ${HOST_DATA_VOLUME} || true;

mkdir -p ${HOST_DATA_VOLUME}/data || true;

mkdir -p ${HOST_DATA_VOLUME}/instance || true;

# Export vars

export CONTAINER_DATA_FOLDER=/data/
echo;

export CONTAINER_INSTANCE_FOLDER=/gobiimarkerapp/instance/
echo;

#-----------------------------------------------------------------------------#
### Docker Login

set +x # Turning down verbosity as the password is echoed to stdout
echo;

#read -p "Please enter your Docker Hub Login Username: " DOCKER_HUB_LOGIN_USERNAME
#echo;

DOCKER_HUB_LOGIN_USERNAME="gadmreader"

read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
echo;


docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
echo;
set -x

#-----------------------------------------------------------------------------#
### Start Container builds & deployments

# Pull redis from dockerhub
docker pull redis
echo;

# Deploy redis
docker run -dti \
-p 6379:6379 \
--name redis \
--restart=always \
redis
echo;

# Copy config.ini for container access

sudo cp config.ini ${HOST_DATA_VOLUME}instance/
echo;

# Setting redis IP from docker network
REDIS_IP=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' redis)
echo;

# ! Running Python Script (What is this doing?)
python3 setup_config.py -i ${HOST_DATA_VOLUME}/instance/config.ini -r ${REDIS_IP}
echo;

# Build app node
docker build -f gobiimarkerapp/Dockerfile -t gobiimarkerapp:0.5 .
echo;

# Build worker node
docker build -f celerytasks/Dockerfile -t gobiimarkerapp-worker:0.5 .
echo;

# Deploy app node
docker run -dti \
-p 80:80 \
-v ${HOST_DATA_VOLUME}data:${CONTAINER_DATA_FOLDER} \
-v ${HOST_DATA_VOLUME}instance:${CONTAINER_INSTANCE_FOLDER} \
--hostname gobiimarkerapp-web-node \
--restart=always \
--name gobiimarkerapp \
gobiimarkerapp:0.5
echo;

# Deploy worker node
docker run -dti  \
-v ${HOST_DATA_VOLUME}data:${CONTAINER_DATA_FOLDER} \
-v ${HOST_DATA_VOLUME}instance:${CONTAINER_INSTANCE_FOLDER} \
--hostname gobiimarkerapp-worker-node  \
--restart=always \
--name gobiimarkerapp-worker \
gobiimarkerapp-worker:0.5
echo;

# create and push docker images
docker commit -m "Image created for build tag: $BUILD_TAG" redis gadm01/gobii_pedver_redis:$BUILD_TAG

docker commit -m "Image created for build tag: $BUILD_TAG" gobiimarkerapp-worker gadm01/gobii_pedver_worker:$BUILD_TAG

docker commit -m "Image created for build tag: $BUILD_TAG" gobiimarkerapp gadm01/gobii_pedver_web:$BUILD_TAG

# Push new docker images
docker push gadm01/gobii_pedver_redis:$BUILD_TAG

docker push gadm01/gobii_pedver_worker:$BUILD_TAG

docker push gadm01/gobii_pedver_web:$BUILD_TAG

echo "PedVer build Completed"
echo;
