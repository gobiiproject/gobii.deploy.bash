#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### the_gobii_ship_sherpa_agent.sh
### <description goes here>
#-----------------------------------------------------------------------------#
#@author: (palace) kdp44@cornell.edu
###>>> Co-Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#


set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
set -x # xtrace: Similar to -v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#
### !!! WARNING !!!
#-----------------------------------------------------------------------------#
### Do not run this script as SUDO! There are explicit sudo commands which
# will prompt you for password. But not everything should run as sudo.

### usage: bash <bash_script_name>.sh <path of gobii.parameters> <dockerhubpassw | "askme">

#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

#-----------------------------------------------------------------------------#
### load parameters -- gobii.parameters for deployment
#-----------------------------------------------------------------------------#

set +x

source $1

#-----------------------------------------------------------------------------#
### Set release version and Dockerhub password
###>>> if pass set to 'askme' call for user to enter password at cli
#-----------------------------------------------------------------------------#

DOCKER_HUB_PASSWORD=$2

#-----------------------------------------------------------------------------#
### Requesting passwords [Dockerhub]
#-----------------------------------------------------------------------------#
###>>> By default the same password will be set for all administration and 
###>>> gadm users. If a different password is needed, uncomment the 'if' ###>>> statements below to interactively enter the different passwords
#-----------------------------------------------------------------------------#

echo;

if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
fi
echo;

set -x

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Starting sherpa deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the portainer-sherpa-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove sherpa container
#-----------------------------------------------------------------------------#

docker stop $DOCKER_SHERPA_AGENT_NAME || true && docker rm $DOCKER_SHERPA_AGENT_NAME || true

#-----------------------------------------------------------------------------#
### Pull and start the sherpa image
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull djenriquez/sherpa:latest;

#-----------------------------------------------------------------------------#
### Run the portainer-sherpa-node image
#-----------------------------------------------------------------------------#

docker run -d \
--name $DOCKER_SHERPA_AGENT_NAME \
-h $DOCKER_SHERPA_CONTAINER_HOSTNAME \
-e CONFIG="[ \
    { \
        \"Path\" : \"/\", \
        \"Access\": \"allow\", \
        \"Addresses\": [$DOCKER_SHERPA_NETWORK_RULES] \
    } \
]" \
-v /var/run/docker.sock:/tmp/docker.sock \
-p 4550:4550 \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
djenriquez/sherpa:latest --allow

#-----------------------------------------------------------------------------#
### Start the portainer-sherpa-node image from dockerhub
#-----------------------------------------------------------------------------#

docker start $DOCKER_SHERPA_AGENT_NAME

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_SHERPA_AGENT_NAME

#-----------------------------------------------------------------------------#
### portainer-sherpa-node deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;

echo "portainer-sherpa-node deployment script completion!"
echo;
