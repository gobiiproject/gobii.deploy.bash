#!/usr/bin/env bash

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Run undefined variable outputs error message, and forces exit
set -x # xtrace: Similar to -v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#
### !!! WARNING !!!
#-----------------------------------------------------------------------------#
### Do not run this script as SUDO! There are explicit sudo commands which
### will prompt you for password. But not everything should run as sudo.

### usage: bash <bash_script_name>.sh <path of main.parameters> <path of 
# install.parameters> <dockerhubpassw | "askme"> <gobii_release_version>

#-----------------------------------------------------------------------------#
### Requirements: 

###>>> 1. The user that will run this script needs to be a sudoer and under the gobii and docker groups. So preferably the user 'gadm'.

###>>> 2. The working directory needs to be where the gobiiconfig_wrapper.sh 
# is as well, typically <gobii_bundle>/conf/
###--->>> NOTE: The order of execution is important.
###--->>> NOTE: If weird things start happening on your containers, try 
# removing the images as well by running 'docker rmi' on each of the 3 nodes.
#-----------------------------------------------------------------------------#
### If you want a delete-all-images command, run this: 
###>>> [sudo docker stop $(sudo docker ps -qa) || true && sudo docker rm 
# $(sudo docker ps -aq) || true && sudo docker rmi $(sudo docker images -aq) 
# || true]
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
#@author: (palace) kdp44@cornell.edu
###>>> Co-Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#


#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

if [ $# -lt 3 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash <bash_script_name>.sh <path of gobii.parameters> <dockerhubpassw | \"askme\"> <gobii_release_version>"
    echo "Set dockerhubpassw parameter to 'askme' for the script to prompt for password instead."
    exit 1
fi

#-----------------------------------------------------------------------------#
### load parameters -- gobii.parameters for deployment
#-----------------------------------------------------------------------------#

set +x

source $1

#-----------------------------------------------------------------------------#
### Set release version and Dockerhub password
###>>> if pass set to 'askme' call for user to enter password at cli
#-----------------------------------------------------------------------------#

DOCKER_HUB_PASSWORD=$2

#-----------------------------------------------------------------------------#
### GDM Release Version
#-----------------------------------------------------------------------------#

GOBII_RELEASE_VERSION=$3

#-----------------------------------------------------------------------------#
### Requesting passwords [Dockerhub]
#-----------------------------------------------------------------------------#
###>>> By default the same password will be set for all administration and 
###>>> gadm users. If a different password is needed, uncomment the 'if' ###>>> statements below to interactively enter the different passwords
#-----------------------------------------------------------------------------#

echo;

if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
fi
echo;

set -x

#-----------------------------------------------------------------------------#
### Creating /data symlink if not already created
#-----------------------------------------------------------------------------#

#sudo ln -sfn $BUNDLE_PARENT_PATH /data || true
sudo mkdir $BUNDLE_PARENT_PATH || true;
sudo chown gadm:gobii $BUNDLE_PARENT_PATH || true;

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
###############################################################################
### !!! NOTICE: THIS IS DESTRUCTIVE                                         ###
###############################################################################
#-----------------------------------------------------------------------------#
### Stop and remove DB docker container
###>>> DISABLED IN PRODUCTION SYSTEMS - ONLY ENABLE IF DOING A FRESH INSTALL
#-----------------------------------------------------------------------------#
###>>> !!! WARNING: THIS WILL REPLACE YOUR DATABASE DOCKER NODE !!!
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Deploying the DB node..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove DB docker container
#-----------------------------------------------------------------------------#

docker stop $DOCKER_DB_NAME || true && docker rm $DOCKER_DB_NAME || true;

#-----------------------------------------------------------------------------#
### Pull and start the DB docker image 
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull $DOCKER_HUB_USERNAME/$DOCKER_HUB_DB_NAME:$GOBII_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Running the db-node 
#-----------------------------------------------------------------------------#

docker run -i --detach --name $DOCKER_DB_NAME \
-h $DOCKER_DB_CONTAINER_HOSTNAME \
-e "gobiiuid=$GOBII_UID" \
-e "gobiigid=$GOBII_GID" \
-e "gobiiuserpassword=${DOCKER_GOBII_ADMIN_PASSWORD}" \
-v ${NDD_PATH}:${NDD_PATH} \
-v ${BUNDLE_PARENT_PATH}:/data \
-v $POSTGRES_ETC:/etc/postgresql \
-v $POSTGRES_LOG:/var/log/postgresql \
-v $POSTGRES_LIB:/var/lib/postgresql \
--restart=always \
-p $DOCKER_DB_PORT:5432 \
--network=$DOCKER_NETWORK_NAME \
--health-cmd="pg_isready -U postgres || exit 1" \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_DB_NAME:$GOBII_RELEASE_VERSION;

#-----------------------------------------------------------------------------#
### Start db-node
#-----------------------------------------------------------------------------#

docker start $DOCKER_DB_NAME;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_DB_NAME

#-----------------------------------------------------------------------------#
### Install rsync
#-----------------------------------------------------------------------------#

DOCKER_CMD="apt update && apt install rsync -y;";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";


#-----------------------------------------------------------------------------#
### Set the proper UID and GID and chown everything from within container
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Matching the docker gadm account to that of the host's and changing file ownerships..."
echo;

set -x

DOCKER_CMD="usermod -u $GOBII_UID gadm;";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="groupmod -g $GOBII_GID gobii;";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -user 1016 -exec chown -h $GOBII_UID {} \; || :";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="find / -group 1016 -exec chgrp -h $GOBII_GID {} \; || :";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";


#-----------------------------------------------------------------------------#
### Clear the target directory of any old gobii_bundle
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Copying the GOBII_BUNDLE to the shared directory/volume..."
echo;

set -x

DOCKER_CMD="rm -rf /data/$DOCKER_BUNDLE_NAME";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Make sure that the target liquibase directory is clear of old files
#-----------------------------------------------------------------------------#

DOCKER_CMD="rm -rf /data/liquibase";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Copy the new gobii bundle to the target directory
#-----------------------------------------------------------------------------#

DOCKER_CMD="cd /var; cp -R $DOCKER_BUNDLE_NAME /data/$DOCKER_BUNDLE_NAME";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### copy the liquibase directory to the target directory
#-----------------------------------------------------------------------------#

DOCKER_CMD="cd /var; rsync -ahI --delete --progress liquibase/ /data/liquibase || true";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Change the ownership of the folder to the proper gadm account
#-----------------------------------------------------------------------------#

DOCKER_CMD="chown -R gadm:gobii /data/$DOCKER_BUNDLE_NAME";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
DOCKER_CMD="chown -R gadm:gobii /data/liquibase";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";

#-----------------------------------------------------------------------------#
### Setting gobii group as postgres primary group
#-----------------------------------------------------------------------------#

echo "Setting gobii group as postgres primary group"
echo;

DOCKER_CMD="usermod -aG gobii postgres";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
echo;

DOCKER_CMD="usermod -g gobii postgres";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
echo;

DOCKER_CMD="usermod -aG postgres postgres";
eval docker exec $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
echo;


#-----------------------------------------------------------------------------#
### db-node deployment script completion
#-----------------------------------------------------------------------------#

echo "Restarting Postgres DB..."
echo;

sleep 3

DOCKER_CMD="service postgresql restart";
eval docker exec -u postgres $DOCKER_DB_NAME bash -c \"${DOCKER_CMD}\";
echo;

sleep 3

#-----------------------------------------------------------------------------#
### db-node deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;

echo "db-node deployment script completion!"
echo;
