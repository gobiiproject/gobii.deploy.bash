#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### the_gobii_ship_portainer.sh
### <description goes here>
#-----------------------------------------------------------------------------#
#@author: (palace) kdp44@cornell.edu
###>>> Co-Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
set -x # xtrace: Similar to -v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#
### !!! WARNING !!!
#-----------------------------------------------------------------------------#
### Do not run this script as SUDO! There are explicit sudo commands which
### will prompt you for password. But not everything should run as sudo.

### usage: bash <bash_script_name>.sh <path of gobii.parameters> <dockerhubpassw | "askme">

#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

#-----------------------------------------------------------------------------#
### load parameters -- gobii.parameters for deployment
#-----------------------------------------------------------------------------#

set +x

source $1

#-----------------------------------------------------------------------------#
### Set release version and Dockerhub password
###>>> if pass set to 'askme' call for user to enter password at cli
#-----------------------------------------------------------------------------#

DOCKER_HUB_PASSWORD=$2

#-----------------------------------------------------------------------------#
### Requesting passwords [Dockerhub]
#-----------------------------------------------------------------------------#
###>>> By default the same password will be set for all administration and 
###>>> gadm users. If a different password is needed, uncomment the 'if' ###>>> statements below to interactively enter the different passwords
#-----------------------------------------------------------------------------#

echo;

if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
fi
echo;

set -x

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;


#-----------------------------------------------------------------------------#
### Starting compute deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the portainer-node deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove COMPUTE container
#-----------------------------------------------------------------------------#

docker stop $DOCKER_PORTAINER_NAME || true && docker rm $DOCKER_PORTAINER_NAME || true

#-----------------------------------------------------------------------------#
### Pull and start the COMPUTE image
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull portainer/portainer;

#-----------------------------------------------------------------------------#
### Run the PORTAINER image
#-----------------------------------------------------------------------------#

if [[ -v DOCKER_PORTAINER_VERSION ]]; then
docker run -i --detach \
--name $DOCKER_PORTAINER_NAME \
-h $DOCKER_PORTAINER_CONTAINER_HOSTNAME \
-p 9000:9000 \
--restart=always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v ${NDD_PATH}:${NDD_PATH} \
-v $BUNDLE_PARENT_PATH:/data \
--network=$DOCKER_NETWORK_NAME \
portainer/portainer:$DOCKER_PORTAINER_VERSION;
else
docker run -i --detach \
--name $DOCKER_PORTAINER_NAME \
-h $DOCKER_PORTAINER_CONTAINER_HOSTNAME \
-p 9000:9000 \
--restart always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v $BUNDLE_PARENT_PATH:/data \
--network=$DOCKER_NETWORK_NAME \
portainer/portainer;
fi

#-----------------------------------------------------------------------------#
### Start the PORTAINER CONTAINER
#-----------------------------------------------------------------------------#

docker start $DOCKER_PORTAINER_NAME;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_PORTAINER_NAME

#-----------------------------------------------------------------------------#
### portainer-node deployment script completion
#-----------------------------------------------------------------------------#

set +x
echo;

echo "portainer-node deployment script completion!"
echo;
