#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Set Environment for shell

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
set -x # xtrace: Similar to -v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

###>>> Co-Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#


#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

if [ $# -lt 3 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash <bash_script_name>.sh <path of gobii.parameters> <dockerhubpassw | \"askme\"> <gobii_release_version>"
    echo "Set dockerhubpassw parameter to 'askme' for the script to prompt for password instead."
    exit 1
fi

#-----------------------------------------------------------------------------#
### load parameters -- gobii.parameters for deployment
#-----------------------------------------------------------------------------#

set +x

source $1

#-----------------------------------------------------------------------------#
### Set release version and Dockerhub password
###>>> if pass set to 'askme' call for user to enter password at cli
#-----------------------------------------------------------------------------#

DOCKER_HUB_PASSWORD=$2

#-----------------------------------------------------------------------------#
### GDM Release Version
#-----------------------------------------------------------------------------#

PEDVER_RELEASE_TAG=$3

#-----------------------------------------------------------------------------#
### Requesting passwords [Dockerhub]
#-----------------------------------------------------------------------------#
###>>> By default the same password will be set for all administration and 
###>>> gadm users. If a different password is needed, uncomment the 'if' ###>>> statements below to interactively enter the different passwords
#-----------------------------------------------------------------------------#

echo;

if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
fi
echo;

set -x

#-----------------------------------------------------------------------------#
### Creating /data symlink if not already created
#-----------------------------------------------------------------------------#

#sudo ln -sfn $BUNDLE_PARENT_PATH /data || true

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $PEDVER_DOCKER_NET || true;

docker network connect $PEDVER_DOCKER_NET $DOCKER_HAPLO_NAME || true;
docker network connect $PEDVER_DOCKER_NET $DOCKER_PORTAINER_NAME || true;
docker network connect $PEDVER_DOCKER_NET $DOCKER_OC_NAME || true;
docker network connect $PEDVER_DOCKER_NET $DOCKER_KDC_NAME || true;
docker network connect $PEDVER_DOCKER_NET $DOCKER_WEB_NAME || true;
docker network connect $PEDVER_DOCKER_NET $DOCKER_DB_NAME || true;
docker network connect $PEDVER_DOCKER_NET $DOCKER_COMPUTE_NAME || true;
if [[ -v DOCKER_SHERPA_AGENT_NAME ]]; then
docker network connect $PEDVER_DOCKER_NET $DOCKER_SHERPA_AGENT_NAME || true;
fi

#-----------------------------------------------------------------------------#
### Starting compute deployment
#-----------------------------------------------------------------------------#

set +x
echo;

echo "Starting the pedver deployment..."
echo;

set -x

#-----------------------------------------------------------------------------#
### Stop and remove containers
#-----------------------------------------------------------------------------#

# removing current docker containers
docker rm -f gobiimarkerapp-worker gobiimarkerapp redis || true;
echo;

docker rm -f $DOCKER_PEDVER_WEB_NAME $DOCKER_PEDVER_WORKER_NAME $DOCKER_PEDVER_REDIS_NAME || true;

#-----------------------------------------------------------------------------#
### Setting location vars
#-----------------------------------------------------------------------------#

# Setting env params
HOST_DATA_VOLUME=$PEDVER_DATA_VOLUME
echo;

# Create directories for pedver data

mkdir ${HOST_DATA_VOLUME} || true;

mkdir ${HOST_DATA_VOLUME}/data || true;

mkdir ${HOST_DATA_VOLUME}/instance || true;

# Export vars
export CONTAINER_DATA_FOLDER=/data/
echo;

export CONTAINER_INSTANCE_FOLDER=/gobiimarkerapp/instance/
echo;

#-----------------------------------------------------------------------------#
### Docker Login

#-----------------------------------------------------------------------------#
### Pull images
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x

docker pull $DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_REDIS:$PEDVER_RELEASE_TAG;

docker pull $DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_WEB:$PEDVER_RELEASE_TAG;

docker pull $DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_WORKER:$PEDVER_RELEASE_TAG;

#-----------------------------------------------------------------------------#
### Start Container builds & deployments

# Deploy redis
docker run -dti \
--name $DOCKER_PEDVER_REDIS_NAME \
-h $CONTAINER_PEDVER_NAME_REDIS \
-p $PEDVER_REDIS_PORT:6379 \
--restart=always \
--network=$PEDVER_DOCKER_NET \
$DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_REDIS:$PEDVER_RELEASE_TAG
echo;

# Copy config.ini for container access
cp devops_files/pedver/config.ini ${HOST_DATA_VOLUME}/instance/
echo;

# Setting redis IP from docker network
SET_INSPECT="{{ .NetworkSettings.Networks.$PEDVER_DOCKER_NET.IPAddress }}"
REDIS_IP=$(docker inspect --format="$SET_INSPECT" $DOCKER_PEDVER_REDIS_NAME)
echo;

sed -ie "s~CELERY_BROKER_URL = redis://localhost:6379/0~CELERY_BROKER_URL = redis://$REDIS_IP:6379/0~g" ${HOST_DATA_VOLUME}/instance/config.ini

sed -ie "s~CELERY_RESULT_BACKEND = redis://localhost:6379/0~CELERY_RESULT_BACKEND = redis://$REDIS_IP:6379/0~g" ${HOST_DATA_VOLUME}/instance/config.ini

# Deploy app node
docker run -dti \
--name $DOCKER_PEDVER_WEB_NAME \
-h $CONTAINER_PEDVER_NAME_WEB \
-p $PEDVER_WEB_PORT:80 \
-v ${HOST_DATA_VOLUME}/data:${CONTAINER_DATA_FOLDER} \
-v ${HOST_DATA_VOLUME}/instance:${CONTAINER_INSTANCE_FOLDER} \
--restart=always \
--network=$PEDVER_DOCKER_NET \
$DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_WEB:$PEDVER_RELEASE_TAG
echo;

# Deploy worker node
docker run -dti  \
--name $DOCKER_PEDVER_WORKER_NAME \
-h $DOCKER_PEDVER_WORKER_NAME \
-v ${HOST_DATA_VOLUME}/data:${CONTAINER_DATA_FOLDER} \
-v ${HOST_DATA_VOLUME}/instance:${CONTAINER_INSTANCE_FOLDER} \
--restart=always \
--network=$PEDVER_DOCKER_NET \
$DOCKER_HUB_USERNAME/$DOCKERHUB_PEDVER_REPO_WORKER:$PEDVER_RELEASE_TAG
echo;

#-----------------------------------------------------------------------------#
### Connect to bridge Network
#-----------------------------------------------------------------------------#

docker network connect bridge $DOCKER_PEDVER_REDIS_NAME

docker network connect bridge $DOCKER_PEDVER_WEB_NAME

docker network connect bridge $DOCKER_PEDVER_WORKER_NAME

#-----------------------------------------------------------------------------#

set +x 

echo "PedVer Redeploy Completed"
echo;
