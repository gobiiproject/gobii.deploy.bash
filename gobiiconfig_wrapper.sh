#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#

#usage: bash config_wrapper.sh <path-of-gobii_install.params>
#run this from the directory where gobiiconfig.jar is

source $1
echo "Path to bundle: " $BUNDLE_PATH/config
echo "Updating $CONFIG_XML via $1..."
cd $BUNDLE_PATH/config;
#Set root gobii directory (global)
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -gR "$BUNDLE_PATH";
#LDAP authentication options as well as "run as" user for digester/extractor.
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -auT $AUTH_TYPE -ldUDN "$LDAP_DN" -ldURL $LDAP_URL -ldBUSR "$LDAP_BIND_USER" -ldBPAS $LDAP_BIND_PASSWORD -ldraUSR $LDAP_BACKGROUND_USER -ldraPAS $LDAP_BACKGROUND_PASSWORD;
#Configure email server (global)
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -stE -soH $MAIL_HOST -soN $MAIL_PORT -soU $MAIL_USERNAME -soP $MAIL_PASSWORD -stT $MAIL_TYPE -stH $MAIL_HASH;

#Configure web server for crop1
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -c  $CROP1  -stW  -soH $WEB_HOST -soN $WEB_PORT -soR $CROP1_CONTEXT_PATH;
#Configure PostGRES server for crop1
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -c  $CROP1  -stP -soH $DB_HOST -soN $DB_PORT -soU $DB_USERNAME -soP $DB_PASS -soR $DB_NAME_CROP1;

# Configure the hdf5ExePath to be FQPN
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -a -gH5 "/data/gobii_bundle/extractors/hdf5/bin"

# Add versioning for /data/gobii_bundle/config/gobii.version
java -jar gobiiconfig.jar -wfqpn gobii-web.xml -wVerFl "/data/gobii_bundle/config/gobii.version" -a || true;

#Set default crop to crop1 (global)
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -gD $CROP1;

#Configure web server for crop2
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -c  $CROP2  -stW  -soH $WEB_HOST -soN $WEB_PORT -soR $CROP2_CONTEXT_PATH | true;
#Configure PostGRES server for crop2
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -c  $CROP2  -stP -soH $DB_HOST -soN $DB_PORT -soU $DB_USERNAME -soP $DB_PASS -soR $DB_NAME_CROP2 | true;

#Configure web server for crop3
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -c  $CROP3  -stW  -soH $WEB_HOST -soN $WEB_PORT -soR $CROP3_CONTEXT_PATH | true;
#Configure PostGRES server for crop3
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -c  $CROP3  -stP -soH $DB_HOST -soN $DB_PORT -soU $DB_USERNAME -soP $DB_PASS -soR $DB_NAME_CROP3 | true;

#Configure web server for crop4
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -c  $CROP4  -stW  -soH $WEB_HOST -soN $WEB_PORT -soR $CROP4_CONTEXT_PATH | true;
#Configure PostGRES server for crop4
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -c  $CROP4  -stP -soH $DB_HOST -soN $DB_PORT -soU $DB_USERNAME -soP $DB_PASS -soR $DB_NAME_CROP4 | true;

#Configure web server for crop5
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -c  $CROP5  -stW  -soH $WEB_HOST -soN $WEB_PORT -soR $CROP5_CONTEXT_PATH | true;
#Configure PostGRES server for crop5
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -c  $CROP5  -stP -soH $DB_HOST -soN $DB_PORT -soU $DB_USERNAME -soP $DB_PASS -soR $DB_NAME_CROP5 | true;

#Configure web server for crop6
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -c  $CROP6  -stW  -soH $WEB_HOST -soN $WEB_PORT -soR $CROP6_CONTEXT_PATH | true;
#Configure PostGRES server for crop6
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -c  $CROP6  -stP -soH $DB_HOST -soN $DB_PORT -soU $DB_USERNAME -soP $DB_PASS -soR $DB_NAME_CROP6 | true;



#Set log file directory (global)
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -gL  $BUNDLE_PATH/logs;
#Create the crop directory structure, ex. /data/gobii_bundle/crops/rice/*
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -wdirs
#unfortunately, I can't get rid of this now. This is for setting the parameters for integration testing, which we don't need for production
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -gt  -gtcd $BUNDLE_PATH/test -gtcr  $CROP1  -gtcs  "java -jar gobiiconfig.jar"  -gtiu http://localhost:8080/$CROP1_CONTEXT_PATH -gtsf false -gtsh localhost -gtsp 22 -gtsu localhost -gtldu $LDAP_BACKGROUND_USER -gtldp $LDAP_BACKGROUND_PASSWORD;
java -jar gobiiconfig.jar -a -wfqpn $CONFIG_XML -gt  -gtcd $BUNDLE_PATH/test -gtcr  $CROP2  -gtcs  "java -jar gobiiconfig.jar"  -gtiu http://localhost:8080/$CROP2_CONTEXT_PATH -gtsf false -gtsh localhost -gtsp 22 -gtsu localhost -gtldu $LDAP_BACKGROUND_USER -gtldp $LDAP_BACKGROUND_PASSWORD | true;
#activate encryption/decryption
java -jar gobiiconfig.jar -wfqpn $CONFIG_XML -e $ACTIVATE_ENCRYPTION;

#KDCompute server settings
java -jar gobiiconfig.jar -ksvr -wfqpn $CONFIG_XML  -soH $KDC_HOST -soN $KDC_PORT_INTERNAL -soR $KDC_CONTEXT_PATH -krscSTA $KDC_JOB_START -krscSTT $KDC_JOB_STATUS -krscDLD $KDC_JOB_DOWNLOAD -kstTRS $KDC_JOB_CHECK_STATUS -kstTRM $KDC_JOB_FAIL_THRESHOLD -krscPRG $KDC_PURGE -soA $KDC_ACTIVE


#validate the new gobii configuration xml
java -jar gobiiconfig.jar -validate -wfqpn $CONFIG_XML;

echo "Done."
