#!/usr/bin/env bash

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
#set -x # xtrace: Similar to -v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#
### !!! WARNING !!!
#-----------------------------------------------------------------------------#
### Do not run this script as SUDO! There are explicit sudo commands which
### will prompt you for password. But not everything should run as sudo.

### usage: bash <bash_script_name>.sh <path of main.parameters> <path of 
# install.parameters> <dockerhubpassw | "askme"> <gobii_release_version>

### This a stand-alone equivalent of my THE_GOBII_SHIP Bamboo plan for the 
### ownCloud NODE

#-----------------------------------------------------------------------------#
### Requirements: 

###>>> 1. The user that will run this script needs to be a sudoer and under the gobii and docker groups. So preferably the user 'gadm'.

###>>> 2. The working directory needs to be where the gobiiconfig_wrapper.sh 
# is as well, typically <gobii_bundle>/conf/
###--->>> NOTE: The order of execution is important.
###--->>> NOTE: If weird things start happening on your containers, try 
# removing the images as well by running 'docker rmi' on each of the 3 nodes.
#-----------------------------------------------------------------------------#
### If you want a delete-all-images command, run this: 
###>>> [sudo docker stop $(sudo docker ps -qa) || true && sudo docker rm 
# $(sudo docker ps -aq) || true && sudo docker rmi $(sudo docker images -aq) 
# || true]
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
#@author: (palace) kdp44@cornell.edu
###>>> Co-Author: RLPetrie (rlp243@cornell.edu)
#-----------------------------------------------------------------------------#


#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

if [ $# -lt 3 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash <bash_script_name>.sh <path of main.parameters> <path of install.parameters> <dockerhubpassw | \"askme\"> <gobii_release_version>"
    echo "Set dockerhubpassw parameter to 'askme' for the script to prompt for password instead."
    exit 1
fi

#-----------------------------------------------------------------------------#
### load parameters -- main.parameters for deployment
#-----------------------------------------------------------------------------#

source $1

#-----------------------------------------------------------------------------#
### Set release version and Dockerhub password
###>>> if pass set to 'askme' call for user to enter password at cli
#-----------------------------------------------------------------------------#

DOCKER_HUB_PASSWORD=$2

#-----------------------------------------------------------------------------#
### Determining if this is a KEEP or WIPE Data Deployment
#-----------------------------------------------------------------------------#
clear

while true; do
		echo "Will this deployment KEEP or WIPE the GDM data?"
        read -p "Please enter \"KEEP\" or \"WIPE\" to continue: " keep_wipe
        echo;
        case $keep_wipe in
                [WIPE]* ) echo "You have choosen to $keep_wipe the data! Starting devops_files/WIPE.sh script..."; bash devops_files/WIPE.sh $1; break;;
                [KEEP]* ) echo "You have choosen to $keep_wipe the data.  Moving on..."; break;;
                * ) clear; echo; echo "Answer not \"KEEP\" or \"WIPE\"!"; echo;;
        esac
done

#-----------------------------------------------------------------------------#
### Menu
#-----------------------------------------------------------------------------#

clear

echo "
#------------------------------#
### GOBii Deployment Options ###
#------------------------------#
"
PS3='
Please enter your choice: '

options=(
"Full GDM Deployment + Portainer Node [DB, Web, Compute, KDC, ownCloud, Portainer]"
"Full GDM Deployment + Sherpa node [DB, Web, Compute, KDC, ownCloud, Sherpa]" 
"GDM + KDC [DB, Web, Compute, KDC]" 
"GDM [DB, Web, Compute]" 
"db-node" 
"web-node" 
"compute-node" 
"KDCompute" 
"ownCloud" 
"Haplo Tool" 
"Portainer" 
"Sherpa [Portainer Agent]" 
"Quit" 
)

select opt in "${options[@]}"


do
        case $opt in

                "Full GDM Deployment + Portainer Node [DB, Web, Compute, KDC, ownCloud, Portainer]")
                        echo;
                        if [[ $REPLY == 1 ]]; then
    
    						echo;

    						echo "You have selected to deploy the full GDM Suite..."
    						echo "[DB, Web, Compute, KDC, ownCloud, Portainer]"
						    echo;

    						read -p "Are you sure you want to deploy the full GDM Suite [y/n]: " SUITE_REPLY
    						echo;

    						if [[ $SUITE_REPLY == 'y' ]]; then
        
        						GDM_RELEASE_VERSION=$3
						        echo;

						        echo "Parameters: $1"
						        echo "GDM release version: $3"
						        echo;

						        read -p "Please press any key to continue... " -n1 -s
						        echo;
						        

								if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    								read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
								fi

								echo;


						        echo "Shipping DB Container..."
						        echo;

						        bash container_scripts/the_gobii_ship_db.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;

						        echo "The db-node deployment has completed."
						        echo;

						        echo "Moving on to the web-node..."
						        sleep 3
						        echo;

						        bash container_scripts/the_gobii_ship_web.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;

						        echo "The web-node deployment has completed."
						        echo;

						        echo "Moving on to the compute-node..."
						        sleep 3
						        echo;


						        bash container_scripts/the_gobii_ship_compute.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;

						        echo "The compute-node deployment has completed."
						        echo "Moving on to the kdc-node..."
						        sleep 3
						        echo;
						        
						        if [ $KDC_RELEASE_VERSION = "askme" ]; then
    								read -p "Please enter KDCompute release version: " KDC_RELEASE_VERSION
								fi
						        echo;

						        echo "Main Parameters: $1"
						        echo "KDCompute release version: $KDC_RELEASE_VERSION"
						        echo;

						        bash container_scripts/the_gobii_ship_kdc.sh $1 $DOCKER_HUB_PASSWORD $KDC_RELEASE_VERSION
						        echo;

						        echo "The kdc-node deployment has completed."
						        echo;

						        echo "Moving on to the oc-node..."
						        sleep 3
						        echo;

						        bash container_scripts/the_gobii_ship_oc.sh $1 $DOCKER_HUB_PASSWORD base
						        echo;

						        echo "The oc-node deployment has completed."
						        echo;

						        echo "Moving on to the Portainer deploy..."
						        sleep 3
						        echo;

						        bash container_scripts/the_gobii_ship_portainer.sh $1 $DOCKER_HUB_PASSWORD
						        echo;

						        echo "The portainer-node deployment has completed."
						        echo;
						        
						    
						    else
						        echo "Exiting the_gobii_ship.sh"
						        echo;
						        break
						    fi
						echo;
						break
						fi
                        ;;

                "Full GDM Deployment + Sherpa node [DB, Web, Compute, KDC, ownCloud, Sherpa]")
                        echo;
                        if [[ $REPLY == 2 ]]; then
    
    						echo;

    						echo "You have selected to deploy the full GDM Suite..."
    						echo "[DB, Web, Compute, KDC, ownCloud, Sherpa]"
						    echo;

    						read -p "Are you sure you want to deploy the full GDM Suite [y/n]: " SUITE_REPLY
    						echo;

    						if [[ $SUITE_REPLY == 'y' ]]; then
        
        						GDM_RELEASE_VERSION=$3
						        echo;

						        echo "Main Parameters: $1"
						        echo "GDM release version: $3"
						        echo;

						        read -p "Please press any key to continue... " -n1 -s
						        echo;
						        

								if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    								read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
								fi

								echo;


						        echo "Shipping DB Container..."
						        echo;

						        bash container_scripts/the_gobii_ship_db.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;

						        echo "The db-node deployment has completed."
						        echo;

						        echo "Moving on to the web-node..."
						        sleep 3
						        echo;

						        bash container_scripts/the_gobii_ship_web.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;

						        echo "The web-node deployment has completed."
						        echo;

						        echo "Moving on to the compute-node..."
						        sleep 3
						        echo;


						        bash container_scripts/the_gobii_ship_compute.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;

						        echo "The compute-node deployment has completed."
						        echo "Moving on to the kdc-node..."
						        sleep 3
						        echo;
						        
						        if [ $KDC_RELEASE_VERSION = "askme" ]; then
    								read -p "Please enter KDCompute release version: " KDC_RELEASE_VERSION
								fi
						        echo;

						        echo "Parameters: $1"
						        echo "KDCompute release version: $KDC_RELEASE_VERSION"
						        echo;

						        bash container_scripts/the_gobii_ship_kdc.sh $1 $DOCKER_HUB_PASSWORD $KDC_RELEASE_VERSION
						        echo;

						        echo "The kdc-node deployment has completed."
						        echo;

						        echo "Moving on to the oc-node..."
						        sleep 3
						        echo;

						        bash container_scripts/the_gobii_ship_oc.sh $1 $DOCKER_HUB_PASSWORD base
						        echo;

						        echo "The oc-node deployment has completed."
						        echo;

						        echo "Moving on to the Sherpa deploy..."
						        sleep 3
						        echo;

						       echo "Shipping Sherpa Node..."
						        echo;

						        bash container_scripts/the_gobii_ship_sherpa_agent.sh $1 $DOCKER_HUB_PASSWORD
						        echo;

						        echo "The sherpa-node deployment has completed."
						        echo;
						        
						    
						    else
						        echo "Exiting the_gobii_ship.sh"
						        echo;
						        break
						    fi
						echo;
						break
						fi
                        ;;        

                "GDM + KDC [DB, Web, Compute, KDC]")
                        echo;
                        if [[ $REPLY == 3 ]]; then
    
    						echo;
    						echo "You have selected to deploy GDM with KDCompute..."
    						echo "[DB, Web, Compute & KDC]"
						    echo;

    						read -p "Are you sure you want to deploy the GDM with KDCompute [y/n]: " GDMK_REPLY
    						echo;

    						if [[ $GDMK_REPLY == 'y' ]]; then
        
        						GDM_RELEASE_VERSION=$3
						        
						        read -p "Please enter KDC Release Version: " KDC_RELEASE_VERSION
								echo;

						        echo "Parameters: $1"
						        echo "KDCompute release version: $KDC_RELEASE_VERSION"
						        echo "GDM release version: $3"
						        echo;

								if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    								read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
								fi
								echo;

								echo "Shipping DB Container..."
						        echo;

						        bash container_scripts/the_gobii_ship_db.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;

						        echo "The db-node deployment has completed."
						        echo;

						        echo "Moving on to the web-node..."
						        sleep 3
						        echo;

						        bash container_scripts/the_gobii_ship_web.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;

						        echo "The web-node deployment has completed."
						        echo;

						        echo "Moving on to the compute-node..."
						        sleep 3
						        echo;


						        bash container_scripts/the_gobii_ship_compute.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;

						        echo "The compute-node deployment has completed."
						        echo;

						        echo "Moving on to the kdc-node..."
						        sleep 3
						        echo;

						        bash container_scripts/the_gobii_ship_kdc.sh $1 $DOCKER_HUB_PASSWORD $KDC_RELEASE_VERSION
						        echo;

						        echo "The web-node deployment has completed."
						        echo;

						    else
						        echo "Exiting the_gobii_ship.sh"
						        echo;
						        break
						    fi
						echo;
						break
						fi
                        ;;

                "GDM [DB, Web, Compute]")
                        echo;
                        if [[ $REPLY == 4 ]]; then
    						echo;

    						echo "You have selected to deploy base GDM..."
    						echo "[DB, Web & Compute]"
						    echo;

    						read -p "Are you sure you want to deploy the base GDM Containers [y/n]: " GDM_REPLY
    						echo;

    						if [[ $GDM_REPLY == 'y' ]]; then
        
        						GDM_RELEASE_VERSION=$3
						        echo;

						        echo "Parameters: $1"
						        echo "GDM release version: $3"
						        echo;

						        read -p "Please press any key to continue... " -n1 -s
						        echo;


								if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    								read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
								fi

								echo;


						        echo "Shipping DB Container..."
						        echo;

						        bash container_scripts/the_gobii_ship_db.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;

						        echo "The db-node deployment has completed."
						        echo;

						        echo "Moving on to the web-node..."
						        sleep 3
						        echo;

						        bash container_scripts/the_gobii_ship_web.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;

						        echo "The web-node deployment has completed."
						        echo;

						        echo "Moving on to the compute-node..."
						        sleep 3
						        echo;


						        bash container_scripts/the_gobii_ship_compute.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;

						        echo "The compute-node deployment has completed."
						        echo;
						        
						    else
						        echo "Exiting the_gobii_ship.sh"
						        echo;

						        break
						    fi
						echo;

						break
						fi
                        ;;

                "db-node")
                        echo;
                        if [[ $REPLY == 5 ]]; then
    						echo;

    						echo "You have selected to deploy db-node..."
						    echo;

    						read -p "Are you sure you want to deploy the DB Container [y/n]: " DB_REPLY
    						echo;

    						if [[ $DB_REPLY == 'y' ]]; then
        
        						GDM_RELEASE_VERSION=$3
						        echo;

						        echo "Main Parameters: $1"
						        echo "GDM release version: $3"
						        echo;

						        read -p "Please press any key to continue... " -n1 -s
						        echo;


								if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    								read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
								fi

								echo;

						        echo "Shipping DB Container..."
						        echo;

						        bash container_scripts/the_gobii_ship_db.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;
						    
						    else
						        echo "Exiting the_gobii_ship.sh"
						        echo;

						        break
						    fi
						echo;

						break
						fi
                        ;;

                "web-node")
                        echo;
                        if [[ $REPLY == 6 ]]; then
    						echo;

    						echo "You have selected to deploy web-node..."
						    echo;

    						read -p "Are you sure you want to deploy the Web Container [y/n]: " WEB_REPLY
    						echo;

    						if [[ $WEB_REPLY == 'y' ]]; then
        
        						GDM_RELEASE_VERSION=$3
						        echo;

						        echo "Main Parameters: $1"
						        echo "GDM release version: $3"
						        echo;

						        read -p "Please press any key to continue... " -n1 -s
						        echo;


								if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    								read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
								fi

								echo;

						        echo "Shipping Web Container..."
						        echo;

						        bash container_scripts/the_gobii_ship_web.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;
						    
						    else
						        echo "Exiting the_gobii_ship.sh"
						        echo;

						        break
						    fi
						echo;

						break
						fi
                        ;;

                "compute-node")
                        echo;
                        if [[ $REPLY == 7 ]]; then
    						echo;

    						echo "You have selected to deploy compute-node..."
						    echo;

    						read -p "Are you sure you want to deploy the Compute Container [y/n]: " COMPUTE_REPLY
    						echo;

    						if [[ $COMPUTE_REPLY == 'y' ]]; then
        
        						GDM_RELEASE_VERSION=$3
						        echo;

						        echo "Main Parameters: $1"
						        echo "GDM release version: $3"
						        echo;

						        read -p "Please press any key to continue... " -n1 -s
						        echo;

								if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    								read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
								fi

								echo;					

						        echo "Shipping Compute Container..."
						        echo;

						        bash container_scripts/the_gobii_ship_compute.sh $1 $DOCKER_HUB_PASSWORD $3
						        echo;
						    
						    else
						        echo "Exiting the_gobii_ship.sh"
						        echo;
						        break
						    fi
						echo;
						break
						fi
                        ;;

                "KDCompute")
                        echo;
                        if [[ $REPLY == 8 ]]; then
    						echo;

    						echo "You have selected to deploy kdc-node..."
						    echo;

    						read -p "Are you sure you want to deploy KDCompute Container [y/n]: " KDC_REPLY
    						echo;

    						if [[ $KDC_REPLY == 'y' ]]; then

    							if [ $KDC_RELEASE_VERSION = "askme" ]; then
    								read -p "Please enter KDCompute release version: " KDC_RELEASE_VERSION
						        fi
						        echo;

						        echo "Parameters: $1"
						        echo "KDCompute release version: $KDC_RELEASE_VERSION"
						        echo;

						        read -p "Please press any key to continue... " -n1 -s
						        echo;

								if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    								read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
								fi

								echo;

						        echo "Shipping KDCompute..."
						        echo;

						        bash container_scripts/the_gobii_ship_kdc.sh $1 $DOCKER_HUB_PASSWORD $KDC_RELEASE_VERSION
						        echo;
						    
						    else
						        echo "Exiting the_gobii_ship.sh"
						        echo;

						        break
						    fi
						echo;

						break
						fi
                        ;;

                "ownCloud")
                        echo;
						if [[ $REPLY == 9 ]]; then
    						echo;

    						echo "You have selected to deploy oc-node..."
						    echo;

    						read -p "Are you sure you want to deploy the ownCloud Container [y/n]: " OC_REPLY
    						echo;

    						if [[ $OC_REPLY == 'y' ]]; then

						        echo "Main Parameters: $1"
						        echo "ownCloud release version: base"
						        echo;

						        read -p "Please press any key to continue... " -n1 -s
						        echo;


								if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    								read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
								fi

								echo;


						        echo "Shipping ownCloud Container..."
						        echo;

						        bash container_scripts/the_gobii_ship_oc.sh $1 $DOCKER_HUB_PASSWORD base
						        echo;
						    
						    else
						        echo "Exiting the_gobii_ship.sh"
						        echo;

						        break
						    fi
						echo;

						break
						fi                
                        ;;

				"Haplo Tool")
                        echo;
						if [[ $REPLY == 10 ]]; then
    						echo;

    						echo "You have selected to deploy haplo-node..."
						    echo;

    						read -p "Are you sure you want to deploy the Haplo Tool Container [y/n]: " HAPLO_REPLY
    						echo;

    						if [[ $HAPLO_REPLY == 'y' ]]; then

								if [ $DOCKER_HUB_HAPLO_TAG = "askme" ]; then
    								read -p "Please enter KDCompute release version: " DOCKER_HUB_HAPLO_TAG
						        fi

						        echo "Main Parameters: $1"
						        echo "HaploTool release version: $DOCKER_HUB_HAPLO_TAG"
						        echo;

						        read -p "Please press any key to continue... " -n1 -s
						        echo;


								if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    								read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
								fi

								echo;


						        echo "Shipping ownCloud Container..."
						        echo;

						        bash container_scripts/the_gobii_ship_haplo.sh $1 $DOCKER_HUB_PASSWORD $DOCKER_HUB_HAPLO_TAG
						        echo;
						    
						    else
						        echo "Exiting the_gobii_ship.sh"
						        echo;

						        break
						    fi
						echo;

						break
						fi                
                        ;;

                "Portainer")
                        echo;
                        if [[ $REPLY == 11 ]]; then
    						echo;

    						echo "You have selected to deploy portainer-node..."
						    echo;

    						read -p "Are you sure you want to deploy the Portainer Container [y/n]: " P_REPLY
    						echo;

    						if [[ $P_REPLY == 'y' ]]; then
        
        						echo "Main Parameters: $1"
						        echo;

						        read -p "Please press any key to continue... " -n1 -s
						        echo;

						        if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    								read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
								fi

								echo "Shipping Portainer..."
						        echo;

						        bash container_scripts/the_gobii_ship_portainer.sh $1 $DOCKER_HUB_PASSWORD
						        echo;
						    
						    else
						        echo "Exiting the_gobii_ship.sh"
						        echo;

						        break
						    fi
						echo;
						break
						fi
                        ;;

                "Sherpa [Portainer Agent]")
                        echo;

                        if [[ $REPLY == 12 ]]; then
    						echo;

    						echo "You have selected to deploy the portainer-sherpa-node..."
						    echo;

    						read -p "Are you sure you want to deploy the Portainer Sherpa Container [y/n]: " S_REPLY
    						echo;

    						if [[ $S_REPLY == 'y' ]]; then
        
        						echo "Main Parameters: $1"
						        echo;

						        read -p "Please press any key to continue... " -n1 -s
						        echo;

						        if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    								read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
								fi

								echo "Shipping Portainer Sherpa Node..."
						        echo;

						        bash container_scripts/the_gobii_ship_sherpa_agent.sh $1 $DOCKER_HUB_PASSWORD
						        echo;
						    
						    else
						        echo "Exiting the_gobii_ship.sh"
						        echo;

						        break
						    fi
						echo;

						break
						fi
                        ;;

                "Quit")
                        break
                        ;;

                *) echo "invalid option $REPLY";;

        esac

done

echo "Moving on to ndd_pathing.sh"
echo;

read -p "Please press any key to continue... " -n1 -s
echo;

bash devops_files/ndd_pathing.sh $1
echo;

echo "the_gobii_ship.sh Script completed, Good Bye"
echo;
