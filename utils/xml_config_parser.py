from __future__ import print_function
import xml.etree.ElementTree as ET
import sys
import getopt
import traceback

def main(argv):
	filePath = ""
	xPath = ""
	replacementValue = ""
	addSubtree = ""
	verbose = False

	####################################################
	# Get parameters
	####################################################
	try:
		opts, args = getopt.getopt(argv, "hf:x:r:a:v", ["filePath=", "xPath=", "replacementValue=", "addSubtree=", "verbose"])
		#print (opts, args)
		# No arguments supplied, show help
		if len(args) < 2 and len(opts) < 2:
			printUsageHelp(0)
	except getopt.GetoptError as e1:
		printError(e1.message)
		traceback.print_exc()
		sys.exit(1)
	for opt, arg in opts:
		if opt == '-h':
			printUsageHelp(0)
		elif opt in ("-f", "--filePath"):
			filePath = arg
		elif opt in ("-x", "--xPath"):
			xPath = arg
		elif opt in ("-r", "--replacementValue"):
			replacementValue = arg
		elif opt in ("-a", "--addSubtree"):
			addSubtree = arg
		elif opt in ("-v", "--verbose"):
			verbose = True
	####################################################
	# Do the dew
	####################################################
	try:
		tree = ET.parse(filePath)
		root = tree.getroot()
		if verbose:
			print (xPath)
		node = tree.find(xPath)
		node.text = replacementValue
		if addSubtree != '':
			subtreeXml = ET.fromstring(addSubtree)
			root.append(subtreeXml)
		if verbose:
			print ("Found %s. " % node)

		# Write back to file
		tree.write(filePath)
	except Exception as e:
		print ("Exception occured while parsing the config xml: %s" % e.message)
		traceback.print_exc()
		sys.exit(1)
def printError(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

def printUsageHelp(eCode):
	print (eCode)
	print ("python xml_config_parser.py -f <filePath:string> -x <xPath:string> -r <replacementValue:string> -a <addSubtree:string>")
	print ("\t-h = Usage help")
	print ("\t-f or --filePath = Absolute path to the location of the XML config file.")
	print ("\t-x or --xPath = The xpath to find the element to change the value of. Supported syntax can be found here: https://docs.python.org/2/library/xml.etree.elementtree.html#supported-xpath-syntax")
	print ("\t-r or --replacementValue = This is the text to replace the value of the element found by the xpath passed to -x.")
	print ("\t-a or --addSubtree = This appends a subtree XML (ie. of a crop's GDM configuration) to the config file. It is REQUIRED that the text passed is a properly formed XML text.")
	print ("\t-v or --verbose = (OPTIONAL) More printings.")

	sys.exit(eCode)


if __name__ == "__main__":
	main(sys.argv[1:])
