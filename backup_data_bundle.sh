#!/usr/bin/env bash
#This script simply backs up the data files in the gobii_bundle directory to a specified backup path for later restore.
#This was written for the purpose of delivering patches and bug fixes without wiping out production data.
#Usage format: bash backup_data_bundle.sh <path_to_bundle> <backup_path> <mode>
#Modes: 1 = full backup, 2 = incremental backup
#Example usage: bash ../Repositories/gobiideployment/backup_data_bundle.sh gobii_bundles_archive/app_test_bundle gobii_backups 1
#Example usage: bash /shared_data/bamboo_gobii_backups/gobiideployment/backup_data_bundle.sh /shared_data/dev_test/gobii_bundle /shared_data/bamboo_gobii_backups/qa_test_incremental/daily 2

if [ $# -lt 3 ]; then
    echo "Invalid usage."
    echo "Usage: bash backup_data_bundle.sh <path_to_bundle> <backup_path> <mode>"
    echo "Description:"
    echo "This script simply backs up the data files in the gobii_bundle directory to a specified backup path for later restore."
    echo "This was written for the purpose of delivering patches and bug fixes without wiping out production data."
	echo "MODES: 1 = full backup, 2 = incremental backup"
	echo "Notes: For full backups, a subdirectory will be created inside the backup path to avoid overwriting an"
	echo "existing snapshot (name based on current timestamp)."
	echo "For incremental backups, the backup path is used directly as an incremental backup directory."
  	exit 1
fi

if [ ! -d $1 ]; then
    echo "$1 is NOT a directory. Aborting."
    exit 0
fi

if [ ! -d $2 ]; then
    echo "$2 is NOT a directory. Aborting."
    exit 0
fi

#Things to backup:
# 1. config/gobii-web.xml
# 2. crops/**
# 3. logs/**

GOBII_BUNDLE_DIR=$1
BACKUP_DIR=$2
SUBDIR=$(date +"%Y-%m-%d_%H-%M-%S")
MODE=$3

case "$MODE" in

1)  echo "Mode set to FULL BACKUP."
    #exit 1
	echo $SUBDIR > backup_timestamp.param; #save to a file for later restore - in automated workflows [DEPRECATED]
	echo "[Full] Backing up data to $BACKUP_DIR/$SUBDIR..."

	echo "Making the subdirectory..."
	mkdir -p $BACKUP_DIR/$SUBDIR

	echo "Backing up config/gobii-web.xml..."
	rsync -ahIL --delete --progress $GOBII_BUNDLE_DIR/config/gobii-web.xml $BACKUP_DIR/$SUBDIR

	echo "Backing up crops/**..."
	rsync -ahIL --delete --progress $GOBII_BUNDLE_DIR/crops $BACKUP_DIR/$SUBDIR

	echo "Backing up logs/**..."
	rsync -ahIL --delete --progress $GOBII_BUNDLE_DIR/logs $BACKUP_DIR/$SUBDIR
	;;
2)  echo  "Mode set to INCREMENTAL BACKUP."
	echo $BACKUP_DIR > backup_timestamp.param; #save to a file for later restore - in automated workflows
	echo "[Incremental] Backing up data to $BACKUP_DIR..."


	echo "Backing up config/gobii-web.xml..."
	rsync -ahL --delete $GOBII_BUNDLE_DIR/config/gobii-web.xml $BACKUP_DIR

	echo "Backing up crops/**..."
	rsync -ahL --delete $GOBII_BUNDLE_DIR/crops $BACKUP_DIR

	echo "Backing up logs/**..."
	rsync -ahL --delete $GOBII_BUNDLE_DIR/logs $BACKUP_DIR
    ;;
*) echo "Invalid mode provided. Please select use a valid value (1,2)."
	exit 0
   ;;
esac
